Projet de fin d'étude pour l'obtention de diplome de master en DSIR option GLNT.

Installation de l'application de gestion des examens
 
1 . installer composer , xampp et demarrer les services apache et mysql

2 . se placer dans le repertoire gestionexamen
	commande :
		cd gestionexamen
		
3 . lancer la commande d'installation des dependances du projet
	commande :
		composer install
		
4 . installer le module fixture
	commande : 
		composer require --dev doctrine/doctrine-fixtures-bundle
		
5 . changer le compte et mot de passe dans le fichier .env pour l'acces a la base des données 

6 . creer la base de données avec la commande suivante :
	commande : 
		php bin/console doctrine:database:create
	
7 . supprimer tous les fichiers de migrations dans le repertoire src/Migrations 

8 . génerer le fichier de migration avec les commandes 
	commandes :
	php bin/console make:migration
	php bin/console doctrine:migrations:migrate
	
9 . installer les comptes automatiquement (admin, agent, scolarite, chefdep, doyen, secgen) pour un mot de passe commun par défaut "password"
	commande:
	php bin/console doctrine:fixtures:load --no-interaction
	
10 . configurer le serveur apache 2.4 dans le fichier C:\xampp\apache\conf\extra\httpd-vhosts.conf avec ajout du texte suivant
	
	<VirtualHost localhost:80>
		ServerName localhost 
		 
		ServerAdmin compte@gmail.com
		DocumentRoot "C:/xampp/htdocs/gestionexamen/public"
		DirectoryIndex index.php
		<Directory "C:/xampp/htdocs/gestionexamen/public">
			Require all granted
			FallbackResource /index.php
		</Directory>
		<Directory "C:/xampp/htdocs/gestionexamen/public/uploads">
			FallbackResource disabled
		</Directory>
		ErrorLog "logs/apache-error.log"
		CustomLog "logs/apache-access.log" common
	</VirtualHost>
	
	il est recommandé de changer localhost par le nom de domaine approprié
	
7 . consulter le lien url http://localhost/ avec votre navigateur si vous avez choisi un nom de domaine 
	consulter le lien http://nomdedomaine/
	l'interface web apparait et vous pourrez y acceder avec le compte admin/password
	pour des mesures de securité, il est recommandé de changer les mots de passes par défauts pour chacun des comptes
