<?php
namespace App\Test\Entity;

use App\Entity\User;

use PHPUnit\Framework\TestCase;
class UserTest extends TestCase
{
    public function testUserToString() {
        $user = new User();
        $user->setFirstname('Amar');
        $user->setUsername('Amar');
        $this->assertSame('Amar', $user->__toString()); 
    }

    public function testUserIsActive()
    {
        $user = new User(); 
        $user->setUsername('Amar');
        $this->assertSame(true, $user->getIsActive());
    }
    public function testUserRole()
    {
        $user = new User();
        $user->setUsername('Amar');
        $user->setRoles(['ROLE_DOYEN']);
        $this->assertNotEquals(['ROLE_USER'], $user->getRoles());
        $this->assertTrue(in_array('ROLE_DOYEN', $user->getRoles(), true));
        $this->assertNotEmpty($user->getRoles());
        $user2 = new User();
        $user2->setUsername('Amar');
        $this->assertFalse(in_array('ROLE_DOYEN', $user2->getRoles(), true));
        $this->assertTrue(in_array('ROLE_USER', $user2->getRoles(), true));
    }

}