<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\SessionRepository")
 * @UniqueEntity(fields={"codesession"}, message="Code deja existant !!!")
 */
class Session
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le Code est obligatoire")
     */
    private $codesession;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le libéllé  est obligatoire")
     */
    private $libellesession;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\AnneeUniversitaire", inversedBy="sessions")
     * @Assert\NotBlank(message="il faut choisir une année")
     */
    private $annee;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Semestre", inversedBy="sessions")
     *
     */
    private $semestre;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Examen", mappedBy="session")
     */
    private $examens;

    private $libelle_session_semestre;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $valideEtp1;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $valideEtp2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $departValid;
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DocumentCsv", mappedBy="session")
     */
    private $documentCsvs;


    public function __construct()
    {
        $this->corrections = new ArrayCollection();
        $this->examens = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodesession(): ?string
    {
        return $this->codesession;
    }

    public function setCodesession(string $codesession): self
    {
        $this->codesession = $codesession;

        return $this;
    }

    public function getLibellesession(): ?string
    {
        return $this->libellesession;
    }

    public function setLibellesession(string $libellesession): self
    {
        $this->libellesession = $libellesession;

        return $this;
    }

    public function __toString()
    {
        // to show the name of the Category in the select
        return $this->libellesession;
        // to show the id of the Category in the select
        // return $this->id;
    }


    public function getAnnee(): ?AnneeUniversitaire
    {
        return $this->annee;
    }

    public function setAnnee(?AnneeUniversitaire $annee): self
    {
        $this->annee = $annee;

        return $this;
    }

    public function getSemestre(): ?Semestre
    {
        return $this->semestre;
    }

    public function setSemestre(?Semestre $semestre): self
    {
        $this->semestre = $semestre;

        return $this;
    }

    /**
     * @return Collection|Examen[]
     */
    public function getExamens(): Collection
    {
        return $this->examens;
    }

    public function addExamen(Examen $examen): self
    {
        if (!$this->examens->contains($examen)) {
            $this->examens[] = $examen;
            $examen->setSession($this);
        }

        return $this;
    }

    public function removeExamen(Examen $examen): self
    {
        if ($this->examens->contains($examen)) {
            $this->examens->removeElement($examen);
            // set the owning side to null (unless already changed)
            if ($examen->getSession() === $this) {
                $examen->setSession(null);
            }
        }

        return $this;
    }

    public function getLibelleSessionSemestre(): ?string
    {
        return $this->libellesession . "   " . $this->getSemestre() . "" . $this->getAnnee() . "";

    }

    public function getValideEtp1(): ?bool
    {
        return $this->valideEtp1;
    }

    public function setValideEtp1(?bool $valideEtp1): self
    {
        $this->valideEtp1 = $valideEtp1;

        return $this;
    }

    public function getValideEtp2(): ?bool
    {
        return $this->valideEtp2;
    }

    public function setValideEtp2(?bool $valideEtp2): self
    {
        $this->valideEtp2 = $valideEtp2;

        return $this;
    }

    public function getDepartValid(): ?string
    {
        return $this->departValid;
    }

    public function setDepartValid(string $departValid): self
    {
        $this->departValid = $departValid;

        return $this;
    }

    /**
     * @return Collection|DocumentCsv[]
     */
    public function getDocumentCsvs(): Collection
    {
        return $this->documentCsvs;
    }

    public function addDocumentCsv(DocumentCsv $documentCsv): self
    {
        if (!$this->documentCsvs->contains($documentCsv)) {
            $this->documentCsvs[] = $documentCsv;
            $documentCsv->setSession($this);
        }

        return $this;
    }

    public function removeDocumentCsv(DocumentCsv $documentCsv): self
    {
        if ($this->documentCsvs->contains($documentCsv)) {
            $this->documentCsvs->removeElement($documentCsv);
            // set the owning side to null (unless already changed)
            if ($documentCsv->getSession() === $this) {
                $documentCsv->setSession(null);
            }
        }

        return $this;
    }

}
