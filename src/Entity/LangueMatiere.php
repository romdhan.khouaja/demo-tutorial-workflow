<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LangueMatiereRepository")
 * @UniqueEntity(fields={"libelle_langue_fr"} ,message="Langue déja existant!!")
 * @UniqueEntity(fields={"libelle_langue_ar"} ,message="Langue déja existant!!")
 */
class LangueMatiere
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le libéllé français est obligatoire")
     */
    private $libelle_langue_fr;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le libéllé arabe est obligatoire")
     */
    private $libelle_langue_ar;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Matiere", mappedBy="languematiere")
     */
    private $matieres;

    public function __construct()
    {
        $this->matieres = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelleLangueFr(): ?string
    {
        return $this->libelle_langue_fr;
    }

    public function setLibelleLangueFr(string $libelle_langue_fr): self
    {
        $this->libelle_langue_fr = $libelle_langue_fr;

        return $this;
    }

    public function getLibelleLangueAr(): ?string
    {
        return $this->libelle_langue_ar;
    }

    public function setLibelleLangueAr(string $libelle_langue_ar): self
    {
        $this->libelle_langue_ar = $libelle_langue_ar;

        return $this;
    }

    /**
     * @return Collection|Matiere[]
     */
    public function getMatieres(): Collection
    {
        return $this->matieres;
    }

    public function addMatiere(Matiere $matiere): self
    {
        if (!$this->matieres->contains($matiere)) {
            $this->matieres[] = $matiere;
            $matiere->setLanguematiere($this);
        }

        return $this;
    }

    public function removeMatiere(Matiere $matiere): self
    {
        if ($this->matieres->contains($matiere)) {
            $this->matieres->removeElement($matiere);
            // set the owning side to null (unless already changed)
            if ($matiere->getLanguematiere() === $this) {
                $matiere->setLanguematiere(null);
            }
        }

        return $this;
    }
    public function __toString(){
        // to show the name of the Category in the select
        return $this->libelle_langue_ar;
        // to show the id of the Category in the select
        // return $this->id;
    }
}
