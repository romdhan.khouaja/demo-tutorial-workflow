<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CorrectionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Correction
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank(message="Le Quota est obligatoire")
     */
    private $part;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Enseignant", inversedBy="corrections")
     * @Assert\NotBlank(message="il faut choisissez un enseignant")
     */
    private $enseignant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Examen", inversedBy="corrections")
     */
    private $examen;
    private $nbPartRest;
    private $oldPart;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPart(): ?float
    {
        return $this->part;
    }

    public function setPart(float $part): self
    {
        $this->part = $part;

        return $this;
    }

    public function getEnseignant(): ?Enseignant
    {
        return $this->enseignant;
    }

    public function setEnseignant(?Enseignant $enseignant): self
    {
        $this->enseignant = $enseignant;

        return $this;
    }



    public function getExamen(): ?Examen
    {
        return $this->examen;
    }

    public function setExamen(?Examen $examen): self
    {
        $this->examen = $examen;

        return $this;
    }
    public function getNbPartRest(): ?string
    {
        return $this->nbPartRest;
    }

    public function setNbPartRest(string $nbPartRest): self
    {
        $this->nbPartRest = $nbPartRest;

        return $this;
    }

    public function getOldPart(): ?string
    {
        return $this->oldPart;
    }

    public function setOldPart(string $oldPart): self
    {
        $this->oldPart = $oldPart;

        return $this;
    }
    /**
     * @Assert\Callback
     */
    /*public function validate(ExecutionContextInterface $context ){

      if ($this->getNbPartRest()>0 and ($this->getNbPartRest() < $this->getPart())){
            $context->buildViolation("il vous reste que ".$this->getNbPartRest() )
                ->atPath('part')
                ->addViolation();
        }

      if ($this->getNbPartRest()==0 and ($this->getOldPart() < $this->getPart())){
            $context->buildViolation("Tous les parts sont distibués, vous ne pouvez pas dépassez ".$this->getOldPart() )
                ->atPath('part')
                ->addViolation();
        }
    }*/
}
