<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ConfigurationRepository")
 */
class Configuration
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomprenomdoyen;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adresse;

    /**
     * @ORM\Column(type="integer")
     */
    private $Telfaculter;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomprenomdoyen(): ?string
    {
        return $this->nomprenomdoyen;
    }

    public function setNomprenomdoyen(string $nomprenomdoyen): self
    {
        $this->nomprenomdoyen = $nomprenomdoyen;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getTelfaculter(): ?int
    {
        return $this->Telfaculter;
    }

    public function setTelfaculter(int $Telfaculter): self
    {
        $this->Telfaculter = $Telfaculter;

        return $this;
    }
}
