<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * @ORM\Entity(repositoryClass="App\Repository\EnseignantRepository")
 * @UniqueEntity(fields={"nomprenomfr"} ,message="Nom et prénom déja existant!!")
 * @UniqueEntity(fields={"nomprenomar"} ,message="Nom et prénom déja existant!!")
 * @UniqueEntity(fields={"code_enseignant"} ,message="Code déja existant!!")
 * @UniqueEntity(fields={"cin"} ,message="CIN déja existant!!")
 * @UniqueEntity(fields={"matricule"} ,message="Matricule déja existante!!")
 * @UniqueEntity(fields={"email"} ,message="Email déja existant!!")
 */
class Enseignant
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le libéllé français est obligatoire")
     */
    private $nomprenomfr;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le libéllé arabe est obligatoire")
     */
    private $nomprenomar;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le code est obligatoire")
     */
    private $code_enseignant;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message="Le casier est obligatoire")
     */
    private $casier;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message="Le cin est obligatoire")
     *  @Assert\Length(min=8,minMessage="Au minimum 8 chiffres")
     */
    private $cin;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message="La matricule est obligatoire")
     */
    private $matricule;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Email obligatoire")
     */
    private $email;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message="Le téléphone est obligatoire")
     * @Assert\Length(min=8,minMessage="Au mminimum 8 chiffres")
     */
    private $telephone1;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Length(min=8,minMessage="Au minimum 8 chiffres")
     */
    private $telephone2;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Grade", inversedBy="enseignants")
     * @Assert\NotBlank(message="il faut choisissez un grade")
     */
    private $grade;



    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Qualiter", inversedBy="enseignants")
     * @Assert\NotBlank(message="il faut choisissez une qualiter")
     */
    private $qualiter;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Matiere", mappedBy="enseignant")
     */
    private $matieres;

   
    /**
     * @ORM\Column(type="boolean")
     */
    private $avec_contrat;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\MailBox", mappedBy="enseignant")
     */
    private $mailBoxes;

    /**
     * @ORM\Column(type="boolean")
     */
    private $etat_surveille;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Correction", mappedBy="enseignant")
     */
    private $corrections;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\GroupeExamen", mappedBy="enseignant")
     */
    private $groupeExamens;


    public function __construct()
    {
        $this->matieres = new ArrayCollection();
        $this->mailBoxes = new ArrayCollection();
        $this->corrections = new ArrayCollection();
        $this->groupeExamens = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomprenomfr(): ?string
    {
        return $this->nomprenomfr;
    }

    public function setNomprenomfr(string $nomprenomfr): self
    {
        $this->nomprenomfr = $nomprenomfr;

        return $this;
    }

    public function getNomprenomar(): ?string
    {
        return $this->nomprenomar;
    }

    public function setNomprenomar(string $nomprenomar): self
    {
        $this->nomprenomar = $nomprenomar;

        return $this;
    }

    public function getCodeEnseignant(): ?string
    {
        return $this->code_enseignant;
    }

    public function setCodeEnseignant(string $code_enseignant): self
    {
        $this->code_enseignant = $code_enseignant;

        return $this;
    }

    public function getCasier(): ?int
    {
        return $this->casier;
    }

    public function setCasier(int $casier): self
    {
        $this->casier = $casier;

        return $this;
    }

    public function getCin(): ?int
    {
        return $this->cin;
    }

    public function setCin(int $cin): self
    {
        $this->cin = $cin;

        return $this;
    }

    public function getMatricule(): ?int
    {
        return $this->matricule;
    }

    public function setMatricule(int $matricule): self
    {
        $this->matricule = $matricule;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getTelephone1(): ?int
    {
        return $this->telephone1;
    }

    public function setTelephone1(int $telephone1): self
    {
        $this->telephone1 = $telephone1;

        return $this;
    }

    public function getTelephone2(): ?int
    {
        return $this->telephone2;
    }

    public function setTelephone2(int $telephone2): self
    {
        $this->telephone2 = $telephone2;

        return $this;
    }

    public function getGrade(): ?Grade
    {
        return $this->grade;
    }

    public function setGrade(?Grade $grade): self
    {
        $this->grade = $grade;

        return $this;
    }



    public function getQualiter(): ?Qualiter
    {
        return $this->qualiter;
    }

    public function setQualiter(?Qualiter $qualiter): self
    {
        $this->qualiter = $qualiter;

        return $this;
    }

    /**
     * @return Collection|Matiere[]
     */
    public function getMatieres(): Collection
    {
        return $this->matieres;
    }

    public function addMatiere(Matiere $matiere): self
    {
        if (!$this->matieres->contains($matiere)) {
            $this->matieres[] = $matiere;
            $matiere->setEnseignant($this);
        }

        return $this;
    }

    public function removeMatiere(Matiere $matiere): self
    {
        if ($this->matieres->contains($matiere)) {
            $this->matieres->removeElement($matiere);
            // set the owning side to null (unless already changed)
            if ($matiere->getEnseignant() === $this) {
                $matiere->setEnseignant(null);
            }
        }

        return $this;
    }

    public function getAvecContrat(): ?bool
    {
        return $this->avec_contrat;
    }

    public function setAvecContrat(bool $avec_contrat): self
    {
        $this->avec_contrat = $avec_contrat;

        return $this;
    }

    /**
     * @return Collection|MailBox[]
     */
    public function getMailBoxes(): Collection
    {
        return $this->mailBoxes;
    }

    public function addMailBox(MailBox $mailBox): self
    {
        if (!$this->mailBoxes->contains($mailBox)) {
            $this->mailBoxes[] = $mailBox;
            $mailBox->addEnseignant($this);
        }

        return $this;
    }

    public function removeMailBox(MailBox $mailBox): self
    {
        if ($this->mailBoxes->contains($mailBox)) {
            $this->mailBoxes->removeElement($mailBox);
            $mailBox->removeEnseignant($this);
        }

        return $this;
    }
    public function __toString(){
        // to show the name of the Category in the select
        return $this->nomprenomar;
        // to show the id of the Category in the select
        // return $this->id;
    }

    public function getEtatSurveille(): ?bool
    {
        return $this->etat_surveille;
    }

    public function setEtatSurveille(bool $etat_surveille): self
    {
        $this->etat_surveille = $etat_surveille;

        return $this;
    }

    /**
     * @return Collection|Correction[]
     */
    public function getCorrections(): Collection
    {
        return $this->corrections;
    }

    public function addCorrection(Correction $correction): self
    {
        if (!$this->corrections->contains($correction)) {
            $this->corrections[] = $correction;
            $correction->setEnseignant($this);
        }

        return $this;
    }

    public function removeCorrection(Correction $correction): self
    {
        if ($this->corrections->contains($correction)) {
            $this->corrections->removeElement($correction);
            // set the owning side to null (unless already changed)
            if ($correction->getEnseignant() === $this) {
                $correction->setEnseignant(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|GroupeExamen[]
     */
    public function getGroupeExamens(): Collection
    {
        return $this->groupeExamens;
    }

    public function addGroupeExamen(GroupeExamen $groupeExamen): self
    {
        if (!$this->groupeExamens->contains($groupeExamen)) {
            $this->groupeExamens[] = $groupeExamen;
            $groupeExamen->addEnseignant($this);
        }

        return $this;
    }

    public function removeGroupeExamen(GroupeExamen $groupeExamen): self
    {
        if ($this->groupeExamens->contains($groupeExamen)) {
            $this->groupeExamens->removeElement($groupeExamen);
            $groupeExamen->removeEnseignant($this);
        }

        return $this;
    }
}
