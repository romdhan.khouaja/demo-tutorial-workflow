<?php

namespace App\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MatiereRepository")
 * @UniqueEntity(fields={"code_matiere"} ,message="Code Matiére déja existant!!")
 *
 */
class Matiere
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message="Le code est obligatoire")
     */
    private $code_matiere;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le libéllé français est obligatoire")
     */
    private $libelle_matiere_fr;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le libéllé arabe est obligatoire")
     */
    private $libelle_matiere_ar;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank(message="Le coefficient est obligatoire")
     */
    private $coefficient;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Module", inversedBy="matieres")
     * @Assert\NotBlank(message="il faut choisissez un module")
     */
    private $module;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\LangueMatiere", inversedBy="matieres")
     * @Assert\NotBlank(message="il faut choisissez une langue")
     */
    private $languematiere;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TypeMatiere", inversedBy="matieres")
     * @Assert\NotBlank(message="il faut choisissez un type")
     */
    private $typematiere;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TypePassage", inversedBy="matieres")
     * @Assert\NotBlank(message="il faut choisissez un passage")
     */
    private $typepassage;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Semestre", inversedBy="matieres")
     * @Assert\NotBlank(message="il faut choisissez une semestre")
     */
    private $semestre;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Niveau", inversedBy="matieres")
     * @Assert\NotBlank(message="il faut choisissez un niveau")
     */
    private $niveau;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Enseignant", inversedBy="matieres")
     */
    private $enseignant;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank(message="la duré est obligatoire")
     */
    private $dure;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Examen", mappedBy="matiere")
     */
    private $examens;

    public function __construct()
    {
        $this->corrections = new ArrayCollection();
        $this->examens = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeMatiere(): ?int
    {
        return $this->code_matiere;
    }

    public function setCodeMatiere(int $code_matiere): self
    {
        $this->code_matiere = $code_matiere;

        return $this;
    }

    public function getLibelleMatiereFr(): ?string
    {
        return $this->libelle_matiere_fr;
    }

    public function setLibelleMatiereFr(string $libelle_matiere_fr): self
    {
        $this->libelle_matiere_fr = $libelle_matiere_fr;

        return $this;
    }

    public function getLibelleMatiereAr(): ?string
    {
        return $this->libelle_matiere_ar;
    }

    public function setLibelleMatiereAr(string $libelle_matiere_ar): self
    {
        $this->libelle_matiere_ar = $libelle_matiere_ar;

        return $this;
    }

    public function getCoefficient(): ?float
    {
        return $this->coefficient;
    }

    public function setCoefficient(float $coefficient): self
    {
        $this->coefficient = $coefficient;

        return $this;
    }

    public function getModule(): ?Module
    {
        return $this->module;
    }

    public function setModule(?Module $module): self
    {
        $this->module = $module;

        return $this;
    }


    public function getLanguematiere(): ?LangueMatiere
    {
        return $this->languematiere;
    }

    public function setLanguematiere(?LangueMatiere $languematiere): self
    {
        $this->languematiere = $languematiere;

        return $this;
    }

    public function getTypematiere(): ?TypeMatiere
    {
        return $this->typematiere;
    }

    public function setTypematiere(?TypeMatiere $typematiere): self
    {
        $this->typematiere = $typematiere;

        return $this;
    }

    public function getTypepassage(): ?TypePassage
    {
        return $this->typepassage;
    }

    public function setTypepassage(?TypePassage $typepassage): self
    {
        $this->typepassage = $typepassage;

        return $this;
    }

    public function getSemestre(): ?Semestre
    {
        return $this->semestre;
    }

    public function setSemestre(?Semestre $semestre): self
    {
        $this->semestre = $semestre;

        return $this;
    }

    public function getNiveau(): ?Niveau
    {
        return $this->niveau;
    }

    public function setNiveau(?Niveau $niveau): self
    {
        $this->niveau = $niveau;

        return $this;
    }

    public function getEnseignant(): ?Enseignant
    {
        return $this->enseignant;
    }

    public function setEnseignant(?Enseignant $enseignant): self
    {
        $this->enseignant = $enseignant;

        return $this;
    }

    /**
     * @return Collection|Correction[]
     */
    public function getCorrections(): Collection
    {
        return $this->corrections;
    }

    public function addCorrection(Correction $correction): self
    {
        if (!$this->corrections->contains($correction)) {
            $this->corrections[] = $correction;
            $correction->setMatiere($this);
        }

        return $this;
    }

    public function removeCorrection(Correction $correction): self
    {
        if ($this->corrections->contains($correction)) {
            $this->corrections->removeElement($correction);
            // set the owning side to null (unless already changed)
            if ($correction->getMatiere() === $this) {
                $correction->setMatiere(null);
            }
        }

        return $this;
    }

    public function getDure(): ?float
    {
        return $this->dure;
    }

    public function setDure(float $dure): self
    {
        $this->dure = $dure;

        return $this;
    }

    /**
     * @return Collection|Examen[]
     */
    public function getExamens(): Collection
    {
        return $this->examens;
    }

    public function addExamen(Examen $examen): self
    {
        if (!$this->examens->contains($examen)) {
            $this->examens[] = $examen;
            $examen->setMatiere($this);
        }

        return $this;
    }

    public function removeExamen(Examen $examen): self
    {
        if ($this->examens->contains($examen)) {
            $this->examens->removeElement($examen);
            // set the owning side to null (unless already changed)
            if ($examen->getMatiere() === $this) {
                $examen->setMatiere(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        // to show the name of the Category in the select
        return $this->libelle_matiere_ar;
        // to show the id of the Category in the select
        // return $this->id;
    }
}
