<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CentreExamenRepository")
 * @UniqueEntity(fields={"code_centre"},message="Code déja existant!!")
 * @UniqueEntity(fields={"libelle_centre"},message="libellé déja existante!!")
 */
class CentreExamen
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le code est obligatoire")
     */
    private $code_centre;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message="La capacité est obligatoire")
     */
    private $capacite;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="La libéllé  est obligatoire")
     */
    private $libelle_centre;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\GroupeExamen", mappedBy="centreexamen")
     */
    private $groupeExamens;
    private $libelle_groupeexamen;

    public function __construct()
    {
        $this->groupeExamens = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeCentre(): ?string
    {
        return $this->code_centre;
    }

    public function setCodeCentre(string $code_centre): self
    {
        $this->code_centre = $code_centre;

        return $this;
    }

    public function getCapacite(): ?int
    {
        return $this->capacite;
    }

    public function setCapacite(int $capacite): self
    {
        $this->capacite = $capacite;

        return $this;
    }

    public function getLibelleCentre(): ?string
    {
        return $this->libelle_centre;
    }

    public function setLibelleCentre(string $libelle_centre): self
    {
        $this->libelle_centre = $libelle_centre;

        return $this;
    }


    public function __toString(){
        // to show the name of the Category in the select
        return $this->libelle_centre;
        // to show the id of the Category in the select
        // return $this->id;
    }

    /**
     * @return Collection|GroupeExamen[]
     */
    public function getGroupeExamens(): Collection
    {
        return $this->groupeExamens;
    }

    public function addGroupeExamen(GroupeExamen $groupeExamen): self
    {
        if (!$this->groupeExamens->contains($groupeExamen)) {
            $this->groupeExamens[] = $groupeExamen;
            $groupeExamen->setCentreexamen($this);
        }

        return $this;
    }

    public function removeGroupeExamen(GroupeExamen $groupeExamen): self
    {
        if ($this->groupeExamens->contains($groupeExamen)) {
            $this->groupeExamens->removeElement($groupeExamen);
            // set the owning side to null (unless already changed)
            if ($groupeExamen->getCentreexamen() === $this) {
                $groupeExamen->setCentreexamen(null);
            }
        }

        return $this;
    }
    public function getLibelleGroupeExamen(): ?string
    {
        return $this->libelle_centre." ] ".$this->capacite."]";
    }
}
