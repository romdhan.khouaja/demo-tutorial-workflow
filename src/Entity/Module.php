<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity(repositoryClass="App\Repository\ModuleRepository")
 * @UniqueEntity(fields={"libelle_module_fr"} ,message="Module déja existant!!")
 * @UniqueEntity(fields={"libelle_module_ar"} ,message="Module déja existant!!")
 */
class Module
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le libéllé français est obligatoire")
     */
    private $libelle_module_fr;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le libéllé arabe est obligatoire")
     */
    private $libelle_module_ar;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Matiere", mappedBy="module")
     */
    private $matieres;
    private $libelle_module;

    public function __construct()
    {
        $this->matieres = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelleModuleFr(): ?string
    {
        return $this->libelle_module_fr;
    }

    public function setLibelleModuleFr(string $libelle_module_fr): self
    {
        $this->libelle_module_fr = $libelle_module_fr;

        return $this;
    }

    public function getLibelleModuleAr(): ?string
    {
        return $this->libelle_module_ar;
    }

    public function setLibelleModuleAr(string $libelle_module_ar): self
    {
        $this->libelle_module_ar = $libelle_module_ar;

        return $this;
    }

    /**
     * @return Collection|Matiere[]
     */
    public function getMatieres(): Collection
    {
        return $this->matieres;
    }

    public function addMatiere(Matiere $matiere): self
    {
        if (!$this->matieres->contains($matiere)) {
            $this->matieres[] = $matiere;
            $matiere->setModule($this);
        }

        return $this;
    }

    public function removeMatiere(Matiere $matiere): self
    {
        if ($this->matieres->contains($matiere)) {
            $this->matieres->removeElement($matiere);
            // set the owning side to null (unless already changed)
            if ($matiere->getModule() === $this) {
                $matiere->setModule(null);
            }
        }

        return $this;
    }
    public function __toString(){
        // to show the name of the Category in the select
        return $this->libelle_module_ar;
        // to show the id of the Category in the select
        // return $this->id;
    }
    public function getLibelleModule(): ?string
    {
        return $this->libelle_module_fr."  [ ".$this->libelle_module_ar."]";
    }
}
