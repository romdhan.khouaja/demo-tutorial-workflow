<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GradeRepository")
 * @UniqueEntity(fields={"libelle_grade_fr"} ,message="Grade déja existant!!")
 * @UniqueEntity(fields={"libelle_grade_ar"} ,message="Grade déja existant!!")
 */
class Grade
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le libéllé français est obligatoire")
     */
    private $libelle_grade_fr;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le libéllé arabe est obligatoire")
     */
    private $libelle_grade_ar;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Enseignant", mappedBy="grade")
     */
    private $enseignants;
    private $libelle_grade;

    public function __construct()
    {
        $this->enseignants = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelleGradeFr(): ?string
    {
        return $this->libelle_grade_fr;
    }

    public function setLibelleGradeFr(string $libelle_grade_fr): self
    {
        $this->libelle_grade_fr = $libelle_grade_fr;

        return $this;
    }

    public function getLibelleGradeAr(): ?string
    {
        return $this->libelle_grade_ar;
    }

    public function setLibelleGradeAr(string $libelle_grade_ar): self
    {
        $this->libelle_grade_ar = $libelle_grade_ar;

        return $this;
    }

    /**
     * @return Collection|Enseignant[]
     */
    public function getEnseignants(): Collection
    {
        return $this->enseignants;
    }

    public function addEnseignant(Enseignant $enseignant): self
    {
        if (!$this->enseignants->contains($enseignant)) {
            $this->enseignants[] = $enseignant;
            $enseignant->setGrade($this);
        }

        return $this;
    }

    public function removeEnseignant(Enseignant $enseignant): self
    {
        if ($this->enseignants->contains($enseignant)) {
            $this->enseignants->removeElement($enseignant);
            // set the owning side to null (unless already changed)
            if ($enseignant->getGrade() === $this) {
                $enseignant->setGrade(null);
            }
        }

        return $this;
    }
    public function __toString(){
        // to show the name of the Category in the select
        return $this->libelle_grade_ar;
        // to show the id of the Category in the select
        // return $this->id;
    }
    public function getLibelleGrade(): ?string
    {
        return $this->libelle_grade_fr."  [ ".$this->libelle_grade_ar."]";
    }
}
