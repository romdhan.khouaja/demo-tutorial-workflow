<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\FiliereRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(fields={"libelle_filiere_fr"} ,message="Nom de Filière déja existant!!")
 * @UniqueEntity(fields={"libelle_filiere_ar"},message="Nom de Filière déja existant!!")
 */
class Filiere
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le libéllé français est obligatoire")
     */
    private $libelle_filiere_fr;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le libéllé arabe est obligatoire")
     */
    private $libelle_filiere_ar;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Departement", inversedBy="filiere")
     */
    private $departement;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Niveau", mappedBy="filiere")
     */
    private $niveaux;

    private $libelle_filiere;

    public function __construct()
    {
        $this->niveaux = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelleFiliereFr(): ?string
    {
        return $this->libelle_filiere_fr;
    }

    public function setLibelleFiliereFr(string $libelle_filiere_fr): self
    {
        $this->libelle_filiere_fr = $libelle_filiere_fr;

        return $this;
    }

    public function getLibelleFiliereAr(): ?string
    {
        return $this->libelle_filiere_ar;
    }

    public function setLibelleFiliereAr(string $libelle_filiere_ar): self
    {
        $this->libelle_filiere_ar = $libelle_filiere_ar;

        return $this;
    }

    public function getDepartement(): ?Departement
    {
        return $this->departement;
    }

    public function setDepartement(?Departement $departement): self
    {
        $this->departement = $departement;

        return $this;
    }

    /**
     * @return Collection|Niveau[]
     */
    public function getNiveaux(): Collection
    {
        return $this->niveaux;
    }

    public function addNiveau(Niveau $niveau): self
    {
        if (!$this->niveaux->contains($niveau)) {
            $this->niveaux[] = $niveau;
            $niveau->setFiliere($this);
        }

        return $this;
    }

    public function removeNiveau(Niveau $niveau): self
    {
        if ($this->niveaux->contains($niveau)) {
            $this->niveaux->removeElement($niveau);
            // set the owning side to null (unless already changed)
            if ($niveau->getFiliere() === $this) {
                $niveau->setFiliere(null);
            }
        }

        return $this;
    }
    public function __toString(){
        // to show the name of the Category in the select
        return $this->libelle_filiere_ar;
        // to show the id of the Category in the select
        // return $this->id;
    }
    public function getLibelleFiliere(): ?string
    {
        return $this->libelle_filiere_fr."  [ ".$this->libelle_filiere_ar."]";
    }

}
