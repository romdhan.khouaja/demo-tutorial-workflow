<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\QualiterRepository")
 * @UniqueEntity(fields={"libelle_qualite_fr"},message="Nom qualiter déja existant!!")
 * @UniqueEntity(fields={"libelle_qualite_ar"},message="Nom qualiter déja existant!!")
 */
class Qualiter
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le libéllé français est obligatoire")
     */
    private $libelle_qualite_fr;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le libéllé arabe est obligatoire")
     */
    private $libelle_qualite_ar;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Enseignant", mappedBy="qualiter")
     */
    private $enseignants;

    public function __construct()
    {
        $this->enseignants = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelleQualiteFr(): ?string
    {
        return $this->libelle_qualite_fr;
    }

    public function setLibelleQualiteFr(string $libelle_qualite_fr): self
    {
        $this->libelle_qualite_fr = $libelle_qualite_fr;

        return $this;
    }

    public function getLibelleQualiteAr(): ?string
    {
        return $this->libelle_qualite_ar;
    }

    public function setLibelleQualiteAr(string $libelle_qualite_ar): self
    {
        $this->libelle_qualite_ar = $libelle_qualite_ar;

        return $this;
    }

    /**
     * @return Collection|Enseignant[]
     */
    public function getEnseignants(): Collection
    {
        return $this->enseignants;
    }

    public function addEnseignant(Enseignant $enseignant): self
    {
        if (!$this->enseignants->contains($enseignant)) {
            $this->enseignants[] = $enseignant;
            $enseignant->setQualiter($this);
        }

        return $this;
    }

    public function removeEnseignant(Enseignant $enseignant): self
    {
        if ($this->enseignants->contains($enseignant)) {
            $this->enseignants->removeElement($enseignant);
            // set the owning side to null (unless already changed)
            if ($enseignant->getQualiter() === $this) {
                $enseignant->setQualiter(null);
            }
        }

        return $this;
    }
    public function __toString(){
        // to show the name of the Category in the select
        return $this->libelle_qualite_ar;
        // to show the id of the Category in the select
        // return $this->id;
    }
}
