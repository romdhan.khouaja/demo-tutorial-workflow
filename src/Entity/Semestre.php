<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SemestreRepository")
 * @UniqueEntity(fields={"libelle_semestre_ar"},message="Nom qualiter déja existant!!")
 * @UniqueEntity(fields={"libelle_semestre_fr"},message="Nom qualiter déja existant!!")
 */
class Semestre
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le libéllé arabe est obligatoire")
     */
    private $libelle_semestre_ar;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le libéllé français est obligatoire")
     */
    private $libelle_semestre_fr;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Matiere", mappedBy="semestre")
     */
    private $matieres;
    private $libelle_semestre;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Session", mappedBy="semestre")
     */
    private $sessions;


    public function __construct()
    {
        $this->matieres = new ArrayCollection();
        $this->sessions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelleSemestreAr(): ?string
    {
        return $this->libelle_semestre_ar;
    }

    public function setLibelleSemestreAr(string $libelle_semestre_ar): self
    {
        $this->libelle_semestre_ar = $libelle_semestre_ar;

        return $this;
    }

    public function getLibelleSemestreFr(): ?string
    {
        return $this->libelle_semestre_fr;
    }

    public function setLibelleSemestreFr(string $libelle_semestre_fr): self
    {
        $this->libelle_semestre_fr = $libelle_semestre_fr;

        return $this;
    }

    /**
     * @return Collection|Matiere[]
     */
    public function getMatieres(): Collection
    {
        return $this->matieres;
    }

    public function addMatiere(Matiere $matiere): self
    {
        if (!$this->matieres->contains($matiere)) {
            $this->matieres[] = $matiere;
            $matiere->setSemestre($this);
        }

        return $this;
    }

    public function removeMatiere(Matiere $matiere): self
    {
        if ($this->matieres->contains($matiere)) {
            $this->matieres->removeElement($matiere);
            // set the owning side to null (unless already changed)
            if ($matiere->getSemestre() === $this) {
                $matiere->setSemestre(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        // to show the name of the Category in the select
        return $this->libelle_semestre_ar;
        // to show the id of the Category in the select
        // return $this->id;
    }

    public function getLibelleSemestre(): ?string
    {
        return $this->libelle_semestre_fr . "  [ " . $this->libelle_semestre_ar . "]";
    }


    /**
     * @return Collection|Session[]
     */
    public function getSessions(): Collection
    {
        return $this->sessions;
    }

    public function addSession(Session $session): self
    {
        if (!$this->sessions->contains($session)) {
            $this->sessions[] = $session;
            $session->setSemestre($this);
        }

        return $this;
    }

    public function removeSession(Session $session): self
    {
        if ($this->sessions->contains($session)) {
            $this->sessions->removeElement($session);
            // set the owning side to null (unless already changed)
            if ($session->getSemestre() === $this) {
                $session->setSemestre(null);
            }
        }

        return $this;
    }

}
