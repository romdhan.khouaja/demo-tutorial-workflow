<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DepartementRepository")
 *  @UniqueEntity(fields={"libelle_departement_fr"} ,message="Nom de Département déja existant!!")
 * @UniqueEntity(fields={"libelle_departement_ar"},message="Nom de Département déja existant!!")
 */
class Departement
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le libéllé français est obligatoire")
     */
    private $libelle_departement_fr;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le libéllé arabe est obligatoire")
     */
    private $libelle_departement_ar;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Filiere", mappedBy="departement")
     */
    private $filiere;


    private $libelle_departement;


    public function __construct()
    {
        $this->filiere = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelleDepartementFr(): ?string
    {
        return $this->libelle_departement_fr;
    }

    public function setLibelleDepartementFr(string $libelle_departement_fr): self
    {
        $this->libelle_departement_fr = $libelle_departement_fr;

        return $this;
    }

    public function getLibelleDepartementAr(): ?string
    {
        return $this->libelle_departement_ar;
    }

    public function setLibelleDepartementAr(string $libelle_departement_ar): self
    {
        $this->libelle_departement_ar = $libelle_departement_ar;

        return $this;
    }

    /**
     * @return Collection|Filiere[]
     */
    public function getFiliere(): Collection
    {
        return $this->filiere;
    }

    public function addFiliere(Filiere $filiere): self
    {
        if (!$this->filiere->contains($filiere)) {
            $this->filiere[] = $filiere;
            $filiere->setDepartement($this);
        }

        return $this;
    }

    public function removeFiliere(Filiere $filiere): self
    {
        if ($this->filiere->contains($filiere)) {
            $this->filiere->removeElement($filiere);
            // set the owning side to null (unless already changed)
            if ($filiere->getDepartement() === $this) {
                $filiere->setDepartement(null);
            }
        }

        return $this;
    }
    public function __toString(){
        // to show the name of the Category in the select
        return $this->libelle_departement_ar;
        // to show the id of the Category in the select
        // return $this->id;
    }
    public function getLibelleDepartement(): ?string
    {
        return $this->libelle_departement_fr."  [ ".$this->libelle_departement_ar."]";
    }
}
