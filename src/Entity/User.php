<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields={"username"}, message="Login deja existant !!!")
 * @UniqueEntity(fields={"email"}, message="Email deja existant !!!")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\NotBlank(message="login  obligatoire")
     */
    private $username;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     *
     */
    private $password;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Prénom obligatoire")
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Nom obligatoire")
     */
    private $lastname;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Departement", cascade={"persist", "remove"})
     */
    private $departement;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    private $modifiedAt;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Email obligatoire")
     */
    private $email;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Document", mappedBy="users")
     */
    private $documents;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Document", mappedBy="createdBy")
     */
    private $mydocuments;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $token;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $passwordRequestedAt;
    /**
     * @Assert\NotBlank(message="Ce champs ne doit pas être vide")
     * @Assert\Length(max=4096)
     */
    private $plainPassword;
    public $oldpassword;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MailBox", mappedBy="user")
     */
    private $mailBoxes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DocumentMail", mappedBy="user")
     */
    private $documentMails;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DocumentOpened", mappedBy="user")
     */
    private $documentOpeneds;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastLogin;


    public function isChefDepartement()
    {
        return in_array('ROLE_CHEF_DEPARTEMENT', $this->getRoles());
}

    public function __construct()
    {
        $this->isActive = true;
        $this->documents = new ArrayCollection();
        $this->mydocuments = new ArrayCollection();
        $this->mailBoxes = new ArrayCollection();
        $this->documentMails = new ArrayCollection();
        $this->documentOpeneds = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string)$this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }
    public function getOldpassword(): string
    {
        return (string) $this->oldpassword;
    }

    public function setoldPassword(string $oldpassword): self
    {
        $this->oldpassword = $oldpassword;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getDepartement(): ?Departement
    {
        return $this->departement;
    }

    public function setDepartement(?Departement $departement): self
    {
        $this->departement = $departement;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Collection|Document[]
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    public function addDocument(Document $document): self
    {
        if (!$this->documents->contains($document)) {
            $this->documents[] = $document;
            $document->addUser($this);
        }

        return $this;
    }

    public function removeDocument(Document $document): self
    {
        if ($this->documents->contains($document)) {
            $this->documents->removeElement($document);
            $document->removeUser($this);

        }

        return $this;
    }


    public function __toString()
    {
        return (string)$this->getUsername();
    }

    /**
     * @return Collection|Document[]
     */
    public function getMydocuments(): Collection
    {
        return $this->mydocuments;
    }

    public function addMydocument(Document $mydocument): self
    {
        if (!$this->mydocuments->contains($mydocument)) {
            $this->mydocuments[] = $mydocument;
            $mydocument->setCreatedBy($this);
        }

        return $this;
    }

    public function removeMydocument(Document $mydocument): self
    {
        if ($this->mydocuments->contains($mydocument)) {
            $this->mydocuments->removeElement($mydocument);
            // set the owning side to null (unless already changed)
            if ($mydocument->getCreatedBy() === $this) {
                $mydocument->setCreatedBy(null);
            }
        }

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }

    public function getPasswordRequestedAt(): ?\DateTimeInterface
    {
        return $this->passwordRequestedAt;
    }

    public function setPasswordRequestedAt(?\DateTimeInterface $passwordRequestedAt): self
    {
        $this->passwordRequestedAt = $passwordRequestedAt;

        return $this;
    }

    /**
     * @return Collection|MailBox[]
     */
    public function getMailBoxes(): Collection
    {
        return $this->mailBoxes;
    }

    public function addMailBox(MailBox $mailBox): self
    {
        if (!$this->mailBoxes->contains($mailBox)) {
            $this->mailBoxes[] = $mailBox;
            $mailBox->setUser($this);
        }

        return $this;
    }

    public function removeMailBox(MailBox $mailBox): self
    {
        if ($this->mailBoxes->contains($mailBox)) {
            $this->mailBoxes->removeElement($mailBox);
            // set the owning side to null (unless already changed)
            if ($mailBox->getUser() === $this) {
                $mailBox->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DocumentMail[]
     */
    public function getDocumentMails(): Collection
    {
        return $this->documentMails;
    }

    public function addDocumentMail(DocumentMail $documentMail): self
    {
        if (!$this->documentMails->contains($documentMail)) {
            $this->documentMails[] = $documentMail;
            $documentMail->setUser($this);
        }

        return $this;
    }

    public function removeDocumentMail(DocumentMail $documentMail): self
    {
        if ($this->documentMails->contains($documentMail)) {
            $this->documentMails->removeElement($documentMail);
            // set the owning side to null (unless already changed)
            if ($documentMail->getUser() === $this) {
                $documentMail->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DocumentOpened[]
     */
    public function getDocumentOpeneds(): Collection
    {
        return $this->documentOpeneds;
    }

    public function addDocumentOpened(DocumentOpened $documentOpened): self
    {
        if (!$this->documentOpeneds->contains($documentOpened)) {
            $this->documentOpeneds[] = $documentOpened;
            $documentOpened->setUser($this);
        }

        return $this;
    }

    public function removeDocumentOpened(DocumentOpened $documentOpened): self
    {
        if ($this->documentOpeneds->contains($documentOpened)) {
            $this->documentOpeneds->removeElement($documentOpened);
            // set the owning side to null (unless already changed)
            if ($documentOpened->getUser() === $this) {
                $documentOpened->setUser(null);
            }
        }

        return $this;
    }

    public function getLastLogin(): ?\DateTimeInterface
    {
        return $this->lastLogin;
    }

    public function setLastLogin(?\DateTimeInterface $lastLogin): self
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

}
