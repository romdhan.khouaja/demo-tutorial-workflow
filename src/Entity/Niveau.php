<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NiveauRepository")
 * @UniqueEntity(fields={"libelle_niveau_fr"} ,message="Nom de Niveau déja existant!!")
 * @UniqueEntity(fields={"libelle_niveau_ar"},message="Nom de Niveau déja existant!!")
 * @UniqueEntity(fields={"code_niveau"},message="Code Niveau déja existant!!")
  */
class Niveau
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le code est obligatoire")
     */
    private $code_niveau;


    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le libéllé français est obligatoire")
     */
    private $libelle_niveau_fr;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le libéllé français est obligatoire")
     */
    private $libelle_niveau_ar;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Filiere", inversedBy="niveaux")
     * @Assert\NotBlank(message="il faut choisissez un Filiére")
     */
    private $filiere;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Matiere", mappedBy="niveau")
     */
    private $matieres;

    private $libelle_filiere;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Examen", mappedBy="niveau")
     */
    private $examens;

    public function __construct()
    {
        $this->matieres = new ArrayCollection();
        $this->examens = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getCodeNiveau(): ?string
    {
        return $this->code_niveau;
    }

    public function setCodeNiveau(string $code_niveau): self
    {
        $this->code_niveau = $code_niveau;

        return $this;
    }



    public function getLibelleNiveauFr(): ?string
    {
        return $this->libelle_niveau_fr;
    }

    public function setLibelleNiveauFr(string $libelle_niveau_fr): self
    {
        $this->libelle_niveau_fr = $libelle_niveau_fr;

        return $this;
    }

    public function getLibelleNiveauAr(): ?string
    {
        return $this->libelle_niveau_ar;
    }

    public function setLibelleNiveauAr(string $libelle_niveau_ar): self
    {
        $this->libelle_niveau_ar = $libelle_niveau_ar;

        return $this;
    }

    public function getFiliere(): ?Filiere
    {
        return $this->filiere;
    }

    public function setFiliere(?Filiere $filiere): self
    {
        $this->filiere = $filiere;

        return $this;
    }

    /**
     * @return Collection|Matiere[]
     */
    public function getMatieres(): Collection
    {
        return $this->matieres;
    }

    public function addMatiere(Matiere $matiere): self
    {
        if (!$this->matieres->contains($matiere)) {
            $this->matieres[] = $matiere;
            $matiere->setNiveau($this);
        }

        return $this;
    }

    public function removeMatiere(Matiere $matiere): self
    {
        if ($this->matieres->contains($matiere)) {
            $this->matieres->removeElement($matiere);
            // set the owning side to null (unless already changed)
            if ($matiere->getNiveau() === $this) {
                $matiere->setNiveau(null);
            }
        }

        return $this;
    }
    public function __toString(){
        // to show the name of the Category in the select
        return $this->libelle_niveau_ar;
        // to show the id of the Category in the select
        // return $this->id;
    }
    public function getLibelleFiliere(): ?string
    {
        return $this->libelle_niveau_fr."  [ ".$this->libelle_niveau_ar."]";
    }

    /**
     * @return Collection|Examen[]
     */
    public function getExamens(): Collection
    {
        return $this->examens;
    }

    public function addExamen(Examen $examen): self
    {
        if (!$this->examens->contains($examen)) {
            $this->examens[] = $examen;
            $examen->setNiveau($this);
        }

        return $this;
    }

    public function removeExamen(Examen $examen): self
    {
        if ($this->examens->contains($examen)) {
            $this->examens->removeElement($examen);
            // set the owning side to null (unless already changed)
            if ($examen->getNiveau() === $this) {
                $examen->setNiveau(null);
            }
        }

        return $this;
    }
}
