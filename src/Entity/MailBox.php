<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MailBoxRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class MailBox
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le sujet est obligatoire")
     */
    private $subject;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\NotBlank(message="Le message est obligatoire")
     */
    private $message;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Enseignant", inversedBy="mailBoxes")
     *
     */
    private $enseignant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="mailBoxes")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DocumentMail", cascade={"persist", "remove"},mappedBy="mailBox")
     * @Assert\NotBlank(message="Il faut choisir une fichier")
     */
    private $documents;

    public function __construct()
    {
        $this->enseignant = new ArrayCollection();
        $this->documents = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $message): self
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return Collection|Enseignant[]
     */
    public function getEnseignant(): Collection
    {
        return $this->enseignant;
    }

    public function addEnseignant(Enseignant $enseignant): self
    {
        if (!$this->enseignant->contains($enseignant)) {
            $this->enseignant[] = $enseignant;
        }

        return $this;
    }

    public function removeEnseignant(Enseignant $enseignant): self
    {
        if ($this->enseignant->contains($enseignant)) {
            $this->enseignant->removeElement($enseignant);
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|DocumentMail[]
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    public function addDocument(DocumentMail $document): self
    {
        if (!$this->documents->contains($document)) {
            $this->documents[] = $document;
            $document->setMailBox($this);
        }

        return $this;
    }

    public function removeDocument(DocumentMail $document): self
    {
        if ($this->documents->contains($document)) {
            $this->documents->removeElement($document);
            // set the owning side to null (unless already changed)
            if ($document->getMailBox() === $this) {
                $document->setMailBox(null);
            }
        }

        return $this;
    }

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context)

    {

        /*if(count($this->getDocuments())== 0){
            $context->buildViolation("Il faut choisir un fichier(s)")
                ->atPath('documents')
                ->addViolation();

        }*/
        if (count($this->getEnseignant()) == 0) {
            $context->buildViolation("Il faut choisir un enseignant(s)")
                ->atPath('enseignant')
                ->addViolation();

        }

    }
}
