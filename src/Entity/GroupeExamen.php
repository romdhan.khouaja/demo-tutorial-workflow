<?php

namespace App\Entity;



use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GroupeExamenRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(fields={"libellegroupe"} ,message="libellé groupe déja existant!!")
 */
class GroupeExamen
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message="Le nombre étudiant est obligatoire")
     */
    private $nbretudiantcentre;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le libéllé groupe est obligatoire")
     */
    private $libellegroupe;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Examen", inversedBy="groupeExamens")
     */
    private $examen;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CentreExamen", inversedBy="groupeExamens")
     * @Assert\NotBlank(message="il faut choisissez un centre d'examen")
     */
    private $centreexamen;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Enseignant", inversedBy="groupeExamens")
     */
    private $enseignant;

    private $nbElevRest;
    private $oldNbElevSaisie;
    private $typeForm;

    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    private $matretuddebut;

    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    private $matretudfin;


    public function __construct()
    {
        $this->enseignant = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNbretudiantcentre(): ?int
    {
        return $this->nbretudiantcentre;
    }

    public function setNbretudiantcentre(int $nbretudiantcentre): self
    {
        $this->nbretudiantcentre = $nbretudiantcentre;

        return $this;
    }

    public function getNbElevRest(): ?int
    {
        return $this->nbElevRest;
    }

    public function setNbElevRest(int $nbElevRest): self
    {
        $this->nbElevRest = $nbElevRest;

        return $this;
    }

    public function getOldNbElevSaisie(): ?int
    {
        return $this->oldNbElevSaisie;
    }

    public function setOldNbElevSaisie(int $oldNbElevSaisie): self
    {
        $this->oldNbElevSaisie = $oldNbElevSaisie;

        return $this;
    }

    public function getLibellegroupe(): ?string
    {
        return $this->libellegroupe;
    }

    public function setLibellegroupe(string $libellegroupe): self
    {
        $this->libellegroupe = $libellegroupe;

        return $this;
    }

    public function getTypeForm(): ?string
    {
        return $this->typeForm;
    }

    public function setTypeForm(string $typeForm): self
    {
        $this->typeForm = $typeForm;

        return $this;
    }

    public function getExamen(): ?Examen
    {
        return $this->examen;
    }

    public function setExamen(?Examen $examen): self
    {
        $this->examen = $examen;

        return $this;
    }

    public function getCentreexamen(): ?CentreExamen
    {
        return $this->centreexamen;
    }

    public function setCentreexamen(?CentreExamen $centreexamen): self
    {
        $this->centreexamen = $centreexamen;

        return $this;
    }

    /**
     * @return Collection|Enseignant[]
     */
    public function getEnseignant(): Collection
    {
        return $this->enseignant;
    }

    public function addEnseignant(Enseignant $enseignant): self
    {
        if (!$this->enseignant->contains($enseignant)) {
            $this->enseignant[] = $enseignant;
        }

        return $this;
    }

    public function removeEnseignant(Enseignant $enseignant): self
    {
        if ($this->enseignant->contains($enseignant)) {
            $this->enseignant->removeElement($enseignant);
        }

        return $this;
    }

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context)
    {
        if ($this->getTypeForm() == 'add_edit') {
            if ($this->getCentreexamen() and ($this->getNbretudiantcentre() > $this->getCentreexamen()->getCapacite())) {
                $context->buildViolation("La salle ne supporte que " . $this->getCentreexamen()->getCapacite())
                    ->atPath('nbretudiantcentre')
                    ->addViolation();
            }

            if ($this->getNbElevRest() > 0 and ($this->getNbElevRest() < $this->getNbretudiantcentre())) {
                $context->buildViolation("il vous reste que " . $this->getNbElevRest() . " étudiants")
                    ->atPath('nbretudiantcentre')
                    ->addViolation();
            }
            if ($this->getNbElevRest() == 0 and ($this->getNbretudiantcentre() > $this->getOldNbElevSaisie())) {
                $context->buildViolation("Tous les étudiant sont affétés, vous pouvez pas dépassez :" . $this->getOldNbElevSaisie())
                    ->atPath('nbretudiantcentre')
                    ->addViolation();
            }
            if ($this->getNbretudiantcentre() > 60) {
                $nb_ens = 3;
            } elseif ($this->getNbretudiantcentre() > 20) {
                $nb_ens = 2;
            } else {
                $nb_ens = 1;
            }
            if (count($this->getEnseignant()) != $nb_ens) {
                $context->buildViolation("Il faut choisir  : " . $nb_ens . " enseignants")
                    ->atPath('enseignant')
                    ->addViolation();
            }

        }

        if ($this->getTypeForm() == 'matricule') {

            if ($this->getMatretuddebut()=='') {
                $context->buildViolation("La matricule de début est obligatoire")
                    ->atPath('matretuddebut')
                    ->addViolation();
            }
            if ($this->getMatretudfin()=='') {
                $context->buildViolation("La matricule de fin est obligatoire")
                    ->atPath('matretudfin')
                    ->addViolation();
            }
            if ($this->getMatretuddebut() != '' and $this->getMatretudfin()!='' and $this->getMatretuddebut() >= $this->getMatretudfin()) {
                $context->buildViolation("La matricule de fin doit etre superieur a celle de début")
                    ->atPath('matretudfin')
                    ->addViolation();
            }
        }

    }

    public function getMatretuddebut(): ?int
    {
        return $this->matretuddebut;
    }

    public function setMatretuddebut(int $matretuddebut): self
    {
        $this->matretuddebut = $matretuddebut;

        return $this;
    }

    public function getMatretudfin(): ?int
    {
        return $this->matretudfin;
    }

    public function setMatretudfin(int $matretudfin): self
    {
        $this->matretudfin = $matretudfin;

        return $this;
    }


}
