<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AnneeUniversitaireRepository")
 * @UniqueEntity(fields={"libelleannee"} ,message="libellé déja existant!!")
 */
class AnneeUniversitaire
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le libéllé est obligatoire")
     */
    private $libelleannee;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Session", mappedBy="annee")
     */
    private $sessions;

    public function __construct()
    {
        $this->semestres = new ArrayCollection();
        $this->sessions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelleannee(): ?string
    {
        return $this->libelleannee;
    }

    public function setLibelleannee(string $libelleannee): self
    {
        $this->libelleannee = $libelleannee;

        return $this;
    }

    public function __toString()
    {
        // to show the name of the Category in the select
        return $this->libelleannee;
        // to show the id of the Category in the select
        // return $this->id;
    }

    /**
     * @return Collection|Session[]
     */
    public function getSessions(): Collection
    {
        return $this->sessions;
    }

    public function addSession(Session $session): self
    {
        if (!$this->sessions->contains($session)) {
            $this->sessions[] = $session;
            $session->setAnnee($this);
        }

        return $this;
    }

    public function removeSession(Session $session): self
    {
        if ($this->sessions->contains($session)) {
            $this->sessions->removeElement($session);
            // set the owning side to null (unless already changed)
            if ($session->getAnnee() === $this) {
                $session->setAnnee(null);
            }
        }

        return $this;
    }
}
