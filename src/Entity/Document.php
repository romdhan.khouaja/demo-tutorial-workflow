<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo; // gedmo annotations
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DocumentRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(fields={"name"}, message=" nom et deja utiliser")
 * @Vich\Uploadable
 */
class Document
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le nom est obligatoire")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank(message="La description est obligatoire")
     */
    private $description;
    /**
     * @Vich\UploadableField(mapping="doc_excel", fileNameProperty="excelName")
     * @Assert\NotBlank(message="Il faut choisir une fichier")
     * @var File
     */
    private $excelFile;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $excelName;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;
    /**
     * @ORM\Column(type="boolean")
     */
    private $isSend;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="documents")
     * @Assert\NotBlank(message="Il faut choisir utilisateur(s)")
     */
    private $users;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="mydocuments")
     */
    private $createdBy;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DocumentOpened", mappedBy="document")
     */
    private $documentOpeneds;


    public function __construct()
    {
        $this->isSend = false;
        $this->users = new ArrayCollection();
        $this->documentOpeneds = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }


    /*
   * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $excelFile
   */
    public function setExcelFile(?File $excelFile = null): void
    {
        $this->excelFile = $excelFile;
        if (null !== $excelFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getExcelFile(): ?File
    {
        return $this->excelFile;
    }

    public function setExcelName(?string $excelName): void
    {
        $this->excelName = $excelName;
    }

    public function getExcelName(): ?string
    {
        return $this->excelName;
    }

    public function __toString()
    {
        return (string)$this->getName();
    }

    public function getIsSend(): ?bool
    {
        return $this->isSend;
    }

    public function setIsSend(bool $isSend): self
    {
        $this->isSend = $isSend;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
        }

        return $this;
    }

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?User $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return Collection|DocumentOpened[]
     */
    public function getDocumentOpeneds(): Collection
    {
        return $this->documentOpeneds;
    }

    public function addDocumentOpened(DocumentOpened $documentOpened): self
    {
        if (!$this->documentOpeneds->contains($documentOpened)) {
            $this->documentOpeneds[] = $documentOpened;
            $documentOpened->setDocument($this);
        }

        return $this;
    }

    public function removeDocumentOpened(DocumentOpened $documentOpened): self
    {
        if ($this->documentOpeneds->contains($documentOpened)) {
            $this->documentOpeneds->removeElement($documentOpened);
            // set the owning side to null (unless already changed)
            if ($documentOpened->getDocument() === $this) {
                $documentOpened->setDocument(null);
            }
        }

        return $this;
    }

    /**
     * Permet de savoir si un utilisateur a vu le document
     *
     * @param User $user
     * @return boolean
     */
    public function isOpenedByUser(User $user): bool
    {
        foreach ($this->documentOpeneds as $opened) {
            if ($opened->getUser() === $user) {
                return true;
            }
        }
        return false;
    }

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context)

    {

        if(count($this->getUsers())== 0){
            $context->buildViolation("Il faut choisir Utilsateur(s)")
                ->atPath('users')
                ->addViolation();

        }

    }

}
