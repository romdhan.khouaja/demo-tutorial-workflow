<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TypeMatiereRepository")
 *  @UniqueEntity(fields={"libelle_type_ar"},message="Type déja existant!!")
 * @UniqueEntity(fields={"libelle_type_fr"},message="Type déja existant!!")
 */
class TypeMatiere
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le libéllé français est obligatoire")
     */
    private $libelle_type_fr;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le libéllé arabe est obligatoire")
     */
    private $libelle_type_ar;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Matiere", mappedBy="typematiere")
     */
    private $matieres;
    private $libelle_type;

    public function __construct()
    {
        $this->matieres = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelleTypeFr(): ?string
    {
        return $this->libelle_type_fr;
    }

    public function setLibelleTypeFr(string $libelle_type_fr): self
    {
        $this->libelle_type_fr = $libelle_type_fr;

        return $this;
    }

    public function getLibelleTypeAr(): ?string
    {
        return $this->libelle_type_ar;
    }

    public function setLibelleTypeAr(string $libelle_type_ar): self
    {
        $this->libelle_type_ar = $libelle_type_ar;

        return $this;
    }

    /**
     * @return Collection|Matiere[]
     */
    public function getMatieres(): Collection
    {
        return $this->matieres;
    }

    public function addMatiere(Matiere $matiere): self
    {
        if (!$this->matieres->contains($matiere)) {
            $this->matieres[] = $matiere;
            $matiere->setTypematiere($this);
        }

        return $this;
    }

    public function removeMatiere(Matiere $matiere): self
    {
        if ($this->matieres->contains($matiere)) {
            $this->matieres->removeElement($matiere);
            // set the owning side to null (unless already changed)
            if ($matiere->getTypematiere() === $this) {
                $matiere->setTypematiere(null);
            }
        }

        return $this;
    }
    public function __toString(){
        // to show the name of the Category in the select
        return $this->libelle_type_ar;
        // to show the id of the Category in the select
        // return $this->id;
    }
    public function getLibelleType(): ?string
    {
        return $this->libelle_type_fr."  [ ".$this->libelle_type_ar."]";
    }
}
