<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\ExamenRepository")
 */
class Examen
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Correction", mappedBy="examen")
     */
    private $corrections;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message="Le nombre etudiant est obligatoire")
     */
    private $nombreetudiant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Niveau", inversedBy="examens")
     * @Assert\NotBlank(message="il faut choisissez un niveau")
     */
    private $niveau;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Matiere", inversedBy="examens")
     * @Assert\NotBlank(message="il faut choisissez une matière")
     */
    private $matiere;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Session", inversedBy="examens")
     * @Assert\NotBlank(message="il faut choisissez une session")
     */
    private $session;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank(message="L'heure est obligatoire")
     */
    private $heureexamen;

    /**
     * @ORM\Column(type="float")
     */
    private $heureexamenfin;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank(message="La date est obligatoire")
     */
    private $dateexamen;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\GroupeExamen", mappedBy="examen")
     */
    private $groupeExamens;



    public function __construct()
    {
        $this->corrections = new ArrayCollection();
        $this->groupeExamens = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Correction[]
     */
    public function getCorrections(): Collection
    {
        return $this->corrections;
    }

    public function addCorrection(Correction $correction): self
    {
        if (!$this->corrections->contains($correction)) {
            $this->corrections[] = $correction;
            $correction->setExamen($this);
        }

        return $this;
    }

    public function removeCorrection(Correction $correction): self
    {
        if ($this->corrections->contains($correction)) {
            $this->corrections->removeElement($correction);
            // set the owning side to null (unless already changed)
            if ($correction->getExamen() === $this) {
                $correction->setExamen(null);
            }
        }

        return $this;
    }

    public function getNombreetudiant(): ?int
    {
        return $this->nombreetudiant;
    }

    public function setNombreetudiant(int $nombreetudiant): self
    {
        $this->nombreetudiant = $nombreetudiant;

        return $this;
    }

    public function getNiveau(): ?Niveau
    {
        return $this->niveau;
    }

    public function setNiveau(?Niveau $niveau): self
    {
        $this->niveau = $niveau;

        return $this;
    }

    public function getMatiere(): ?Matiere
    {
        return $this->matiere;
    }

    public function setMatiere(?Matiere $matiere): self
    {
        $this->matiere = $matiere;

        return $this;
    }

    public function getSession(): ?Session
    {
        return $this->session;
    }

    public function setSession(?Session $session): self
    {
        $this->session = $session;

        return $this;
    }


    public function __toString()
    {
        return  $this->session." /".$this->matiere ;
    }

    /**
     * @return Collection|GroupeExamen[]
     */
    public function getGroupeExamens(): Collection
    {
        return $this->groupeExamens;
    }

    public function addGroupeExamen(GroupeExamen $groupeExamen): self
    {
        if (!$this->groupeExamens->contains($groupeExamen)) {
            $this->groupeExamens[] = $groupeExamen;
            $groupeExamen->setExamen($this);
        }

        return $this;
    }

    public function removeGroupeExamen(GroupeExamen $groupeExamen): self
    {
        if ($this->groupeExamens->contains($groupeExamen)) {
            $this->groupeExamens->removeElement($groupeExamen);
            // set the owning side to null (unless already changed)
            if ($groupeExamen->getExamen() === $this) {
                $groupeExamen->setExamen(null);
            }
        }

        return $this;
    }

    public function getHeureexamen(): ?float
    {
        return $this->heureexamen;
    }

    public function setHeureexamen(float $heureexamen): self
    {
        $this->heureexamen = $heureexamen;

        return $this;
    }

    public function getDateexamen(): ?\DateTimeInterface
    {
        return $this->dateexamen;
    }

    public function setDateexamen(\DateTimeInterface $dateexamen): self
    {
        $this->dateexamen = $dateexamen;

        return $this;
    }


    public function getHeureexamenfin(): ?float
    {
        return $this->heureexamenfin;
    }

    public function setHeureexamenfin(float $heureexamenfin): self
    {
        $this->heureexamenfin = $heureexamenfin;

        return $this;
    }
}
