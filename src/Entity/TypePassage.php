<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity(repositoryClass="App\Repository\TypePassageRepository")
 * @UniqueEntity(fields={"libelle_passage_fr"},message="Type de passage déja existant!!")
 * @UniqueEntity(fields={"libelle_passage_ar"},message="Type de passage déja existant!!")
 */
class TypePassage
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le libéllé français est obligatoire")
     */
    private $libelle_passage_fr;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le libéllé français est obligatoire")
     */
    private $libelle_passage_ar;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Matiere", mappedBy="typepassage")
     */
    private $matieres;

    public function __construct()
    {
        $this->matieres = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibellePassageFr(): ?string
    {
        return $this->libelle_passage_fr;
    }

    public function setLibellePassageFr(string $libelle_passage_fr): self
    {
        $this->libelle_passage_fr = $libelle_passage_fr;

        return $this;
    }

    public function getLibellePassageAr(): ?string
    {
        return $this->libelle_passage_ar;
    }

    public function setLibellePassageAr(string $libelle_passage_ar): self
    {
        $this->libelle_passage_ar = $libelle_passage_ar;

        return $this;
    }

    /**
     * @return Collection|Matiere[]
     */
    public function getMatieres(): Collection
    {
        return $this->matieres;
    }

    public function addMatiere(Matiere $matiere): self
    {
        if (!$this->matieres->contains($matiere)) {
            $this->matieres[] = $matiere;
            $matiere->setTypepassage($this);
        }

        return $this;
    }

    public function removeMatiere(Matiere $matiere): self
    {
        if ($this->matieres->contains($matiere)) {
            $this->matieres->removeElement($matiere);
            // set the owning side to null (unless already changed)
            if ($matiere->getTypepassage() === $this) {
                $matiere->setTypepassage(null);
            }
        }

        return $this;
    }
    public function __toString(){
        // to show the name of the Category in the select
        return $this->libelle_passage_ar;
        // to show the id of the Category in the select
        // return $this->id;
    }
}
