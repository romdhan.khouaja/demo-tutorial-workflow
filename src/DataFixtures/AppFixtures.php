<?php

namespace App\DataFixtures;

use App\Entity\AnneeUniversitaire;
use App\Entity\CentreExamen;
use App\Entity\Departement;
use App\Entity\Filiere;
use App\Entity\Grade;
use App\Entity\LangueMatiere;
use App\Entity\Module;
use App\Entity\Semestre;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

use App\Entity\User;

use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{

    /**
     * Encodeur de mot de passe
     *
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        $users = [];

        // tous les roles ROLE_ADMIN ROLE_AGENT ROLE_SCOLARITE ROLE_CHEF_DEPARTEMENT ROLE_DOYEN ROLE_SECRETAIRE_GENERAL
        // ROLE_ADMIN
        $user = new User();
        // departement_id username roles password firstname lastname email 	 
        $user->setUsername('admin')
            ->setRoles(['ROLE_ADMIN'])
            ->setEmail('admin@mondomaine.lan')
            ->setPassword($this->encoder->encodePassword($user, 'password'))
            ->setFirstname('admin')
            ->setLastname('admin');
        $manager->persist($user);

        $userag = new User();
        // departement_id username roles password firstname lastname email 	 
        $userag->setUsername('agent')
            ->setRoles(['ROLE_AGENT'])
            ->setEmail('agent@mondomaine.lan')
            ->setPassword($this->encoder->encodePassword($userag, 'password'))
            ->setFirstname('agent')
            ->setLastname('agent');
        $manager->persist($userag);

        // ROLE_SCOLARITE
        $usersc = new User();
        // departement_id username roles password firstname lastname email 	 
        $usersc->setUsername('scolarite')
            ->setRoles(['ROLE_SCOLARITE'])
            ->setEmail('scolarite@mondomaine.lan')
            ->setPassword($this->encoder->encodePassword($usersc, 'password'))
            ->setFirstname('scolarite')
            ->setLastname('scolarite');
        $manager->persist($usersc);

        // ROLE_CHEF_DEPARTEMENT 
        $usercd = new User();
        // departement_id username roles password firstname lastname email 	 
        $usercd->setUsername('chefdep')
            ->setRoles(['ROLE_CHEF_DEPARTEMENT'])
            ->setEmail('chefdep@mondomaine.lan')
            ->setPassword($this->encoder->encodePassword($usercd, 'password'))
            ->setFirstname('chefdep')
            ->setLastname('chefdep');
        $manager->persist($usercd);

        // ROLE_DOYEN
        $userdn = new User();
        // departement_id username roles password firstname lastname email 	 
        $userdn->setUsername('doyen')
            ->setRoles(['ROLE_DOYEN'])
            ->setEmail('doyen@mondomaine.lan')
            ->setPassword($this->encoder->encodePassword($userdn, 'password'))
            ->setFirstname('doyen')
            ->setLastname('doyen');
        $manager->persist($userdn);

        // ROLE_SECRETAIRE_GENERAL 
        $usersg = new User();
        // departement_id username roles password firstname lastname email 	 
        $usersg->setUsername('secgen')
            ->setRoles(['ROLE_SECRETAIRE_GENERAL'])
            ->setEmail('secgen@mondomaine.lan')
            ->setPassword($this->encoder->encodePassword($usersg, 'password'))
            ->setFirstname('secgen')
            ->setLastname('secgen');
        $manager->persist($usersg);

        /* 
        $users[] = $user;

        for ($i = 0; $i < 20; $i++) {
            $user = new User();
            $user->setEmail($faker->email)
                ->setPassword($this->encoder->encodePassword($user, 'password')); 
            $manager->persist($user);
            $users[] = $user;
        }
        */
        // $manager->persist($product);

        /* SEMESTRE  */
        /* $semestre1 = new Semestre();
        $semestre1->setLibelleSemestreAr("S1")->setLibelleSemestreFr("S1");
        $manager->persist($semestre1);

        $semestre2 = new Semestre();
        $semestre2->setLibelleSemestreAr("S2")->setLibelleSemestreFr("S2");
        $manager->persist($semestre2); */

        /* ANNEE UNIVERSITAIRE */
        /* $anneUn= new AnneeUniversitaire();
        $anneUn->setLibelleannee("2018/2019");
        $manager->persist($anneUn); */

        /* DEPARTEMENT */
        /* $dep1 = new Departement();
        $dep1->setLibelleDepartementAr("Droit Internationnal")
            ->setLibelleDepartementFr("Droit Internationnal");
        $dep2 = new Departement();
        $dep2->setLibelleDepartementAr("Droit Commun")
            ->setLibelleDepartementFr("Droit Commun");  
        $manager->persist($dep1);
        $manager->persist($dep2); */

        /* GRADE */
        /* $grd1 = new Grade();
        $grd1->setLibelleGradeAr("Professeur")
            ->setLibelleGradeFr("Professeur");
        $grd2 = new Grade(); 
        $grd2->setLibelleGradeAr("Maitre de conference")
            ->setLibelleGradeFr("Maitre de conference");
        $manager->persist($grd2);
        $manager->persist($grd1); */

        /* FILIERE */
        /*$flr1 = new Filiere();
        $flr1->setLibelleFiliereFr("droit universelle")
            ->setLibelleFiliereAr("droit universelle")
            ->setDepartement($dep1);

        $flr2 = new Filiere(); 
        $flr2->setLibelleFiliereFr("droit commun")
            ->setLibelleFiliereAr("droit commun")
            ->setDepartement($dep2);
        $manager->persist($flr2);
        $manager->persist($flr1);*/

        /* LANGUE */
        /* $lg1 = new LangueMatiere();
        $lg2 = new LangueMatiere();
        $lg3 = new LangueMatiere();
        $lg1->setLibelleLangueFr("arabe")
            ->setLibelleLangueAr("arabe");
        $lg2->setLibelleLangueFr("français")
            ->setLibelleLangueAr("français");
        $lg3->setLibelleLangueFr("anglais")
            ->setLibelleLangueAr("anglais");
        $manager->persist($lg1);
        $manager->persist($lg2);
        $manager->persist($lg3); */

        /* MODULE */
        /*$modules = [] ;

        for ($i = 1; $i < 20; $i++) {
            $module = new Module();
            $module->setLibelleModuleAr("module ". $i)->setLibelleModuleFr("module " . $i) ;
            $manager->persist($module);
            $modules[] = $module;
        }*/

        /* CENTRE EXAMEN */
        /*for ($i = 1; $i < 20; $i++) {
        $ce = new CentreExamen();
        $ce->setCodeCentre("code_".$i)
            ->setCapacite($i*10)
            ->setLibelleCentre("Salle ".$i);
            $manager->persist($ce);
        }*/

        $manager->flush();
    }
}
