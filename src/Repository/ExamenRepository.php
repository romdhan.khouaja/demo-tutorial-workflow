<?php

namespace App\Repository;

use App\Entity\Examen;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Examen|null find($id, $lockMode = null, $lockVersion = null)
 * @method Examen|null findOneBy(array $criteria, array $orderBy = null)
 * @method Examen[]    findAll()
 * @method Examen[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExamenRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Examen::class);
    }

    public function searchSession($data)
    {
        $queryBuilder = $this->createQueryBuilder('e');
        if (isset($data['session']) && !empty($data['session'])) {
            $queryBuilder->andWhere('e.session = :depart')
                ->setParameter('depart', $data['session']);
        }
        if (isset($data['niveau']) && !empty($data['niveau'])) {
            $queryBuilder->andWhere('e.niveau = :niveau')
                ->setParameter('niveau', $data['niveau']);
        }

        $query = $queryBuilder->getQuery();
        $results = $query->getResult();
        return $results;
    }

    public function examenCalendrier($session)
    {
        $queryBuilder = $this->createQueryBuilder('ex');
        $queryBuilder->andWhere('ex.session = :depart')
            ->setParameter('depart', $session)
            ->orderBy('ex.dateexamen');

        $query = $queryBuilder->getQuery();
        $results = $query->getResult();
        return $results;
    }

    public function examenPart($session)
    {
        $queryBuilder = $this->createQueryBuilder('e');
        $queryBuilder->leftJoin('e.corrections', 'correction')
            ->addSelect('correction')
            ->andWhere('e.session = :sess')
            ->setParameter('sess', $session);

        $query = $queryBuilder->getQuery();
        $results = $query->getResult();
        return $results;
    }

    public function examenCentre($session)
    {
        $queryBuilder = $this->createQueryBuilder('e');

        $queryBuilder->leftJoin('e.groupeExamens', 'groupeExamen')
            ->addSelect('groupeExamen')
            ->andWhere('e.session = :sess')
        ->setParameter('sess', $session);

        $query = $queryBuilder->getQuery();
        $results = $query->getResult();
        return $results;

    }

    // /**
    //  * @return Examen[] Returns an array of Examen objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Examen
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
