<?php

namespace App\Repository;

use App\Entity\TypePassage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TypePassage|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypePassage|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypePassage[]    findAll()
 * @method TypePassage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypePassageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TypePassage::class);
    }

    // /**
    //  * @return TypePassage[] Returns an array of TypePassage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypePassage
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
