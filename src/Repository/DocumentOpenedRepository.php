<?php

namespace App\Repository;

use App\Entity\DocumentOpened;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DocumentOpened|null find($id, $lockMode = null, $lockVersion = null)
 * @method DocumentOpened|null findOneBy(array $criteria, array $orderBy = null)
 * @method DocumentOpened[]    findAll()
 * @method DocumentOpened[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DocumentOpenedRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DocumentOpened::class);
    }

    // /**
    //  * @return DocumentOpened[] Returns an array of DocumentOpened objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DocumentOpened
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
