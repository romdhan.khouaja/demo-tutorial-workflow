<?php

namespace App\Repository;

use App\Entity\Enseignant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Enseignant|null find($id, $lockMode = null, $lockVersion = null)
 * @method Enseignant|null findOneBy(array $criteria, array $orderBy = null)
 * @method Enseignant[]    findAll()
 * @method Enseignant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EnseignantRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Enseignant::class);
    }

    public function searchEnseignant($data)
    {
        $queryBuilder = $this->createQueryBuilder('e');
        if (isset($data['grade']) && !empty($data['grade'])) {
            $queryBuilder->andWhere('e.grade = :depart')
                ->setParameter('depart', $data['grade']);
        }
        if (isset($data['nomprenomar']) && !empty($data['nomprenomar'])) {
            $queryBuilder->andWhere('e.nomprenomar like :nomprenomar')
                ->setParameter('nomprenomar', '%' . $data['nomprenomar'] . '%');
        }
        if (isset($data['nomprenomfr']) && !empty($data['nomprenomfr'])) {
            $queryBuilder->andWhere('e.nomprenomfr like :nomprenomfr')
                ->setParameter('nomprenomfr', '%' . $data['nomprenomfr'] . '%');
        }
        if (isset($data['code_enseignant']) && !empty($data['code_enseignant'])) {
            $queryBuilder->andWhere('e.code_enseignant like :code_enseignant')
                ->setParameter('code_enseignant', '%' . $data['code_enseignant'] . '%');
        }
        $query = $queryBuilder->getQuery();
        $results = $query->getResult();
        return $results;
    }

    public function enseignantOccupe($date_exam,$heureexamen,$heureexamenfin)
    {
        $queryBuilder = $this->createQueryBuilder('e');

        $queryBuilder->leftJoin('e.groupeExamens','groupeExamens')
            ->addSelect('groupeExamens')
            ->leftJoin('groupeExamens.examen','examen')
            ->addSelect('examen')
            ->andWhere('examen.dateexamen = :date_exam')
            ->andWhere('( examen.heureexamen <= :h_d_exam and  examen.heureexamenfin >= :h_d_exam or examen.heureexamen <= :h_f_exam and examen.heureexamenfin >= :h_f_exam)')
            ->setParameter('date_exam', $date_exam)
            ->setParameter('h_d_exam', $heureexamen)
            ->setParameter('h_f_exam', $heureexamenfin);

        $query = $queryBuilder->getQuery();
        $results = $query->getResult();
        return $results;
    }

    public function printConvocation($session_id)
    {
        $queryBuilder = $this->createQueryBuilder('e');

        $queryBuilder->leftJoin('e.groupeExamens','groupeExamens')
            ->addSelect('groupeExamens')
            ->leftJoin('groupeExamens.examen','examen')
            ->addSelect('examen')
            ->andWhere('examen.session = :session')
            ->setParameter('session', $session_id);

        $query = $queryBuilder->getQuery();
        $results = $query->getResult();
        return $results;
    }


    // /**
    //  * @return Enseignant[] Returns an array of Enseignant objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Enseignant
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
