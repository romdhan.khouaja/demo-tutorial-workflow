<?php

namespace App\Repository;

use App\Entity\GroupeExamen;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method GroupeExamen|null find($id, $lockMode = null, $lockVersion = null)
 * @method GroupeExamen|null findOneBy(array $criteria, array $orderBy = null)
 * @method GroupeExamen[]    findAll()
 * @method GroupeExamen[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GroupeExamenRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, GroupeExamen::class);
    }

    // /**
    //  * @return GroupeExamen[] Returns an array of GroupeExamen objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GroupeExamen
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
