<?php

namespace App\Repository;

use App\Entity\Niveau;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Niveau|null find($id, $lockMode = null, $lockVersion = null)
 * @method Niveau|null findOneBy(array $criteria, array $orderBy = null)
 * @method Niveau[]    findAll()
 * @method Niveau[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NiveauRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Niveau::class);
    }

    public function findAllNiveau($depart_id)
    {
        $queryBuilder = $this->createQueryBuilder('n');
        $queryBuilder->leftJoin('n.filiere', 'filiere')
            ->addSelect('filiere')
            ->andWhere('filiere.departement = :depart_id')
            ->setParameter('depart_id', $depart_id);
        $query = $queryBuilder->getQuery();
        $results = $query->getResult();
        return $results;
    }

    public function searchNiveau($data, $depart_id = null)
    {
        $queryBuilder = $this->createQueryBuilder('n');
        if ($depart_id != null) {
            $queryBuilder->leftJoin('n.filiere', 'filiere')
                ->addSelect('filiere')
                ->andWhere('filiere.departement = :depart_id')
                ->setParameter('depart_id', $depart_id);
        }

        if (isset($data['filiere']) && !empty($data['filiere'])) {
            $queryBuilder->andWhere('n.filiere = :depart')
                ->setParameter('depart', $data['filiere']);
        }
        if (isset($data['code_niveau']) && !empty($data['code_niveau'])) {
            $queryBuilder->andWhere('n.code_niveau like :code_niveau')
                ->setParameter('code_niveau', '%' . $data['code_niveau'] . '%');
        }
        if (isset($data['libelle_niveau_fr']) && !empty($data['libelle_niveau_fr'])) {
            $queryBuilder->andWhere('n.libelle_niveau_fr like :libelle_niveau_fr')
                ->setParameter('libelle_niveau_fr', '%' . $data['libelle_niveau_fr'] . '%');
        }
        if (isset($data['libelle_niveau_ar']) && !empty($data['libelle_niveau_ar'])) {
            $queryBuilder->andWhere('n.libelle_niveau_ar like :libelle_niveau_ar')
                ->setParameter('libelle_niveau_ar', '%' . $data['libelle_niveau_ar'] . '%');
        }
        $query = $queryBuilder->getQuery();
        $results = $query->getResult();
        return $results;
    }

    public function selectNiveau($session)
    {
        $queryBuilder = $this->createQueryBuilder('n')
            ->leftJoin('n.examens','examen')
            ->addSelect('examen')
            ->andWhere('examen.session = :sess')
            ->setParameter('sess', $session);

        $query = $queryBuilder->getQuery();
        $results = $query->getResult();
        return $results;
    }

    // /**
    //  * @return Niveau[] Returns an array of Niveau objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Niveau
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
