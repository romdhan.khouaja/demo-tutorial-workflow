<?php

namespace App\Repository;

use App\Entity\Matiere;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Matiere|null find($id, $lockMode = null, $lockVersion = null)
 * @method Matiere|null findOneBy(array $criteria, array $orderBy = null)
 * @method Matiere[]    findAll()
 * @method Matiere[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MatiereRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Matiere::class);
    }

    public function findAllMatiere($depart_id)
    {
        $queryBuilder = $this->createQueryBuilder('m');
        $queryBuilder->leftJoin('m.niveau', 'niveau')
            ->addSelect('niveau')
            ->leftJoin('niveau.filiere', 'filiere')
            ->addSelect('filiere')
            ->andWhere('filiere.departement = :depart_id')
            ->setParameter('depart_id', $depart_id);

        $query = $queryBuilder->getQuery();
        $results = $query->getResult();
        return $results;
    }

    public function searchMatiere($data, $depart_id = null)
    {
        $queryBuilder = $this->createQueryBuilder('m');
        if ($depart_id != null) {
            $queryBuilder->leftJoin('m.niveau', 'niveau')
                ->addSelect('niveau')
                ->leftJoin('niveau.filiere', 'filiere')
                ->addSelect('filiere')
                ->andWhere('filiere.departement = :depart_id')
                ->setParameter('depart_id', $depart_id);
        }
        if (isset($data['module']) && !empty($data['module'])) {
            $queryBuilder->andWhere('m.module = :depart')
                ->setParameter('depart', $data['module']);
        }
        if (isset($data['niveau']) && !empty($data['niveau'])) {
            $queryBuilder->andWhere('m.niveau = :niveau')
                ->setParameter('niveau', $data['niveau']);
        }
        if (isset($data['semestre']) && !empty($data['semestre'])) {
            $queryBuilder->andWhere('m.semestre = :depart')
                ->setParameter('depart', $data['semestre']);
        }
        if (isset($data['typematiere']) && !empty($data['typematiere'])) {
            $queryBuilder->andWhere('m.typematiere = :depart')
                ->setParameter('depart', $data['typematiere']);
        }
        if (isset($data['code_matiere']) && !empty($data['code_matiere'])) {
            $queryBuilder->andWhere('m.code_matiere like :code_matiere')
                ->setParameter('code_matiere', '%' . $data['code_matiere'] . '%');
        }
        if (isset($data['coefficient']) && !empty($data['coefficient'])) {
            $queryBuilder->andWhere('m.coefficient like :coefficient')
                ->setParameter('coefficient', '%' . $data['coefficient'] . '%');
        }
        if (isset($data['libelle_matiere_fr']) && !empty($data['libelle_matiere_fr'])) {
            $queryBuilder->andWhere('m.libelle_matiere_fr like :libelle_matiere_fr')
                ->setParameter('libelle_matiere_fr', '%' . $data['libelle_matiere_fr'] . '%');
        }
       /* if (isset($data['libelle_matiere_ar']) && !empty($data['libelle_matiere_ar'])) {
            $queryBuilder->andWhere('m.libelle_matiere_ar like :libelle_matiere_ar')
                ->setParameter('libelle_matiere_ar', '%' . $data['libelle_matiere_ar'] . '%');
        }*/


        $query = $queryBuilder->getQuery();
        $results = $query->getResult();
        return $results;
    }
    // /**
    //  * @return Matiere[] Returns an array of Matiere objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Matiere
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
