<?php

namespace App\Repository;

use App\Entity\CentreExamen;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CentreExamen|null find($id, $lockMode = null, $lockVersion = null)
 * @method CentreExamen|null findOneBy(array $criteria, array $orderBy = null)
 * @method CentreExamen[]    findAll()
 * @method CentreExamen[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CentreExamenRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CentreExamen::class);
    }
    public function searchCentre($data)
    {
        $queryBuilder = $this->createQueryBuilder('e');
        if(isset($data['code_centre']) && !empty($data['code_centre'])){
            $queryBuilder->andWhere('e.code_centre = :depart')
                ->setParameter('depart',$data['code_centre'] );
        }
        if(isset($data['capacite']) && !empty($data['capacite'])){
            $queryBuilder->andWhere('e.capacite like :capacite')
                ->setParameter('capacite','%'.$data['capacite'].'%' );
        }

        $query = $queryBuilder->getQuery();
        $results = $query->getResult();
        return $results;
    }

    public function centreOccupe($date_exam,$heureexamen,$heureexamenfin)
    {
        $queryBuilder = $this->createQueryBuilder('c');

        $queryBuilder->leftJoin('c.groupeExamens','groupeExamens')
            ->addSelect('groupeExamens')
            ->leftJoin('groupeExamens.examen','examen')
            ->addSelect('examen')
            ->andWhere('examen.dateexamen = :date_exam')
            ->andWhere('( examen.heureexamen <= :h_d_exam and  examen.heureexamenfin >= :h_d_exam or examen.heureexamen <= :h_f_exam and examen.heureexamenfin >= :h_f_exam)')
            ->setParameter('date_exam', $date_exam)
            ->setParameter('h_d_exam', $heureexamen)
            ->setParameter('h_f_exam', $heureexamenfin);

        $query = $queryBuilder->getQuery();
        $results = $query->getResult();
        return $results;
    }


    // /**
    //  * @return CentreExamen[] Returns an array of CentreExamen objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CentreExamen
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
