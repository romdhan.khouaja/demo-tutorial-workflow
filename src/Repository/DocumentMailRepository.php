<?php

namespace App\Repository;

use App\Entity\DocumentMail;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DocumentMail|null find($id, $lockMode = null, $lockVersion = null)
 * @method DocumentMail|null findOneBy(array $criteria, array $orderBy = null)
 * @method DocumentMail[]    findAll()
 * @method DocumentMail[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DocumentMailRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DocumentMail::class);
    }

    // /**
    //  * @return DocumentMail[] Returns an array of DocumentMail objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DocumentMail
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
