<?php

namespace App\Repository;

use App\Entity\Qualiter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Qualiter|null find($id, $lockMode = null, $lockVersion = null)
 * @method Qualiter|null findOneBy(array $criteria, array $orderBy = null)
 * @method Qualiter[]    findAll()
 * @method Qualiter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QualiterRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Qualiter::class);
    }

    // /**
    //  * @return Qualiter[] Returns an array of Qualiter objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('q.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Qualiter
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
