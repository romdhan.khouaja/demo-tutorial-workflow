<?php

namespace App\Repository;

use App\Entity\Filiere;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Filiere|null find($id, $lockMode = null, $lockVersion = null)
 * @method Filiere|null findOneBy(array $criteria, array $orderBy = null)
 * @method Filiere[]    findAll()
 * @method Filiere[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FiliereRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Filiere::class);
    }

    public function searchFiliere($data, $depart_id = null)
    {
        $queryBuilder = $this->createQueryBuilder('f');

        if($depart_id != null){
            $queryBuilder->andWhere('f.departement = :depart_id')
                ->setParameter('depart_id', $depart_id);
        }
        if(isset($data['departement']) && !empty($data['departement'])){
            $queryBuilder->andWhere('f.departement = :depart')
                ->setParameter('depart',$data['departement'] );
        }
        if(isset($data['libelle_filiere_fr']) && !empty($data['libelle_filiere_fr'])){
            $queryBuilder->andWhere('f.libelle_filiere_fr like :libelle_filiere_fr')
                ->setParameter('libelle_filiere_fr','%'.$data['libelle_filiere_fr'].'%' );
        }
        if(isset($data['libelle_filiere_ar']) && !empty($data['libelle_filiere_ar'])){
            $queryBuilder->andWhere('f.libelle_filiere_ar like :libelle_filiere_ar')
                ->setParameter('libelle_filiere_ar','%'.$data['libelle_filiere_ar'].'%' );
        }
        $query = $queryBuilder->getQuery();
        $results = $query->getResult();
        return $results;
    }
    // /**
    //  * @return Filiere[] Returns an array of Filiere objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Filiere
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
