<?php 
namespace App\EventListener;
 

use Doctrine\ORM\EntityManagerInterface; 
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

 

class LoginListener{
    private $em;
    protected $session;
    
    public function __construct(EntityManagerInterface $em, SessionInterface $session)
    {
        $this->em = $em ;
        $this->session = $session;
    }

    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        $user = $event->getAuthenticationToken()->getUser();
        $str_date='';

        if( $user->getLastLogin())
        {
            $date_derniere_connexion = $user->getLastLogin(); 
            $str_date = $date_derniere_connexion->format('d-m-Y H:i:s');
        }

        if( $user instanceof \App\Entity\User ){
            $user->setLastLogin(new \DateTime());
            $this->em->persist($user);
            $this->em->flush();
        }   

        $docsOpened = $user->getDocumentOpeneds()->count();
        $docsInterne = $user->getDocuments()->count();
        // difference entre les documents et celles consultées
        $nbr = $docsInterne - $docsOpened;
         
        if($str_date===''){
            $this->session->getFlashBag()->add('success', "Bienvenue sur le compte " );
        }else{
            $this->session->getFlashBag()->add('success', "Bienvenue sur le compte ".$user->getUsername()." , "."Vous avez ".$nbr." nouveau(x) message(s) , Votre dernière connection  : ".$str_date);
        } 
    }
}
