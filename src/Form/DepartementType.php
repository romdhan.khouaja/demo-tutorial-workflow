<?php

namespace App\Form;

use App\Entity\Departement;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DepartementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libelle_departement_fr',TextType::class, array(
                'required' => false,
                'label' => 'Nom du département en Français : ',
                'attr' => array('class'=>'form-control')))
            ->add('libelle_departement_ar',TextType::class, array(
                'required' => false,
                'label' => 'Nom du département en Arabe : ',
                'attr' => array('class'=>'form-control')))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Departement::class,
        ]);
    }
}
