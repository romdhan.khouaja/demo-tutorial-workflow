<?php

namespace App\Form;

use App\Entity\Departement;
use App\Entity\Filiere;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\NotBlank;

class FiliereType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $responsable = $options['responsable'];

        $builder
            ->add('libelle_filiere_fr', TextType::class, array(
                'required' => false,
                'empty_data' => '',
                'label' => 'Nom du Filière en Français : ',
                'attr' => array(
                    'class' => 'form-control')))
            ->add('libelle_filiere_ar', TextType::class, array(
                'required' => false,
                'label' => 'Nom du Filière en Arabe : ',
                'attr' => array('class' => 'form-control')
            ));
        if (!$responsable) {
            $builder
                ->add('departement', EntityType::class, [
                    'class' => Departement::class,
                    'constraints'=>array(
                        new NotBlank(['message'=>'Il faut choisir un département'])
                    ),
                    'required' => false,
                    'choice_label' => 'libelle_departement',
                    'placeholder' => 'Choisissez un département',

                ])
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Filiere::class,
            'responsable' => null
        ]);
    }
}
