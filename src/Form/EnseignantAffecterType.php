<?php

namespace App\Form;

use App\Entity\Enseignant;
use App\Entity\Filiere;
use App\Entity\Niveau;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvents;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class EnseignantAffecterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $responsable = $options['responsable'];

        $builder
            ->add('filiere', EntityType::class, [
                'class' => 'App\Entity\Filiere',
                'choice_label' => 'libelle_filiere_ar',
                'query_builder' => function (EntityRepository $er) use ($responsable) {
                    return $er->createQueryBuilder('f')
                        ->andWhere('f.departement = :depart_id')
                        ->setParameter('depart_id', $responsable)
                        ->orderBy('f.libelle_filiere_ar', 'ASC');
                },
                'placeholder' => 'Sélectionnez une Filière',
                'label' => 'Filière',
                'required' => false,
            ])

            ->add('niveau', ChoiceType::class, [
                'choices' => [],
                'placeholder' => 'Sélectionnez un niveau',
                'label' => 'Niveau',
                'required' => false,
            ])

            ->add('semestre', EntityType::class, [
                'class' => 'App\Entity\Semestre',
                'choice_label' => 'libelle_semestre_ar',
                'placeholder' => 'Sélectionnez un semestre',
                'label' => 'Semestre',
                'required' => false,
                'label_attr' => [
                    'class' => 'required'
                ],
            ]);


    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults([
            'responsable'=>null
        ]);
    }
}
