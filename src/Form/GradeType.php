<?php

namespace App\Form;

use App\Entity\Grade;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GradeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libelle_grade_fr',TextType::class,
                array('label' => 'Grade en Français : ',
                    'required'=>false,
                    'attr' => array('class'=>'form-control')))
            ->add('libelle_grade_ar',TextType::class,
                array('label' => 'Grade en Arabe : ',
                    'required'=>false,
                    'attr' => array('class'=>'form-control')))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Grade::class,
        ]);
    }
}
