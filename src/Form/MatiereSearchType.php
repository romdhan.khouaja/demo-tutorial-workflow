<?php

namespace App\Form;

use App\Entity\Module;
use App\Entity\Niveau;
use App\Entity\Semestre;
use App\Entity\TypeMatiere;
use App\Entity\Matiere;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class MatiereSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $responsable = $options['responsable'];
        $builder
            ->add('code_matiere', NumberType::class, array(
                    'label' => 'Code matière : ',
                    'required' => false,
                    'attr' => array(
                        'class' => 'form-control',
                        'autocomplete' => 'off'
                    )
                )
            )
            ->add('coefficient', NumberType::class, array(
                    'label' => 'Coefficient : ',
                    'required' => false,
                    'attr' => array(
                        'class' => 'form-control',
                        'autocomplete' => 'off'
                    )
                )
            )
            ->add('libelle_matiere_fr', TextType::class, array(
                    'label' => 'Nom du matière en Français : ',
                    'required' => false,
                    'attr' => array(
                        'class' => 'form-control',
                        'autocomplete' => 'off'
                    )
                )
            )
            ->add('module', EntityType::class, [
                'class' => Module::class,
                'required' => false,
                'choice_label' => 'libelle_module',
                'placeholder' => 'Choisissez un module',

            ])
            ->add('semestre', EntityType::class, [
                'class' => Semestre::class,
                'required' => false,
                'choice_label' => 'libelle_semestre',
                'placeholder' => 'Choisissez une semestre',

            ])
            ->add('typematiere', EntityType::class, [
                'class' => TypeMatiere::class,
                'label' => 'Type matière : ',
                'required' => false,
                'choice_label' => 'libelle_type',
                'placeholder' => 'Choisissez un Type',

            ]);
            if ($responsable) {
                $builder
                    ->add('niveau', EntityType::class, [
                        'class' => Niveau::class,
                        'required' => false,
                        'query_builder' => function (EntityRepository $er) use ($responsable) {
                            return $er->createQueryBuilder('m')
                                ->leftJoin('m.filiere', 'filiere')
                                ->addSelect('filiere')
                                ->andWhere('filiere.departement = :depart_id')
                                ->setParameter('depart_id', $responsable);

                        },
                        'placeholder' => 'Choisissez un Niveau',

                    ]);
            } else {
                $builder
                    ->add('niveau', EntityType::class, [
                        'class' => Niveau::class,
                        'required' => false,
                        'placeholder' => 'Choisissez un Niveau',

                    ]);

            }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'responsable' => null

        ]);
    }
}
