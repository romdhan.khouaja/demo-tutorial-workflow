<?php

namespace App\Form;

use App\Entity\Configuration;

use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ConfigurationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nomprenomdoyen',TextType::class,array(
                'required' => false,
                'empty_data' => '',
                'label' => 'Nom Prenom du doyen : ',
                'attr' => array(
        'class' => 'form-control')))
            ->add('adresse',TextType::class,array(
                'required' => false,
                'empty_data' => '',
                'label' => 'Adresse Faculter : ',
                'attr' => array(
                    'class' => 'form-control')))
            ->add('Telfaculter',IntegerType::class,array(
                'required' => false,
                'empty_data' => '',
                'label' => 'Telephone Faculter : ',
                'attr' => array(
        'class' => 'form-control')))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Configuration::class,
        ]);
    }
}
