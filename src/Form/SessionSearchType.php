<?php

namespace App\Form;


use App\Entity\Niveau;
use App\Entity\Session;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class SessionSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder->add('session', EntityType::class, [
            'class' => Session::class,
            'required' => false,
            'choice_label' => 'libelle_session_semestre',
            'placeholder' => 'Choisissez une session',

        ]);
        $builder->add('niveau', EntityType::class, [
            'class' => Niveau::class,
            'required' => false,
            'placeholder' => 'Choisissez un niveau',
           /* 'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('e')
                    ->andWhere('e.niveau = :niveau_id')
                    ->setParameter('niveau_id', '')
                    ->orderBy('e.libelle_niveau_ar', 'ASC');
            },*/

        ]);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([

        ]);
    }
}
