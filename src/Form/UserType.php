<?php

namespace App\Form;

use App\Entity\Departement;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $roles=array('admin'=>'ROLE_ADMIN','Chef Departement'=>'ROLE_CHEF_DEPARTEMENT','Doyen'=>'ROLE_DOYEN','Secretaire General'=>'ROLE_SECRETAIRE_GENERAL','Scolarite'=>'ROLE_SCOLARITE','Agent'=>'ROLE_AGENT');
        $builder
            ->add('username',TextType::class, array(
                'required' => false,
                'empty_data'=>'',
                'label' => 'Login: ',
                'attr' => array(
                    'class' => 'form-control')))
            ->add('plainPassword', RepeatedType::class, [
              // instead of being set onto the object directly,
              // this is read and encoded in the controller
              'type' => PasswordType::class,
              'mapped'=>false,
              'constraints' => [
                  new NotBlank([
                      'message' => 'Veuillez taper votre mot de passe',
                  ]),
                  new Length([
                      'min' => 6,
                      'minMessage' => 'Votre mot de passe doit etre au minimun 6 caractéres',
                      // max length allowed by Symfony for security reasons
                      'max' => 4096,
                  ]),
              ],
              'first_options'=>['label'=>'Mot de passe'],
              'second_options'=>['label'=>'Connfirmer mot de passe'],
          ])
            ->add('roles', CollectionType::class, [
                'entry_type' => ChoiceType::class,
                'entry_options' => [
                    'label' => false,
                    'choices' => [
                        'Administrateur' => 'ROLE_ADMIN',
                        'Doyen' => 'ROLE_DOYEN',
                        'Secretaire General' => 'ROLE_SECRETAIRE_GENERAL',
                        'Chef de departement' => 'ROLE_CHEF_DEPARTEMENT',
                        'Service Scolarité' => 'ROLE_SCOLARITE',
                        'Agent Scolarité' => 'ROLE_AGENT',

                    ]]])


            ->add('isActive')
            ->add('firstname',TextType::class, array(
                'required' => false,
                'label' => 'Nom: ',
                'attr' => array(
                    'class' => 'form-control')))
            ->add('lastname',TextType::class, array(
                'required' => false,
                'label' => 'Prénom: ',
                'attr' => array(
                    'class' => 'form-control')))
            ->add('email',EmailType::class, array(
                'label' => 'Adresse Mail ',
                'required'=>false,
                'attr' => array(
                    'class' => 'form-control',
                    'autocomplete'=>'off'
                )
            ))
            ->add('departement',EntityType::class, [
                'class' => Departement::class,
                'required'=>false,
                'placeholder' => 'Choisissez une département'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
