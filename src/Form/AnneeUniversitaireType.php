<?php

namespace App\Form;

use App\Entity\AnneeUniversitaire;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AnneeUniversitaireType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libelleannee',TextType::class, array(
                'required' => false,
                'label' => 'Libellé Année Universitaire : ',
                'attr' => array(
                    'class' => 'form-control')))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AnneeUniversitaire::class,
        ]);
    }
}
