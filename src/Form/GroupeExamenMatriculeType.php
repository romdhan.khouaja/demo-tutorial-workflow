<?php

namespace App\Form;

use App\Entity\CentreExamen;
use App\Entity\Enseignant;
use App\Entity\GroupeExamen;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class GroupeExamenMatriculeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('matretuddebut', IntegerType::class, array(
                'required' => false,
                'label' => 'Matricule de debut : ',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'form-control')))
            ->add('matretudfin', IntegerType::class, array(
                'required' => false,
                'label' => 'Matricule de fin : ',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'form-control')))
            ->add('typeForm', HiddenType::class, array(
                'required' => false,
                'attr' => array(
                    'class' => 'form-control',
                    'autocomplete'=>'off')))


        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => GroupeExamen::class,
        ]);
    }
}
