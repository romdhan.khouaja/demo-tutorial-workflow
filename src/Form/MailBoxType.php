<?php

namespace App\Form;

use App\Entity\Enseignant;
use App\Entity\MailBox;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Validator\Constraints\File;

class MailBoxType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('enseignant', EntityType::class, [
                'class' => Enseignant::class,
                'choice_label' => 'nomprenomar',
                'multiple' => true,
                'required' => false,
                'label' => 'Choisissez enseignant(s)',
                'placeholder' => 'Choisissez enseignant-s',
                'attr' => array(
                    'class' => 'form form-control'),

            ])
            ->add('subject', TextType::class, array(
                'required' => false,
                'label' => 'Sujet',
                'attr' => array(
                    'class' => 'form-control',
                    'autocomplete' => 'off')))
            ->add('message', TextareaType::class, array(
                'required' => false, 'attr' => array(
                    'class' => 'form-control',
                    'autocomplete' => 'off')))
            ->
            add('documents', FileType::class, [
                'mapped' => false,
                'required' => false,
                'multiple' => true,]);
        /*'constraints' => [
 new File([
     'maxSize' => '1024k',

     'mimeTypesMessage' => 'Please upload a valid PDF document',
 ])
     ]]);*/
        /*  ->add('Envoie', SubmitType::class, [
              'attr' => [
                  'class' => 'btn btn-success'
              ]
          ]);*/
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MailBox::class,
        ]);
    }
}
