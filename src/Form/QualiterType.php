<?php

namespace App\Form;

use App\Entity\Qualiter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QualiterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libelle_qualite_fr',TextType::class,
                array('label' => 'Libelle qualiter en français: ',
                    'required'=>false,
                    'attr' => array('class'=>'form-control')))
            ->add('libelle_qualite_ar',TextType::class,
                array('label' => 'Libelle qualiter en arabe ',
                    'required'=>false,
                    'attr' => array('class'=>'form-control')))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Qualiter::class,
        ]);
    }
}
