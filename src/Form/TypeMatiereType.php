<?php

namespace App\Form;

use App\Entity\TypeMatiere;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TypeMatiereType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libelle_type_fr', TextType::class,
                array('required'=>false,
                    'label' => 'Libelle Type en Français: ',
                    'attr' => array('class'=>'form-control')))
            ->add('libelle_type_ar',TextType::class,
                array('required'=>false,
                    'label' => 'Libelle Type en arabe: ',
                    'attr' => array('class'=>'form-control')))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TypeMatiere::class,
        ]);
    }
}
