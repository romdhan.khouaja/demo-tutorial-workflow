<?php

namespace App\Form;

use App\Entity\AnneeUniversitaire;
use App\Entity\Semestre;
use App\Entity\Session;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SessionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('codesession', TextType::class, array(
                'required' => false,
                'label' => 'Code Session: ',
                'attr' => array(
                    'autocomplete'=>'off',
                    'class' => 'form-control')))
            ->add('libellesession', TextType::class, array(
                'required' => false,
                'label' => 'Libellé Session : ',
                'attr' => array(
                    'autocomplete'=>'off',
                    'class' => 'form-control')))
            ->add('semestre', EntityType::class, [
                'class' => Semestre::class,
                'required' => false,
                'placeholder' => 'Choisissez une Semestre',

            ])
            ->add('Annee', EntityType::class, [
                'class' => AnneeUniversitaire::class,
                'required' => false,
                'placeholder' => 'Choisissez une Année',

            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Session::class,
        ]);
    }
}
