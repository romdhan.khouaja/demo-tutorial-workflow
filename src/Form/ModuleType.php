<?php

namespace App\Form;

use App\Entity\Module;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ModuleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libelle_module_fr',TextType::class, array(
                'required' => false,
                'label' => 'Nom du Module en Français : ',
                'attr' => array(
                    'class' => 'form-control')))
            ->add('libelle_module_ar',TextType::class, array(
                'required' => false,
                'label' => 'Nom du Module en Arabe : ',
                'attr' => array(
                    'class' => 'form-control')))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Module::class,
        ]);
    }
}
