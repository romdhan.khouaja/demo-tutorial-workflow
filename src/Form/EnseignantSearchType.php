<?php

namespace App\Form;

use App\Entity\Departement;
use App\Entity\Enseignant;
use App\Entity\Grade;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class EnseignantSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code_enseignant', TextType::class, array(
                    'label' => 'Code enseignant : ',
                    'required'=>false,
                    'attr' => array(
                        'class' => 'form-control',
                        'autocomplete'=>'off'
                    )
                )
            )
            ->add('nomprenomfr', TextType::class, array(
                    'label' => 'Nom prenom en Français : ',
                    'required'=>false,
                    'attr' => array(
                        'class' => 'form-control',
                        'autocomplete'=>'off'
    )
                )
            )

            ->add('grade', EntityType::class, [
                'class' => Grade::class,
                'required'=>false,
                'choice_label' => 'libelle_grade',
                'placeholder' => 'Choisissez un grade',

            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([

        ]);
    }
}
