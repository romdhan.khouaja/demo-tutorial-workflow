<?php

namespace App\Form;

use App\Entity\Niveau;
use App\Entity\Filiere;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class NiveauSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $responsable = $options['responsable'];
        $builder
            ->add('code_niveau', TextType::class, array(
                    'label' => 'Code Niveau : ',
                    'required'=>false,
                    'attr' => array(
                        'class' => 'form-control',
                        'autocomplete'=>'off'
                    )
                )
            )
            ->add('libelle_niveau_fr', TextType::class, array(
                    'label' => 'Nom  en Français : ',
                    'required'=>false,
                    'attr' => array(
                        'class' => 'form-control',
                        'autocomplete'=>'off'
    )
                )
            )
            ->add('libelle_niveau_ar', TextType::class, array(
                    'label' => 'Nom  en Arabe : ',
                    'required'=>false,
                    'attr' => array(
                        'class' => 'form-control',
                        'autocomplete'=>'off'
                    )
                )
            );
        if($responsable){
            $builder
                ->add('filiere', EntityType::class, [
                    'class' => Filiere::class,
                    'query_builder' => function (EntityRepository $er) use ($responsable) {
                        return $er->createQueryBuilder('f')
                            ->andWhere('f.departement = :depart_id')
                            ->setParameter('depart_id', $responsable)
                            ->orderBy('f.libelle_filiere_ar', 'ASC');
                    },
                    'required'=>false,
                    'choice_label' => 'libelle_filiere',
                    'placeholder' => 'Choisissez un Filière',

                ]);
        }else{
            $builder
                ->add('filiere', EntityType::class, [
                    'class' => Filiere::class,
                    'required'=>false,
                    'choice_label' => 'libelle_filiere',
                    'placeholder' => 'Choisissez un Filière',

                ]);
        }


    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'responsable' => null
        ]);
    }
}
