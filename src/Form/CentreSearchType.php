<?php

namespace App\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class CentreSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code_centre', TextType::class, array(
                    'label' => 'Code centre : ',
                    'required'=>false,
                    'attr' => array(
                        'class' => 'form-control',
                        'autocomplete'=>'off'
                    )
                )
            )
            ->add('capacite', IntegerType::class, array(
                    'label' => 'Capacité : ',
                    'required'=>false,
                    'attr' => array(
                        'class' => 'form-control',
                        'autocomplete'=>'off'
    )
                )
            )

            ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([

        ]);
    }
}
