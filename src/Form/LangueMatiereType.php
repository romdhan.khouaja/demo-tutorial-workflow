<?php

namespace App\Form;

use App\Entity\LangueMatiere;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LangueMatiereType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libelle_langue_fr',TextType::class,
                array('label' => 'Langue en Français : ',
                    'required'=>false,
                    'attr' => array('class'=>'form-control')))
            ->add('libelle_langue_ar',TextType::class,
                array('label' => 'Langue en Arabe : ',
                    'required'=>false,
                    'attr' => array('class'=>'form-control')))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => LangueMatiere::class,
        ]);
    }
}
