<?php

namespace App\Form;

use App\Entity\Correction;
use App\Entity\Enseignant;
use Doctrine\DBAL\Types\FloatType;
use PhpParser\Node\Expr\Cast\Double;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;



class CorrectionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('part',ChoiceType::class,[
                'placeholder' => false,
                'required' => false,
                'label' => 'Part',
                'choices'=>['0.25'=>'0.25','0.5'=>'0.5','0.75'=>'0.75','1'=>'1'],
            ])
            ->add('nbPartRest',HiddenType::class,[
                'required' => false,
                'label' => false,
            ])
            ->add('enseignant',EntityType::class,[
                'class' => Enseignant::class,
                'required' => false,
                'placeholder' => 'Choisissez un enseignant',

            ])
           // ->add('examen')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Correction::class,
        ]);
    }
}
