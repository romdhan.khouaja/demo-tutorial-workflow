<?php

namespace App\Form;

use App\Entity\Enseignant;
use App\Entity\Grade;
use App\Entity\Qualiter;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class EnseignantType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nomprenomfr',TextType::class,
                array('required' => false,
                    'label' => 'Nom et Prenom en Français : ',
                    'attr' => array('class'=>'form-control')))
            ->add('nomprenomar',TextType::class,
                array('required' => false,
                    'label' => 'Nom et Prenom en Arabe : ',
                    'attr' => array('class'=>'form-control')))
            ->add('code_enseignant',TextType::class,
                array('required' => false,
                    'label' => 'Code enseignant : ',
                    'attr' => array('class'=>'form-control')))
            ->add('casier',NumberType::class,
                array('required' => false,
                    'label' => 'Casier : ',
                    'attr' => array('class'=>'form-control')))
            ->add('cin',NumberType::class,
                array('required' => false,
                    'label' => 'Cin : ',
                    'attr' => array('class'=>'form-control')))
            ->add('matricule',NumberType::class,
                array('required' => false,
                    'label' => 'Matricule : ',
                    'attr' => array('class'=>'form-control')))
            ->add('email',EmailType::class,
                array('required' => false,
                    'label' => ' Email : ',
                    'attr' => array('class'=>'form-control')))
            ->add('telephone1',NumberType::class,
                array('required' => false,
                    'label' => 'Telephone 1 : ',
                    'attr' => array('class'=>'form-control')))
            ->add('telephone2',NumberType::class,
                array('required' => false,
                    'label' => 'Telephone 2 : ',
                    'attr' => array('class'=>'form-control')))
            ->add('grade',EntityType::class,[
                'class' => Grade::class,
                'required' => false,
                'placeholder' => 'Choisissez un grade',

            ])
            ->add('qualiter',EntityType::class,[
                'class' => Qualiter::class,
                'required' => false,
                'placeholder' => 'Choisissez une qualiter',

            ])
            ->add('avec_contrat')
            ->add('etat_surveille')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Enseignant::class,
        ]);
    }
}
