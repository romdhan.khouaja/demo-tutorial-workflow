<?php

namespace App\Form;

use App\Entity\Departement;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserModifType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $roles=array('admin'=>'ROLE_ADMIN','Chef Departement'=>'ROLE_CHEF_DEPARTEMENT','Doyen'=>'ROLE_DOYEN','Secretaire General'=>'ROLE_SECRETAIRE_GENERAL','Scolarite'=>'ROLE_SCOLARITE','Agent'=>'ROLE_AGENT');
        $builder
            ->add('username',TextType::class,array('label' => 'Nom d\'utilisateur: ',))
           /* ->add('plainPassword', RepeatedType::class, [
              // instead of being set onto the object directly,
              // this is read and encoded in the controller
              'type' => PasswordType::class,
              'mapped'=>false,
              'constraints' => [
                  new NotBlank([
                      'message' => 'Please enter a password',
                  ]),
                  new Length([
                      'min' => 6,
                      'minMessage' => 'Your password should be at least {{ limit }} characters',
                      // max length allowed by Symfony for security reasons
                      'max' => 4096,
                  ]),
              ],
              'first_options'=>['label'=>'Password'],
              'second_options'=>['label'=>'Confirm Password'],
          ])*/


          /*  ->add('roles', CollectionType::class, [
                'entry_type' => ChoiceType::class,
                'entry_options' => [
                    'label' => false,
                    'multiple'=> false,
                    'choices' => [
                        'Administrateur' => 'ROLE_ADMIN',
                        'Doyen' => 'ROLE_DOYEN',
                        'Secretaire General' => 'ROLE_SECRETAIRE_GENERAL',
                        'Chef de departement' => 'ROLE_CHEF_DEPARTEMENT',
                        'Service Scolarité' => 'ROLE_SCOLARITE',
                        'Agent Scolarité' => 'ROLE_AGENT',

                    ]]])*/


            ->add('isActive')
            ->add('firstname',TextType::class,array('label' => 'Nom : ',))
            ->add('lastname',TextType::class,array('label' => 'Prénom : ',))
            ->add('email',EmailType::class)

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
