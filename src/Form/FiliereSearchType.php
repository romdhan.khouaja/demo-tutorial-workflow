<?php

namespace App\Form;

use App\Entity\Departement;
use App\Entity\Filiere;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class FiliereSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $responsable = $options['responsable'];
        $builder
            ->add('libelle_filiere_fr', TextType::class, array(
                    'label' => 'Nom du Filière en Français : ',
                    'required' => false,
                    'attr' => array(
                        'class' => 'form-control',
                        'autocomplete' => 'off'
                    )
                )
            )
            ->add('libelle_filiere_ar', TextType::class, array(
                    'label' => 'Nom du Filière en Arabe : ',
                    'required' => false,
                    'attr' => array(
                        'class' => 'form-control',
                        'autocomplete' => 'off'
                    )
                )
            );
        if (!$responsable) {
            $builder->add('departement', EntityType::class, [
                'class' => Departement::class,
                'required' => false,
                'choice_label' => 'libelle_departement',
                'placeholder' => 'Choisissez un département',

            ]);
        }

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'responsable' => null
        ]);
    }
}
