<?php

namespace App\Form;

use App\Entity\Filiere;
use App\Entity\Niveau;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

class NiveauType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $responsable = $options['responsable'];
        $builder
            ->add('code_niveau',TextType::class,
                array('required' => false,
                    'label' => 'Code Niveau : ',
                    'attr' => array('class'=>'form-control')))
            ->add('libelle_niveau_fr',TextType::class,
                array('required' => false,
                    'label' => 'Libelle Niveau en Français : ',
                    'attr' => array('class'=>'form-control')))
            ->add('libelle_niveau_ar',TextType::class,
                array('required' => false,
                    'label' => 'Libelle Niveau en Arabe : ',
                    'attr' => array('class'=>'form-control')))
            ->add('filiere',EntityType::class , [
                'class' => Filiere::class,
                    'required' => false,
                'query_builder' => function (EntityRepository $er) use ($responsable) {
                    return $er->createQueryBuilder('f')
                        ->andWhere('f.departement = :depart_id')
                        ->setParameter('depart_id', $responsable)
                        ->orderBy('f.libelle_filiere_ar', 'ASC');
                },
                'placeholder' => 'Choisissez un Filiére',

                ])
        ;
        if (!$responsable) {
            $builder
        ->add('filiere',EntityType::class , [
        'class' => Filiere::class,
        'required' => false,
            'placeholder' => 'Choisissez un Filiére',

    ])
    ;
    }}

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Niveau::class,
            'responsable' => null
        ]);
    }
}
