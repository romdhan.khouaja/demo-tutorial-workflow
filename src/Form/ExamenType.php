<?php

namespace App\Form;

use App\Entity\Examen;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class ExamenType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombreetudiant', NumberType::class, array(
                'required' => false,
                'label' => 'Nombre étudiant : ',
                'attr' => array(
                    'class' => 'form-control')))
            ->add('heureexamen', NumberType::class, array(
                'required' => false,
                'label' => 'Heure Examen: ',
                'attr' => array(
                    'class' => 'form-control')))
            ->add('dateexamen', DateType::class, array(
                    'label' => 'Date Examen : ',
                )
            )
            /*->add('languematiere', EntityType::class, [
                'class' => LangueMatiere::class,
                'required' => false,
                'placeholder' => 'Choisissez un langue',

            ])
            ->add('typematiere', EntityType::class, [
                'class' => TypeMatiere::class,
                'required' => false,
                'placeholder' => 'Choisissez un Type',

            ])*/
            /*->add('niveau',EntityType::class,[
            'class' => Niveau::class,
                'required' => false,
                'placeholder' => 'Choisissez un niveau',

            ])
            ->add('matiere',EntityType::class,[
                'class' => Matiere::class,
                'required' => false,
                'placeholder' => 'Choisissez une matière',

            ])
            ->add('session',EntityType::class,[
                'class' => Session::class,
                'required' => false,
                'placeholder' => 'Choisissez une session',

            ])*/
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Examen::class,
        ]);
    }
}
