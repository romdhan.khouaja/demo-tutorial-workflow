<?php

namespace App\Form;

use App\Entity\CentreExamen;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CentreExamenType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
           /* ->add('ordre',IntegerType::class,
                array('required' => false,
                    'label' => 'Ordre : ',
                    'attr' => array('class'=>'form-control')))*/
            ->add('code_centre',TextType::class,
                array('required' => false,
                    'label' => 'Code : ',
                    'attr' => array('class'=>'form-control')))
            ->add('capacite',IntegerType::class,
                array('required' => false,
                    'label' => 'Capacité : ',
                    'attr' => array('class'=>'form-control')))
            ->add('libelle_centre',TextType::class,
                array('required' => false,
                    'label' => 'Libellé Centre : ',
                    'attr' => array('class'=>'form-control')))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CentreExamen::class,
        ]);
    }
}
