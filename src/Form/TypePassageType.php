<?php

namespace App\Form;

use App\Entity\TypePassage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TypePassageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libelle_passage_fr',TextType::class,
                array('required'=>false,
                    'label' => 'Libelle Type de passage en Français: ',
                    'attr' => array('class'=>'form-control')))
            ->add('libelle_passage_ar',
                TextType::class,
                array('required'=>false,
                    'label' => 'Libelle Type de passage en Arabe: ',
                    'attr' => array('class'=>'form-control')))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TypePassage::class,
        ]);
    }
}
