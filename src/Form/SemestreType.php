<?php

namespace App\Form;


use App\Entity\Semestre;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SemestreType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libelle_semestre_ar',TextType::class,
                array('label' => 'Libelle semestre en arabe: ',
                    'required'=>false,
                    'attr' => array('class'=>'form-control')))

            ->add('libelle_semestre_fr',TextType::class,
                array('label' => 'Libelle semestre en français: ',
                    'required'=>false,
                    'attr' => array('class'=>'form-control')))

            ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Semestre::class,
        ]);
    }
}
