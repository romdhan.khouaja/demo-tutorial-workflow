<?php

namespace App\Form;

use App\Entity\HeureMatiere;
use App\Entity\LangueMatiere;
use App\Entity\Matiere;
use App\Entity\Module;
use App\Entity\Niveau;
use App\Entity\Semestre;
use App\Entity\TypeMatiere;
use App\Entity\TypePassage;
use Doctrine\DBAL\Types\FloatType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

class MatiereType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $responsable = $options['responsable'];
        $builder
            ->add('code_matiere', NumberType::class, array(
                'required' => false,
                'label' => 'Code du matière : ',
                'attr' => array('autocomplete' => 'off','class' => 'form-control')))
            ->add('libelle_matiere_fr', TextType::class, array('required' => false,
                'label' => 'Libellé matière en Français : ',
                'attr' => array('autocomplete' => 'off','class' => 'form-control')))
            ->add('libelle_matiere_ar', TextType::class,
                array('required' => false,
                    'label' => 'Libellé matière en Arabe : ',
                    'attr' => array('autocomplete' => 'off','class' => 'form-control')))
            ->add('coefficient', NumberType::class, array(
                'required' => false,
                'label' => 'Coefficient : ',
                'attr' => array('autocomplete' => 'off', 'class' => 'form-control')))
            ->add('dure', NumberType::class, array(
                'required' => false,
                'label' => 'Duré : ',
                'attr' => array('autocomplete' => 'off','class' => 'form-control')))
            ->add('module', EntityType::class, [
                'class' => Module::class,
                'required' => false,
                'placeholder' => 'Choisissez un module',

            ])
            ->add('languematiere', EntityType::class, [
                'class' => LangueMatiere::class,
                'label' => 'Langue matiere : ',
                'required' => false,
                'placeholder' => 'Choisissez un langue',

            ])
            ->add('typematiere', EntityType::class, [
                'class' => TypeMatiere::class,
                'label' => 'Type matiere : ',
                'required' => false,
                'placeholder' => 'Choisissez un Type',

            ])
            ->add('typepassage', EntityType::class, [
                'class' => TypePassage::class,
                'label' => 'Type passage : ',
                'required' => false,
                'placeholder' => 'Choisissez un Passage',

            ])
            ->add('semestre', EntityType::class, [
                'class' => Semestre::class,
                'required' => false,
                'placeholder' => 'Choisissez une semestre',

            ])
            ->add('niveau', EntityType::class, [
                'class' => Niveau::class,
                'required' => false,
                'query_builder' => function (EntityRepository $er) use ($responsable) {
                    return $er->createQueryBuilder('n')
                        ->leftJoin('n.filiere', 'filiere')
                        ->addSelect('filiere')
                        ->andWhere('filiere.departement = :depart_id')
                        ->setParameter('depart_id', $responsable);

                },
                'placeholder' => 'Choisissez un Niveau',

            ]);
        if (!$responsable) {
            $builder
                ->add('niveau', EntityType::class, [
                    'class' => Niveau::class,
                    'required' => false,
                    'placeholder' => 'Choisissez un Niveau',
                ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Matiere::class,
            'responsable' => null
        ]);
    }
}
