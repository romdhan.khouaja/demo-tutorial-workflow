<?php

namespace App\Form;

use App\Entity\CentreExamen;
use App\Entity\Enseignant;
use App\Entity\GroupeExamen;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class GroupeExamenType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nbretudiantcentre', IntegerType::class, array(
                'required' => false,
                'label' => 'Nombre d\'etudiant : ',
                'attr' => array(
                    'class' => 'form-control')))
            ->add('oldNbElevSaisie', HiddenType::class, array(
                'required' => false,
                'attr' => array(
                    'class' => 'form-control',
                    'autocomplete'=>'off')))
            ->add('typeForm', HiddenType::class, array(
                'required' => false,
                'attr' => array(
                    'class' => 'form-control',
                    'autocomplete'=>'off')))
            ->add('nbElevRest', HiddenType::class, array(
                'required' => false,
                'attr' => array(
                    'class' => 'form-control',
                    'autocomplete'=>'off')))
            ->add('libellegroupe', TextType::class, array(
                'required' => false,
                'label' => 'Libelle Groupe : ',
                'attr' => array(
                    'class' => 'form-control',
                    'autocomplete'=>'off')))
            ->add('centreexamen', EntityType::class, [
                'class' => CentreExamen::class,
                'label' => 'Centre d\'examen : ',
                'choice_label' => 'libelle_groupeexamen',
                'placeholder' => 'Choisissez un centre d\'examen',

            ])
            ->add('enseignant',EntityType::class , [
                'class' => Enseignant::class,
                'required' => false,
                'query_builder' => function (EntityRepository $er)  {
                    return $er->createQueryBuilder('e')
                        ->andWhere('e.etat_surveille = :etat')
                        ->setParameter('etat', 1)
                        ->orderBy('e.nomprenomar', 'ASC');
                },
                'label' => 'enseignant(s) : ',
                'multiple' => true,
                'placeholder' => 'Choisissez enseignant-s',

            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => GroupeExamen::class,
        ]);
    }
}
