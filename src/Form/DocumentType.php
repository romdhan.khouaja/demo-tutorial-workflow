<?php

namespace App\Form;

use App\Entity\Document;
use App\Entity\Filiere;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Vich\UploaderBundle\Form\Type\VichFileType;

class DocumentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,array(
                'label' => 'Nom du document: ',
                'attr' => array('label' => 'Nom du document : ',
                    'required' => false,
                    'class' => 'form-control',
                    'autocomplete' => 'off')))

            ->add('description', TextareaType::class,array(
                'attr'=> array('label' => 'Description: ',
                    'required' => false,
                    'class' => 'form-control')))
            ->add('excelFile',VichFileType::class, [
                'required' => false,
                'label' => 'Fichier',
            ])

            ->add('users', EntityType::class,[
                'class'=>User::class,
                'choice_label'=>'username',
                'multiple'=> true,
                'required' => false,
                'label'=>'Choisissez utilisateur(s)'
            ])
            // ->add('Enregistrer', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Document::class,
        ]);
    }
}
