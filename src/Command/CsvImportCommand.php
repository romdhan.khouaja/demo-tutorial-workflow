<?php

namespace App\Command;

use League\Csv\Reader;
use App\Entity\Examen;  
use App\Entity\Niveau ;  
use App\Entity\Matiere ;  

use App\Entity\DocumentCsv;
use App\Entity\Session;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption; 
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface; 
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;  
use SensioLabs\AnsiConverter\Bridge\Twig\AnsiExtension; 
use SensioLabs\AnsiConverter\Theme\SolarizedTheme; 
use DateTimeInterface;
class CsvImportCommand extends Command  
{
    protected $count_error_duplicate = 0 ;
    protected $count_error_file_duplicate = 0 ;
    protected $count_success_file = 0 ;
    protected $count_success = 0 ;
    protected static $defaultName = 'cvs:import:to:datatabase';
    private $params;
    private $stateNiveau ;
    private $stateMatiere;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * CsvImportCommand constructor.
     *
     * @param EntityManagerInterface $em
     *
     * @throws \Symfony\Component\Console\Exception\LogicException
     */
    /**
     * CsvImportCommand constructor.
     *
     * @param EntityManagerInterface $em
     * @param ParameterBagInterface $params
     */
    public function __construct(EntityManagerInterface $em,ParameterBagInterface $params)
    { 
        $this->em = $em;
        $this->params = $params;
        parent::__construct();
        $this->count_error_duplicate = 0;
        $this->count_success = 0; 
        $this->count_error_file_duplicate = 0;
        $this->count_success_file = 0 ;
    }

    protected function configure()
    {
        $this
            ->setDescription('Creates a new import for csv file.')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addArgument('arg2', InputArgument::OPTIONAL, 'Argument id session')
            ->addOption('option1', null, InputOption::VALUE_OPTIONAL , 'Option description')
        ;
    }
    // fonction permettant de traiter le chemin dans windows rendre les / en \ 
    private function getCheminFichier(string $chaine){
        $app_path_csv_documents = $this->params->get('app.path.csv_documents');
        $kernel_project_dir = $this->params->get('kernel.project_dir');
        $resulat_chemin = $kernel_project_dir.'/public'.$app_path_csv_documents.'/'.$chaine ;
        // $dd = str_replace('\’', '\'', "String’s Title");
        $chemin = str_replace('/', '\\', $resulat_chemin);
        return $chemin;
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        // $io->title('Importation des données ...<br>');
        $arg1 = $input->getArgument('arg1');

        if ($arg1) {
            //  $io->note(sprintf('...Fichier nommé : %s <br>', $arg1));
            $chemin = self::getCheminFichier($arg1);
        }
        $arg2 = intval($input->getArgument('arg2'));
        if ($input->getOption('option1')) {
            // aucune option à placer  
        }
        $document = $this->em->getRepository(DocumentCsv::class)->findOneBy(['csvName' => $arg1]); 
        $session = $this->em->getRepository(Session::class)->findOneBy(['id' => $arg2]);
        if (null === $session ) {
            $io->error('Session inexistante');
        }
        if( null === $document || $document->getIsSent()===true){
            $io->error('Document a déja été envoyé ou est inexistant');
        }
        else
        { 
            // ############################################### 
            $reader = Reader::createFromPath($chemin,'r');  
            $reader->setDelimiter(';');
            $reader->setHeaderOffset(0);   

            $input_bom = $reader->getInputBOM(); 
            if ($input_bom === Reader::BOM_UTF16_LE || $input_bom === Reader::BOM_UTF16_BE) {
                $reader->addStreamFilter('convert.iconv.UTF-16/UTF-8');
            }
            // traitement filtrage tableau du fichier
            $tableau_unique = array();
            foreach ($reader as $row) {
                if(in_array($row, $tableau_unique)){ 
                    // echo "ROW WAS FOUND\n";
                    $this->count_error_file_duplicate++;
                }else{
                    $this->count_success_file++;
                    // echo "row was not found\n";
                    // affectation du tableau non dupliqué 
                    $tableau_unique[] = $row; 
                } 
            } 
            // $io->progressStart(iterator_count($reader));
            //  foreach ($tableau_unique as $tab) {
            // var_dump($row);
            // $io->note(sprintf(var_dump($row)));
            // schema de correspondance csv : id_niveau;code_matiere
            foreach($tableau_unique as $row)
            {
                // 	id 	nombreetudiant 	niveau_id 	matiere_id 	session_id /database
                // id_niveau;code_matiere;nombreetudiant /csv
                $stateMatiere = true;
                $stateNiveau = true;
                $code_niveau = $row["id_niveau" ];
                $niveau = $this->em->getRepository(Niveau::class)->findOneBy(['code_niveau' => $code_niveau]);
                if (!$niveau) 
                {
                    $io->error('Pas de niveau trouvé pour name '.$code_niveau);
                    $stateNiveau = false;
                    // throw new NotFoundHttpException('Pas de niveau trouvé pour name');
                    // throw $this->createNotFoundException( 'Pas de niveau trouvé pour name '.$code_niveau );
                }

                $code_matiere = $row['code_matiere'];
                $matiere = $this->em->getRepository(Matiere::class)->findOneBy(['code_matiere' => $code_matiere]);
                if (!$matiere) 
                {
                    $stateMatiere = false;
                    $io->error('Pas de matiere trouvée pour l\'id '.$code_matiere);
                    // throw new NotFoundHttpException('Pas de matiere trouvée pour l\'id '.$code_matiere);
                    // throw $this->createNotFoundException( 'Pas de matiere trouvée pour l\'id '.$code_matiere );
                }
                $nombreetudiant =$row['nombreetudiant'];
                $heureexamen=$row['heureexamen'];
                $heureexamenfin=$heureexamen + $matiere->getDure();
                $t_dateexamen=explode('/',$row['dateexamen']);
                $dateexamen=new \DateTime($t_dateexamen[2].'-'.$t_dateexamen[1].'-'.$t_dateexamen[0]);

                // faire une boucle pour trouver si l'examen existe pour eviter les duplications
                $examenResult = null;

                if ($stateMatiere && $stateNiveau && $session){
                    $examenResult = $this->em->getRepository(Examen::class)
                    ->findOneBy([
                        'session' => $session,
                        'matiere' => $matiere,
                        'niveau' => $niveau,
                    ]);
                }
                if($examenResult !== null){
                    $this->count_error_duplicate++;
                }
                if ($examenResult === null && $stateMatiere && $stateNiveau && $session) 
                {
                    $examen = new Examen();
                    $examen->setMatiere($matiere);
                    $examen->setNiveau($niveau); 
                    $examen->setSession($session);
                    $examen->setNombreetudiant($nombreetudiant);
                    $examen->setHeureexamen($heureexamen);
                    $examen->setHeureexamenfin($heureexamenfin);
                    $examen->setDateexamen($dateexamen);
                    //$examen->setNombrenonaffect(0);
                    $this->em->persist($examen);
                    $this->em->flush();
                    $this->count_success++;
                }
                // $io->progressAdvance();
            }
            //}
                
            // end of foreach
            // enregistrement total dans la db hors de la boucle  

            // test var_dump($document);
            // desactivation du document apres son importation 
            if($document)
            {
                $document->setSession($session);
                $document->setIsSent(true);
                $this->em->persist($document);
                $this->em->flush();
            }

            if($this->count_success_file > 0){
                $io->success('Nombre de lignes accepté depuis le fichier = '.$this->count_success_file);
            }
            if($this->count_error_file_duplicate>0){
                $io->warning('Nombre de lignes dupliquées depuis le fichier = '.$this->count_error_file_duplicate);
            }

            if($this->count_success > 0){
                $io->success('Succes ! nombre de ligne acceptées pour l\'enregistrement = '.$this->count_success);
            }
            if($this->count_error_duplicate>0){
                $io->warning('Erreur ! nombre duplication dans la base de donnée = '.$this->count_error_duplicate);
            }
            
            // $io->progressFinish();
            
        }
    }
        
}
