<?php

namespace App\Controller;

use App\Entity\Departement;
use App\Form\DepartementType;
use App\Repository\DepartementRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * @Route("/departement")
 * @Security("is_granted('ROLE_ADMIN')")
 */
class DepartementController extends AbstractController
{
    protected $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @Route("/", name="departement_index", methods={"GET","POST"})
     */
    public function index(DepartementRepository $departementRepository, Request $request, PaginatorInterface $paginator): Response
    {
        $departement = new Departement();
        $form = $this->createForm(DepartementType::class, $departement);
        $form->handleRequest($request);

        $alldepartements = $departementRepository->findAll();
// Paginate the results of the query
        $departements = $paginator->paginate(
        // Doctrine Query, not results
            $alldepartements,
            // Define the page parameter
            $request->query->getInt('page', 1),
            // Items per page
            8
        );

        return $this->render('departement/index.html.twig', [
            'departement' => $departement,
            'departements' => $departements,
            'form' => $form->createView(),

        ]);

    }

    /**
     * @Route("/new-ajax", name="departement_new", methods={"GET","POST"})
     */
    public function newAjax(Request $request): Response
    {
        $departement = new Departement();
        $form = $this->createForm(DepartementType::class, $departement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($departement);
            $entityManager->flush();
            $this->addFlash("info", "Departement Ajouter Avec Succés");
            return new Response('ok');
        }

        return $this->render('departement/_form.html.twig', [
            'departement' => $departement,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="departement_show", methods={"GET"})
     */
    public function show(Departement $departement): Response
    {
        return $this->render('departement/show.html.twig', [
            'departement' => $departement,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="departement_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Departement $departement): Response
    {
        $form = $this->createForm(DepartementType::class, $departement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("info", "Département Modifier Avec Succés");
            return new Response('ok');

        }

        return $this->render('departement/_form_edit.html.twig', [
            'departement' => $departement,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="departement_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Departement $departement): Response
    {
        if ($this->isCsrfTokenValid('delete' . $departement->getId(), $request->request->get('_token'))) {
            if ($departement->getFiliere()->count() > 0) {
                $this->session->getFlashBag()->add('danger', 'Cette département ne peut pas étre supprimer');
            } else {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($departement);
                $entityManager->flush();
                $this->addFlash("info", "Département supprimé Avec Succés");
            }
        }
        return $this->redirectToRoute('departement_index');
    }
}
