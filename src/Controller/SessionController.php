<?php

namespace App\Controller;

use App\Entity\Session;
use App\Form\SessionType;
use App\Repository\DepartementRepository;
use App\Repository\SessionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Session\SessionInterface;


/**
 * @Route("/session")
 */
class SessionController extends AbstractController
{
    protected $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @Route("/", name="session_index", methods={"GET","POST"})
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_SCOLARITE')")
     */
    public function index(SessionRepository $sessionRepository, Request $request): Response
    {
        $session = new Session();
        $form = $this->createForm(SessionType::class, $session);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($session);
            $entityManager->flush();

            return $this->redirectToRoute('session_index');
        }
        return $this->render('session/index.html.twig', [
            'sessions' => $sessionRepository->findAll(),
            'session' => $session,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/new", name="session_new", methods={"GET","POST"})
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_SCOLARITE')")
     */
    public function new(Request $request): Response
    {
        $session = new Session();
        $form = $this->createForm(SessionType::class, $session);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($session);
            $entityManager->flush();
            $this->addFlash("info", "Session ajouter Avec Succés");
            return new Response('ok');
        }

        return $this->render('session/_form.html.twig', [
            'session' => $session,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="session_show", methods={"GET"})
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_SCOLARITE')")
     */
    public function show(Session $session): Response
    {
        return $this->render('session/show.html.twig', [
            'session' => $session,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="session_edit", methods={"GET","POST"})
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_SCOLARITE')")
     */
    public function edit(Request $request, Session $session): Response
    {
        $form = $this->createForm(SessionType::class, $session);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash("info", "Session modifier Avec Succés");
            return new Response('ok');
        }

        return $this->render('session/_form_edit.html.twig', [
            'session' => $session,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/session_valider_etp1", name="session_valider_etp1", methods={"GET","POST"})
     * @Security("is_granted('ROLE_DOYEN') or is_granted('ROLE_SECRETAIRE_GENERAL')")
     */
    public function sessionValiderEtp1(Request $request, Session $session): Response
    {
        $session->setValideEtp1(true);
        $this->getDoctrine()->getManager()->flush();

        $this->addFlash("info", "la session a été valider Avec Succés");
        return $this->redirectToRoute('examen_index', ['session' => $session->getId()]);
    }
    /**
     * @Route("/{id}/session_valider_etp2", name="session_valider_etp2", methods={"GET","POST"})
     * @Security("is_granted('ROLE_CHEF_DEPARTEMENT')")
     */
    public function sessionValiderEtp2(Request $request,DepartementRepository $departementRepository, Session $session): Response
    {
        $departements=$departementRepository->findAll();
        $nb_departement=count($departements)."--";

        $depart_valid=$session->getDepartValid().$this->getUser()->getDepartement()->getId().";";
        $t_depart=explode(';',$depart_valid);
        $nb_depart_valid= count($t_depart)-1;

        if($nb_departement==$nb_depart_valid){
            $session->setValideEtp2(true);
        }
        $session->setDepartValid($depart_valid);

        $this->getDoctrine()->getManager()->flush();
        $this->addFlash("info", "la session a été valider Avec Succés");
        return $this->redirectToRoute('examen_index', ['session' => $session->getId()]);
    }

    /**
     * @Route("/{id}", name="session_delete", methods={"DELETE"})
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_SCOLARITE')")
     */
    public function delete(Request $request, Session $session): Response
    {
        if ($this->isCsrfTokenValid('delete' . $session->getId(), $request->request->get('_token'))) {
            if ($session->getExamens()->count() > 0) {
                $this->session->getFlashBag()->add('danger', 'Cette session ne peut pas etre supprimer');

            } else {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($session);
                $entityManager->flush();
                $this->addFlash("info", "Session supprimer Avec Succés");
            }
        }
        return $this->redirectToRoute('session_index');
    }
}
