<?php

namespace App\Controller;

use App\Repository\CentreExamenRepository;
use App\Repository\DepartementRepository;
use App\Repository\DocumentRepository;
use App\Repository\EnseignantRepository;
use App\Repository\FiliereRepository;
use App\Repository\MatiereRepository;
use App\Repository\NiveauRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DoyenController extends AbstractController
{
    /**
     * @Route("/doyen", name="doyen")
     */
    public function index(DepartementRepository $departementRepository,FiliereRepository $filiereRepository,NiveauRepository $niveauRepository,EnseignantRepository $enseignantRepository,DocumentRepository $documentRepository,MatiereRepository $matiereRepository,CentreExamenRepository $centreExamenRepository)
    {
        $dep=$departementRepository->findAll();
        $mat=$matiereRepository->findAll();
        $ens=$enseignantRepository->findBy(['avec_contrat' => 1]);
       $fil=$filiereRepository->findAll();
        $niv=$niveauRepository->findAll();
        $cen=$centreExamenRepository->findAll();
        $user=$this->getUser();

        $documents=$user->getDocuments();
        return $this->render('doyen/index.html.twig', [
            'controller_name' => 'DoyenController',
            'dep' => $dep,
            'mat' => $mat,
            'ens' => $ens,
            'doc'=>$documents,
            'cen'=>$cen,
            'fil'=>$fil,
            'niv'=>$niv,
        ]);
    }
}
