<?php

namespace App\Controller;

use App\Entity\TypePassage;
use App\Form\TypePassageType;
use App\Repository\TypePassageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


/**
 * @Route("/typepassage")
 * @Security("is_granted('ROLE_ADMIN')")
 */
class TypePassageController extends AbstractController
{

    /**
     * @Route("/", name="type_passage_index", methods={"GET","POST"})
     */
    public function index(TypePassageRepository $typePassageRepository, Request $request): Response
    {
        $typePassage = new TypePassage();
        $form = $this->createForm(TypePassageType::class, $typePassage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($typePassage);
            $entityManager->flush();
            $this->addFlash("info", "Type de passage Ajouter Avec Succés");
            return $this->redirectToRoute('type_passage_index');
        }
        return $this->render('type_passage/index.html.twig', [
            'type_passages' => $typePassageRepository->findAll(), 'type_passage' => $typePassage,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/new", name="type_passage_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $typePassage = new TypePassage();
        $form = $this->createForm(TypePassageType::class, $typePassage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($typePassage);
            $entityManager->flush();
            $this->addFlash("info", "Type de passage Ajouter Avec Succés");
            return new Response('ok');
        }

        return $this->render('type_passage/_form.html.twig', [
            'type_passage' => $typePassage,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="type_passage_show", methods={"GET"})
     */
    public function show(TypePassage $typePassage): Response
    {
        return $this->render('type_passage/show.html.twig', [
            'type_passage' => $typePassage,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="type_passage_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, TypePassage $typePassage): Response
    {
        $form = $this->createForm(TypePassageType::class, $typePassage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("info", "Type de passage Modifier Avec Succés");
            return new Response('ok');
        }

        return $this->render('type_passage/_form_edit.html.twig', [
            'type_passage' => $typePassage,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="type_passage_delete", methods={"DELETE"})
     */
    public function delete(Request $request, TypePassage $typePassage): Response
    {
        if ($this->isCsrfTokenValid('delete' . $typePassage->getId(), $request->request->get('_token'))) {
            if($typePassage->getMatieres()->count()>0){
                $this->addFlash("danger", " Ce type de passage ne peut pas être supprimer ");
            }
            else{
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($typePassage);
            $entityManager->flush();
            $this->addFlash("info", "Type de passage supprimer Avec Succés");
        }
        }
        return $this->redirectToRoute('type_passage_index');
    }
}
