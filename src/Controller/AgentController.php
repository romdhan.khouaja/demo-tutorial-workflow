<?php

namespace App\Controller;

use App\Repository\DocumentRepository;
use App\Repository\EnseignantRepository;
use App\Repository\ExamenRepository;
use App\Repository\FiliereRepository;
use App\Repository\MatiereRepository;
use App\Repository\NiveauRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AgentController extends AbstractController
{
    /**
     * @Route("/agent", name="agent")
     */
    public function index(EnseignantRepository $enseignantRepository,MatiereRepository $matiereRepository,DocumentRepository $documentRepository,ExamenRepository $examenRepository,NiveauRepository $niveauRepository,FiliereRepository $filiereRepository)
    {
        $ens=$enseignantRepository->findBy(['avec_contrat' => 1]);
        $fil=$filiereRepository->findAll();
        $niv=$niveauRepository->findAll();
        $user=$this->getUser();

        $documents=$user->getDocuments();
        return $this->render('agent/index.html.twig', [
            'controller_name' => 'AgentController',
            'ens' => $ens,
            'fil'=>$fil,
            'niv'=>$niv,
            'doc'=>$documents,
        ]);
    }
}
