<?php

namespace App\Controller;

use App\Entity\DocumentCsv;
use App\Form\DocumentCsvType;
use App\Repository\ExamenRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\DocumentCsvRepository;
use App\Repository\SessionRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Routing\Annotation\Route;
use SensioLabs\AnsiConverter\AnsiToHtmlConverter;
use Symfony\Component\HttpKernel\KernelInterface;
use SensioLabs\AnsiConverter\Theme\SolarizedTheme;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Knp\Component\Pager\PaginatorInterface;


/**
 * @Route("/documentcsv")
 * @Security("is_granted('ROLE_SCOLARITE')")
 */
class DocumentCsvController extends AbstractController
{
    private $session;
    private $nav;

    public function __construct($nav = 'document', SessionInterface $session)
    {
        $this->session = $session;
        $this->nav = $nav;
    }

    /**
     * @Route("/send/{file_name}", name="send_document_csv", methods={"GET","POST"})
     */
    public function sendSpool(EntityManagerInterface $em, KernelInterface $kernel, string $file_name = "", string $idSession = "")
    {
        $idSession = $this->session->get('sessionId');
        $application = new Application($kernel);
        $application->setAutoExit(false);
        // dd($file_name);
        $input = new ArrayInput([
            'command' => 'cvs:import:to:datatabase',

            'arg1' => $file_name,
            'arg2' => $idSession,

        ]);

        $output = new BufferedOutput(
            OutputInterface::VERBOSITY_NORMAL,
            true // true for decorated
        );
        $application->run($input, $output);

        $theme = new SolarizedTheme();
        $styles = $theme->asCss();
        $converter = new AnsiToHtmlConverter($theme, false);
        $content = $output->fetch();
        $this->session->set('message', $converter->convert($content));
        $this->session->set('mystyle', $styles);
        return $this->redirectToRoute('document_csv_index', [
            'nav' => $this->nav,
            'mystyle' => $styles,
        ]);
    }


    /**
     * @Route("/", name="document_csv_index", methods={"GET","POST"})
     */
    public function index(Request $request,SessionRepository $sessionRepository, ExamenRepository $examenRepository,DocumentCsvRepository $documentCsvRepository, PaginatorInterface $paginator): Response
    {
        $form = $this->createForm(DocumentCsvType::class);
        $form->handleRequest($request);
        $idSession = intval($request->request->get('id'));
        if ($idSession === null) {
            $idSession = 0;
        }
        $message = $this->session->get('message');
        $mystyle = $this->session->get('mystyle');
        $this->session->remove('message');
        $this->session->remove('mystyle');
        if ($request->getMethod() === "POST" && $request->request->get('id')) {
            $idSession = intval($request->request->get('id'));
            $this->session->set('sessionId', $idSession);
            $session = $sessionRepository->findOneBy(['id' => $idSession]);
            $documents = $documentCsvRepository->findBy(['session' => $session]);
        } else {
            $documents = null;
        }
/*// Paginate the results of the query
        $doc = $paginator->paginate(
        // Doctrine Query, not results
            $documents,
            // Define the page parameter
            $request->query->getInt('page', 1),
            // Items per page
            5
        );*/
        $sessions = $sessionRepository->findAll();
        $examens = $examenRepository->findAll();
        return $this->render('document_csv/index.html.twig', [
            'document_csvs' => $documents,
//            'doc' => $doc,
            'sessions' => $sessions,
            'examens'=>$examens,
            'sessionId' => $idSession,
            'nav' => $this->nav,
            'message' => $message,
            'form' => $form->createView(),
            'mystyle' => $mystyle,
        ]);
    }

    /**
     * @Route("/new", name="document_csv_new", methods={"GET","POST"})
     */
    public function new(Request $request, SessionRepository $sessionRepository): Response
    {

        $idSession = intval($this->session->get('sessionId'));
        $session = $sessionRepository->findOneBy(['id' => $idSession]);
        $documentCsv = new DocumentCsv();
        $form = $this->createForm(DocumentCsvType::class, $documentCsv);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $documentCsv->setSession($session);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($documentCsv);
            $entityManager->flush();
            if ($this->session->get('sessionId') === null) {
                $this->session->getFlashBag()->add('danger', "Vous n'avez pas selectionnez de session");
                return new Response('ok');
            }
            $this->addFlash("info", "fichier ajouter Avec Succés");
            return new Response('ok');
        }

        return $this->render('document_csv/_form.html.twig', [
            'document_csv' => $documentCsv,
            'form' => $form->createView(),
            'nav' => $this->nav,
        ]);
    }

    /**
     * @Route("/{id}", name="document_csv_show", methods={"GET"})
     */
    public function show(DocumentCsv $documentCsv): Response
    {
        return $this->render('document_csv/show.html.twig', [
            'document_csv' => $documentCsv,
            'nav' => $this->nav,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="document_csv_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, DocumentCsv $documentCsv): Response
    {
        $form = $this->createForm(DocumentCsvType::class, $documentCsv);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('document_csv_index');
        }

        return $this->render('document_csv/edit.html.twig', [
            'document_csv' => $documentCsv,
            'form' => $form->createView(),
            'nav' => $this->nav,
        ]);
    }

    /**
     * @Route("/{id}", name="document_csv_delete", methods={"DELETE"})
     */
    public function delete(Request $request, DocumentCsv $documentCsv): Response
    {
        if ($this->isCsrfTokenValid('delete' . $documentCsv->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($documentCsv);
            $entityManager->flush();
        }

        return $this->redirectToRoute('document_csv_index');
    }
}
