<?php

namespace App\Controller;

use App\Entity\Enseignant;
use App\Form\EnseignantType;
use App\Form\EnseignantAffecterType;
use App\Form\EnseignantSearchType;
use App\Repository\EnseignantRepository;
use App\Repository\MatiereRepository;
use App\Repository\NiveauRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Dompdf\Dompdf;
use Dompdf\Options;


/**
 * @Route("/enseignant")
 */
class EnseignantController extends AbstractController
{
    protected $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @Route("/", name="enseignant_index", methods={"GET","POST"})
     * @Security("is_granted('ROLE_ADMIN')or is_granted('ROLE_CHEF_DEPARTEMENT')")
     */
    public function index(EnseignantRepository $enseignantRepository, Request $request, PaginatorInterface $paginator): Response
    {
        $enseignant = new Enseignant();
        $form = $this->createForm(EnseignantType::class, $enseignant);
        $form->handleRequest($request);
        $form_search = $this->createForm(EnseignantSearchType::class);
        $form_search->handleRequest($request);
        if ($form_search->isSubmitted()) {
            $url = $this->getUrlSearch($request->request->get('enseignant_search'));

            return $this->redirectToRoute('enseignant_index', $url);
            //exit();
        }

        $allenseignants = $enseignantRepository->searchEnseignant($request->query->all());
// Paginate the results of the query
        $enseignants = $paginator->paginate(
        // Doctrine Query, not results
            $allenseignants,
            // Define the page parameter
            $request->query->getInt('page', 1),
            // Items per page
            6
        );

        return $this->render('enseignant/index.html.twig', [
            'enseignants' => $enseignants,
            'enseignant' => $enseignant,
            'form' => $form->createView(),
            'form_search' => $form_search->createView(),
        ]);
    }

    public function getUrlSearch($request)
    {
        $url = [];
        if (isset($request['code_enseignant']) && !empty($request['code_enseignant'])) {
            $url['code_enseignant'] = $request['code_enseignant'];
        }
        if (isset($request['nomprenomfr']) && !empty($request['nomprenomfr'])) {
            $url['nomprenomfr'] = $request['nomprenomfr'];
        }
        if (isset($request['nomprenomar']) && !empty($request['nomprenomar'])) {
            $url['nomprenomar'] = $request['nomprenomar'];
        }
        if (isset($request['grade']) && !empty($request['grade'])) {
            $url['grade'] = $request['grade'];
        }
        return $url;
    }

    public function getUrlAffect($request)
    {
        $url = [];
        if (isset($request['filiere']) && !empty($request['filiere'])) {
            $url['filiere'] = $request['filiere'];
        }
        if (isset($request['semestre']) && !empty($request['semestre'])) {
            $url['semestre'] = $request['semestre'];
        }
        if (isset($request['niveau']) && !empty($request['niveau'])) {
            $url['niveau'] = $request['niveau'];
        }
        return $url;
    }

    /**
     * @Route("/listenseignant", name="enseignant_list", methods={"GET"})
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_CHEF_DEPARTEMENT') or is_granted('ROLE_SCOLARITE') or is_granted('ROLE_AGENT') or is_granted('ROLE_DOYEN') or is_granted('ROLE_SECRETAIRE_GENERAL')")
     */
    public function listd(EnseignantRepository $enseignantRepository): Response
    {
        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'courier');
        //$pdfOptions->set('isHtml5ParserEnabled', true);

        $pdfOptions->set('DOMPDF_ENABLE_FONTSUBSETTING', true);
        $pdfOptions->set('DOMPDF_UNICODE_ENABLED', true);
        $pdfOptions->setIsRemoteEnabled(true);
        $dompdf = new Dompdf($pdfOptions);
        $enseignants = $enseignantRepository->findBy(['avec_contrat' => 1]);

        $comp = 0;
        $page=1;
        foreach ($enseignants as $enseignant) {
            $tab_enseignant[$page][]=$enseignant;
            $comp++;
            if ($comp == 20) {
                $comp = 0;
                $page++;
            }
        }
        $dompdf->output();


        $d = date('Y-m-d H:i:s');
        // Retrieve the HTML generated in our twig file
        $html = $this->renderView('enseignant/listeenseignant.html.twig', [
            'enseignants' => $enseignants, 'tab_enseignant' =>  $tab_enseignant,'dt' => $d
        ]);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html, 'UTF-8');

        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');
        // $dompdf->set_option('isRemoteEnabled', true);

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser (force download)
        $dompdf->stream("mypdf.pdf", [
            "Attachment" => false
        ]);

    }

    /**
     * @Route("/listenseignantresponsable", name="enseignant_responsable_list", methods={"GET"})
     * @Security(" is_granted('ROLE_SCOLARITE') ")
     */
    public function listresponsable(MatiereRepository $matiereRepository, EnseignantRepository $enseignantRepository): Response
    {
        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'courier');
        //$pdfOptions->set('isHtml5ParserEnabled', true);

        $pdfOptions->set('DOMPDF_ENABLE_FONTSUBSETTING', true);
        $pdfOptions->set('DOMPDF_UNICODE_ENABLED', true);
        $pdfOptions->setIsRemoteEnabled(true);
        $dompdf = new Dompdf($pdfOptions);
        $matieres = $matiereRepository->findAll();
        $comp = 0;
        $page=1;
        foreach ($matieres as $matiere) {
            $tab_matiere[$page][]=$matiere;
            $comp++;
            if ($comp == 20) {
                $comp = 0;
                $page++;
            }
        }
        $dompdf->output();

        $d = date('Y-m-d H:i:s');
        // Retrieve the HTML generated in our twig file
        $html = $this->renderView('enseignant/listeenseignantresponsable.html.twig', [
            'matieres' => $matieres,'tab_matiere' => $tab_matiere,
            'dt' => $d
        ]);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html, 'UTF-8');

        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');
        // $dompdf->set_option('isRemoteEnabled', true);

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser (force download)
        $dompdf->stream("mypdf.pdf", [
            "Attachment" => false
        ]);

    }

    /**
     * @Route("/listenseignantsurveillant", name="enseignant_list_surveillant", methods={"GET"})
     * @Security(" is_granted('ROLE_SCOLARITE') ")
     */
    public function listsurveillant(MatiereRepository $matiereRepository, EnseignantRepository $enseignantRepository): Response
    {
        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'courier');
        //$pdfOptions->set('isHtml5ParserEnabled', true);

        $pdfOptions->set('DOMPDF_ENABLE_FONTSUBSETTING', true);
        $pdfOptions->set('DOMPDF_UNICODE_ENABLED', true);
        $pdfOptions->setIsRemoteEnabled(true);
        $dompdf = new Dompdf($pdfOptions);
        $enseignants = $enseignantRepository->findBy(['etat_surveille' => 1]);
        $comp = 0;
        $page=1;
        foreach ($enseignants as $enseignant) {
            $tab_enseignant[$page][]=$enseignant;
            $comp++;
            if ($comp == 18) {
                $comp = 0;
                $page++;
            }
        }
        $dompdf->output();


        $matieres = $matiereRepository->findAll();
        $d = date('Y-m-d H:i:s');
        // Retrieve the HTML generated in our twig file
        $html = $this->renderView('enseignant/listeenseignantsurveillant.html.twig', [
            'enseignants' => $enseignants,
            'matieres' => $matieres,
            'tab_enseignant' => $tab_enseignant,
            'dt' => $d
        ]);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html, 'UTF-8');

        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');
        // $dompdf->set_option('isRemoteEnabled', true);

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser (force download)
        $dompdf->stream("mypdf.pdf", [
            "Attachment" => false
        ]);

    }

    /**
     * @Route("/edit_etat_enseignant", name="edit_etat_enseignant", methods={"GET","POST"})
     */
    public function editEtatEnseignant(MatiereRepository $matiereRepository, EnseignantRepository $enseignantRepository, Request $request): Response
    {
        $id_enseig = $request->request->get('id_enseig');
        $etat = $request->request->get('etat');
        $enseignant = $enseignantRepository->find($id_enseig);
        if ($etat == 1) {
            $new_etat = 0;
            $enseignant->setEtatSurveille(0);
            $matieres = $matiereRepository->findBy(['enseignant' => $enseignant->getId()]);
            foreach ($matieres as $matiere_enseig) {
                $matiere_enseig->setEnseignant(null);
                $this->getDoctrine()->getManager()->flush();
            }
        } else {
            $new_etat = 1;
        }
        $enseignant->setAvecContrat($new_etat);
        $this->getDoctrine()->getManager()->flush();
        return new Response($new_etat);

    }

    /**
     * @Route("/edit_surveille", name="edit_surveille", methods={"GET","POST"})
     */
    public function editSurveille(EnseignantRepository $enseignantRepository, Request $request): Response
    {
        $id_enseig = $request->request->get('id_enseig');
        $etat = $request->request->get('etat');
        $enseignant = $enseignantRepository->find($id_enseig);
        if ($etat == 1) {
            $new_etat = 0;
        } else {
            $new_etat = 1;
        }
        $enseignant->setEtatSurveille($new_etat);
        $this->getDoctrine()->getManager()->flush();
        return new Response($new_etat);

    }

    /**
     * @Route("/affecterenseignant", name="affecter_enseignant", methods={"GET","POST"})
     * @Security("is_granted('ROLE_CHEF_DEPARTEMENT')")
     */
    public function affectation(EnseignantRepository $enseignantRepository, MatiereRepository $matiereRepository, PaginatorInterface $paginator, NiveauRepository $niveauRepository, Request $request): Response
    {
        $form = $this->createForm(EnseignantAffecterType::class, null, ['responsable' => $this->getUser()->getDepartement()->getId()]);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $url = $this->getUrlAffect($request->request->get('enseignant_affecter'));

            return $this->redirectToRoute('affecter_enseignant', $url);
        }
        $allmatieres = $matiereRepository->searchMatiere($request->query->all());
// Paginate the results of the query
        $matieres = $paginator->paginate(
        // Doctrine Query, not results
            $allmatieres,
            // Define the page parameter
            $request->query->getInt('page', 1),
            // Items per page
            6
        );
        return $this->render('enseignant/affecterenseignant.html.twig', [
            'form' => $form->createView(),
            'matieres' => $matieres,
            'niveaux' => $niveauRepository->findAllNiveau($this->getUser()->getDepartement()),
            'enseignants' => $enseignantRepository->findBy(['avec_contrat' => 1]),
        ]);

    }


    /**
     * @Route("/save_affect_ensaignant", name="save_affect_ensaignant", methods={"GET","POST"})
     */
    public function saveAffectEnsaignant(EnseignantRepository $enseignantRepository, MatiereRepository $matiereRepository, Request $request): Response
    {
        $matiere = $matiereRepository->find($request->request->get('id_matiere'));
        $enseignant = $enseignantRepository->find($request->request->get('id_enseignant'));
        $matiere->setEnseignant($enseignant);
        $this->getDoctrine()->getManager()->flush();
        exit();
    }

    /**
     * @Route("/new-ajax", name="enseignant_new_ajax", methods={"GET","POST"})
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_CHEF_DEPARTEMENT')")
     */
    public function newAjax(Request $request): Response
    {
        $enseignant = new Enseignant();
        $form = $this->createForm(EnseignantType::class, $enseignant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($enseignant);
            $entityManager->flush();
            $this->addFlash("info", "Enseignant Ajouter Avec Succés");
            return new Response('ok');
        }

        return $this->render('enseignant/_form.html.twig', [
            'enseignant' => $enseignant,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="enseignant_show", methods={"GET"})
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_CHEF_DEPARTEMENT')")
     */
    public function show(Enseignant $enseignant): Response
    {
        return $this->render('enseignant/show.html.twig', [
            'enseignant' => $enseignant,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="enseignant_edit", methods={"GET","POST"})
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_CHEF_DEPARTEMENT')")
     */
    public function edit(Request $request, Enseignant $enseignant): Response
    {
        $form = $this->createForm(EnseignantType::class, $enseignant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("info", "Enseignant Modifier Avec Succés");
            return new Response('ok');
        }

        return $this->render('enseignant/_form_edit.html.twig', [
            'enseignant' => $enseignant,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="enseignant_delete", methods={"DELETE"})
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_CHEF_DEPARTEMENT')")
     */
    public function delete(Request $request, Enseignant $enseignant): Response
    {
        if ($this->isCsrfTokenValid('delete' . $enseignant->getId(), $request->request->get('_token'))) {
            if ($enseignant->getMatieres()->count() > 0) {
                $this->addFlash('danger', 'Cet enseignant ne peut pas étre supprimer');
            } else {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($enseignant);
                $entityManager->flush();
                $this->addFlash("info", "Enseignant supprimé Avec Succés");
            }
        }

        return $this->redirectToRoute('enseignant_index');
    }
}
