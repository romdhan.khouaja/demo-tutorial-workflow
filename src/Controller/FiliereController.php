<?php

namespace App\Controller;


use App\Entity\Filiere;
use App\Form\FiliereType;
use App\Form\FiliereSearchType;
use App\Repository\FiliereRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Session\SessionInterface; 
use Dompdf\Dompdf;
use Dompdf\Options;

/**
 * @Route("/filiere")
 * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_CHEF_DEPARTEMENT')")
 */
class FiliereController extends AbstractController
{
    protected $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    } 

    /**
     * @Route("/", name="filiere_index", methods={"GET","POST"})
     */
    public function index(FiliereRepository $filiereRepository, Request $request, PaginatorInterface $paginator): Response
    {
        $filiere = new Filiere();
        $form = $this->createForm(FiliereType::class, $filiere);
        $form->handleRequest($request);

        $form_search = $this->createForm(FiliereSearchType::class, null, ['responsable' => $this->getUser()->getDepartement()]);
        $form_search->handleRequest($request);
        if ($form_search->isSubmitted()) {
            $url = $this->getUrlSearch($request->request->get('filiere_search'));

            return $this->redirectToRoute('filiere_index', $url);
        }

        $allfilieres = $filiereRepository->searchFiliere($request->query->all());


        if ($this->getUser()->isChefDepartement()) {
            $allfilieres = $filiereRepository->searchFiliere($request->query->all(), $this->getUser()->getDepartement());

            //$all=$filiereRepository->findBy(['departement'=>$this->getUser()->getDepartement()]);
        }

        // Paginate the results of the query
        $filieres = $paginator->paginate(
        // Doctrine Query, not results
            $allfilieres,
            // Define the page parameter
            $request->query->getInt('page', 1),
            // Items per page
            10
        );
        return $this->render('filiere/index.html.twig', [
            'filieres' => $filieres,
            'filiere' => $filiere,
            'form' => $form->createView(),
            'form_search' => $form_search->createView(),
        ]);
    }

    public function getUrlSearch($request)
    {
        $url = [];
        if (isset($request['libelle_filiere_fr']) && !empty($request['libelle_filiere_fr'])) {
            $url['libelle_filiere_fr'] = $request['libelle_filiere_fr'];
        }
        if (isset($request['libelle_filiere_ar']) && !empty($request['libelle_filiere_ar'])) {
            $url['libelle_filiere_ar'] = $request['libelle_filiere_ar'];
        }
        if (isset($request['departement']) && !empty($request['departement'])) {
            $url['departement'] = $request['departement'];
        }
        return $url;
    }

    /**
     * @Route("/listfiliere", name="filiere_list", methods={"GET"})
     */
    public function listd(FiliereRepository $filiereRepository) 
    {
        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'courier');
        //$pdfOptions->set('isHtml5ParserEnabled', true);

        $pdfOptions->set('DOMPDF_ENABLE_FONTSUBSETTING', true);
        $pdfOptions->set('DOMPDF_UNICODE_ENABLED', true);
        $pdfOptions->setIsRemoteEnabled(true);
        $dompdf = new Dompdf($pdfOptions);

        $filieres = $filiereRepository->findAll();
        if ($this->getUser()->isChefDepartement()) {
            $filieres = $filiereRepository->findBy(['departement' => $this->getUser()->getDepartement()]);
        }
        $comp = 0;
        $page=1;
        foreach ($filieres as $filiere) {
            $tab_filiere[$page][]=$filiere;
            $comp++;
            if ($comp == 20) {
                $comp = 0;
                $page++;
            }
        }
        $dompdf->output();
        $d = date('Y-m-d H:i:s');
        // Retrieve the HTML generated in our twig file
        $html = $this->renderView('filiere/listefiliere.html.twig', [
            'filieres' => $filieres, 'tab_filiere' => $tab_filiere,'dt' => $d
        ]);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html, 'UTF-8');

        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser (force download)
        $dompdf->stream("mypdf.pdf", [
            "Attachment" => false
        ]);


    }

    /**
     * @Route("/new-ajax", name="filiere_new_ajax", methods={"GET","POST"})
     */
    public function newAjax(Request $request): Response
    {
        $filiere = new Filiere();
        $form = $this->createForm(FiliereType::class, $filiere, ['responsable' => $this->getUser()->getDepartement()]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if ($this->getUser()->isChefDepartement()) {
                $filiere->setDepartement($this->getUser()->getDepartement());
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($filiere);
            $entityManager->flush();
            $this->addFlash("info", "Filière Ajouter Avec Succés");
            return new Response('ok');
        }

        return $this->render('filiere/_form.html.twig', [
            'filiere' => $filiere,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit-ajax", name="filiere_edit_ajax", methods={"GET","POST"})
     */
    public function editAjax(Filiere $filiere, Request $request): Response
    {

        $form = $this->createForm(FiliereType::class, $filiere, ['responsable' => $this->getUser()->getDepartement()]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if ($this->getUser()->isChefDepartement()) {
                $filiere->setDepartement($this->getUser()->getDepartement());
            }
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("info", "Filière modifier Avec Succés");
            return new Response('ok');
        }

        return $this->render('filiere/_form_edit.html.twig', [
            'filiere' => $filiere,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="filiere_show", methods={"GET"})
     */
    public function show(Filiere $filiere): Response
    {
        return $this->render('filiere/show.html.twig', [
            'filiere' => $filiere,
        ]);
    }

    /**
     * @Route("/delete/{id}", name="filiere_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Filiere $filiere): Response
    {
        if ($this->isCsrfTokenValid('delete' . $filiere->getId(), $request->request->get('_token'))) {
            if ($filiere->getNiveaux()->count() > 0) {
                // $session->getFlashBag()->add('notice', 'Profile updated');
                $this->session->getFlashBag()->add('danger', 'Cette filière ne peut pas etre supprimer');
            } else {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($filiere);
                $entityManager->flush();
                $this->addFlash("info", "Filière supprimer Avec Succés");
            }
        }
        return $this->redirectToRoute('filiere_index');
    }
}
