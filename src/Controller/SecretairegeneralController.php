<?php

namespace App\Controller;

use App\Repository\CentreExamenRepository;
use App\Repository\DocumentRepository;
use App\Repository\EnseignantRepository;
use App\Repository\FiliereRepository;
use App\Repository\MatiereRepository;
use App\Repository\NiveauRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class SecretairegeneralController extends AbstractController
{
    /**
     * @Route("/secretairegeneral", name="secretairegeneral")
     */
    public function index(FiliereRepository $filiereRepository,NiveauRepository $niveauRepository,EnseignantRepository $enseignantRepository,DocumentRepository $documentRepository,MatiereRepository $matiereRepository,CentreExamenRepository $centreExamenRepository)
    {
        $mat=$matiereRepository->findAll();
        $ens=$enseignantRepository->findBy(['avec_contrat' => 1]);
        $fil=$filiereRepository->findAll();
        $niv=$niveauRepository->findAll();
        $cen=$centreExamenRepository->findAll();
        $user=$this->getUser();

        $documents=$user->getDocuments();
        return $this->render('secretairegeneral/index.html.twig', [
            'controller_name' => 'SecretairegeneralController',
            'mat' => $mat,
            'ens' => $ens,
            'doc'=>$documents,
            'cen'=>$cen,
            'fil'=>$fil,
            'niv'=>$niv,
        ]);
    }
}
