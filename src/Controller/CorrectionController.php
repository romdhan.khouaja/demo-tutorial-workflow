<?php

namespace App\Controller;

use App\Entity\Correction;
use App\Form\CorrectionType;
use App\Repository\CorrectionRepository;
use App\Repository\ExamenRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/correction")
 * @Security("is_granted('ROLE_CHEF_DEPARTEMENT')")
 */
class CorrectionController extends AbstractController
{
    protected $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @Route("/", name="correction_index", methods={"GET"})
     */
    public function index(CorrectionRepository $correctionRepository): Response
    {
        return $this->render('correction/index.html.twig', [
            'corrections' => $correctionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}/newcorrection", name="correction_new", methods={"GET","POST"})
     */
    public function correction(ExamenRepository $examenRepository, CorrectionRepository $correctionRepository, Request $request, $id = null): Response
    {
        $examen = $examenRepository->find($id);
        $correction = new Correction();
        $form = $this->createForm(CorrectionType::class, $correction);
        $form->handleRequest($request);
        $examens = $examenRepository->findAll();

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $correction->setExamen($examen);
            $entityManager->persist($correction);
            $entityManager->persist($examen);
            $entityManager->flush();
            $this->addFlash("info", "Quota Ajouter Avec Succés");
            return new Response('ok');
        }

        return $this->render('correction/_form.html.twig', [
            'correction' => $correction,
            'examens' => $examens,
            'examan' => $examen,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="correction_show", methods={"GET"})
     */
    public function show(Correction $correction): Response
    {
        return $this->render('correction/show.html.twig', [
            'correction' => $correction,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="correction_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Correction $correction): Response
    {
        $form = $this->createForm(CorrectionType::class, $correction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash("info", "Quota Modifier Avec Succés");
            return new Response('ok');
        }

        return $this->render('correction/_form_edit.html.twig', [
            'correction' => $correction,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="correction_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Correction $correction): Response
    {
        if ($this->isCsrfTokenValid('delete' . $correction->getId(), $request->request->get('_token'))) {
            $ex_id=$correction->getExamen()->getId();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($correction);
            $entityManager->flush();
            $this->addFlash("info", "Quota Supprimer Avec Succés");
            return $this->redirectToRoute('examen_correction', ['id' => $ex_id]);
        }


    }
}
