<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserModifType;
use App\Form\ChangePasswordType;
use App\Form\UserType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/utilisateur")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/profile", name="profile_edit")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_CHEF_DEPARTEMENT') or is_granted('ROLE_SCOLARITE') or is_granted('ROLE_AGENT') or is_granted('ROLE_DOYEN') or is_granted('ROLE_SECRETAIRE_GENERAL')")
     */
    public function profile(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {

        $user = $this->getUser();
        $form = $this->createForm(ChangePasswordType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $oldpwd = $form->get('oldPassword')->getData();
            $state = $passwordEncoder->isPasswordValid($user, $oldpwd);
            // test si le mot de passe est parreil à l'ancien
            if (!$state) {
                // message flash
                $request->getSession()->getFlashBag()->add('warning', 'Votre ancien mot de passe est incorrect !');

                return $this->redirectToRoute('profile_edit');
            } else {
                $newpwd = $form->get('plainPassword')->getData();

                $newEncodedPassword = $passwordEncoder->encodePassword($user, $newpwd);
                $user->setPassword($newEncodedPassword);
                $this->getDoctrine()->getManager()->flush();
                $request->getSession()->getFlashBag()->add('success', 'Votre  mot de passe a été modifié avec succés !');


                return new Response('ok');
            }

        }

        return $this->render('user/profile.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/user", name="user_index", methods={"GET","POST"})
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder, UserRepository $userRepository, PaginatorInterface $paginator): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            $this->addFlash("info", "Utilisateur Ajouter Avec Succés");
            // do anything else you need here, like send an email

            return $this->redirectToRoute('user_index');
        }
        $allusers = $userRepository->findAll();
        // Paginate the results of the query
        $users = $paginator->paginate(
        // Doctrine Query, not results
            $allusers,
            // Define the page parameter
            $request->query->getInt('page', 1),
            // Items per page
            5
        );
        return $this->render('user/index.html.twig', [
            'users' => $users,
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/info", name="info_index", methods={"GET","POST"})
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_CHEF_DEPARTEMENT') or is_granted('ROLE_SCOLARITE') or is_granted('ROLE_AGENT') or is_granted('ROLE_DOYEN') or is_granted('ROLE_SECRETAIRE_GENERAL')")
     */
    public function info(Request $request, UserPasswordEncoderInterface $passwordEncoder, UserRepository $userRepository, PaginatorInterface $paginator): Response
    {

        return $this->render('user/profil.html.twig', [

        ]);
    }

    /**
     * @Route("/new", name="user_new", methods={"GET","POST"})
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function new(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            $this->addFlash("info", "Utilisateur Ajouter Avec Succés");
            return new Response('ok');

        }

        return $this->render('user/_form.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_show", methods={"GET"})
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function show(User $user): Response
    {
        return $this->render('user/show.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function edit(Request $request, User $user): Response
    {
        $form = $this->createForm(UserModifType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("info", "Utilisateur Ajouter Avec Succés");
            return new Response('ok');
        }

        return $this->render('user/_form_edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }
    /**
     * @Route("/edit_user", name="edit_user", methods={"GET","POST"})
     */
    public function edituser(UserRepository $userRepository, Request $request): Response
    {
        $id_us = $request->request->get('id_us');
        $etat = $request->request->get('etat');
        $user = $userRepository->find($id_us);
        if ($etat == 1) {
            $new_etat = 0;
        } else {
            $new_etat = 1;
        }
        $user->setIsActive($new_etat);
        $this->getDoctrine()->getManager()->flush();
        return new Response($new_etat);

    }
    /**
     * @Route("/{id}", name="user_delete", methods={"DELETE"})
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete' . $user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_index');
    }

}
