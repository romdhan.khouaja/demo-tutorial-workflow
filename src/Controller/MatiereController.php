<?php

namespace App\Controller;

use App\Entity\Matiere;
use App\Form\MatiereType;
use App\Form\MatiereSearchType;
use App\Repository\MatiereRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Session\SessionInterface;


/**
 * @Route("/matiere")
 * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_CHEF_DEPARTEMENT') or is_granted('ROLE_DOYEN') or is_granted('ROLE_SECRETAIRE_GENERAL') or is_granted('ROLE_SCOLARITE')")
 */
class MatiereController extends AbstractController
{
    protected $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @Route("/", name="matiere_index", methods={"GET","POST"})
     */
    public function index(MatiereRepository $matiereRepository, Request $request, PaginatorInterface $paginator): Response
    {
        $form_search = $this->createForm(MatiereSearchType::class, null, ['responsable' => $this->getUser()->getDepartement()]);
        $form_search->handleRequest($request);
        if ($form_search->isSubmitted()) {
            $url = $this->getUrlSearch($request->request->get('matiere_search'));

            return $this->redirectToRoute('matiere_index', $url);

        }
        $allmatieres = $matiereRepository->searchMatiere($request->query->all());
        if ($this->getUser()->isChefDepartement()) {
            $allmatieres = $matiereRepository->searchMatiere($request->query->all(), $this->getUser()->getDepartement());
        }
        // Paginate the results of the query
        $matieres = $paginator->paginate(
        // Doctrine Query, not results
            $allmatieres,
            // Define the page parameter
            $request->query->getInt('page', 1),
            // Items per page
            10
        );
        return $this->render('matiere/index.html.twig', [
            'matieres' => $matieres,

            'form_search' => $form_search->createView(),
        ]);
    }

    public function getUrlSearch($request)
    {
        $url = [];
        if (isset($request['code_matiere']) && !empty($request['code_matiere'])) {
            $url['code_matiere'] = $request['code_matiere'];
        }
        if (isset($request['coefficient']) && !empty($request['coefficient'])) {
            $url['coefficient'] = $request['coefficient'];
        }
        if (isset($request['libelle_matiere_fr']) && !empty($request['libelle_matiere_fr'])) {
            $url['libelle_matiere_fr'] = $request['libelle_matiere_fr'];
        }
        if (isset($request['niveau']) && !empty($request['niveau'])) {
            $url['niveau'] = $request['niveau'];
        }
        if (isset($request['module']) && !empty($request['module'])) {
            $url['module'] = $request['module'];
        }
        if (isset($request['semestre']) && !empty($request['semestre'])) {
            $url['semestre'] = $request['semestre'];
        }
        if (isset($request['typematiere']) && !empty($request['typematiere'])) {
            $url['typematiere'] = $request['typematiere'];
        }
        return $url;
    }

    /**
     * @Route("/new-ajax", name="matiere_new_ajax", methods={"GET","POST"})
     */
    public function newAjax(Request $request): Response
    {
        $matiere = new Matiere();
        $form = $this->createForm(MatiereType::class, $matiere, ['responsable' => $this->getUser()->getDepartement()]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($matiere);
            $entityManager->flush();
            $this->addFlash("info", "Matiére Ajouter Avec Succés");
            return new Response('ok');
        }

        return $this->render('matiere/_form.html.twig', [
            'matiere' => $matiere,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit-ajax", name="matiere_edit_ajax", methods={"GET","POST"})
     */
    public function editAjax(Matiere $matiere, Request $request): Response
    {

        $form = $this->createForm(MatiereType::class, $matiere, ['responsable' => $this->getUser()->getDepartement()]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("info", "Matiére modifier Avec Succés");
            return new Response('ok');
        }

        return $this->render('matiere/_form_edit.html.twig', [
            'matiere' => $matiere,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="matiere_show", methods={"GET"})
     */
    public function show(Matiere $matiere): Response
    {
        return $this->render('matiere/show.html.twig', [
            'matiere' => $matiere,
        ]);
    }

    /**
     * @Route("/{id}", name="matiere_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Matiere $matiere): Response
    {
        if ($this->isCsrfTokenValid('delete' . $matiere->getId(), $request->request->get('_token'))) {

            if ($matiere->getSemestre()->getSessions()->count() > 0) {
                $this->session->getFlashBag()->add('danger', 'Cette matière ne peut pas etre supprimer');
            } else {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($matiere);
                $entityManager->flush();
                $this->addFlash("info", "matière supprimer Avec Succés");
            }
        }
        return $this->redirectToRoute('matiere_index');
    }


}
