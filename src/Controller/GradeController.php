<?php

namespace App\Controller;

use App\Entity\Grade;
use App\Form\GradeType;
use App\Repository\GradeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/grade")
 * @Security("is_granted('ROLE_ADMIN')")
 */
class GradeController extends AbstractController
{
    /**
     * @Route("/", name="grade_index", methods={"GET","POST"})
     */
    public function index(GradeRepository $gradeRepository, Request $request, PaginatorInterface $paginator): Response
    {
        $grade = new Grade();
        $form = $this->createForm(GradeType::class, $grade);
        $form->handleRequest($request);
        $allgrades = $gradeRepository->findAll();
        // Paginate the results of the query
        $grades = $paginator->paginate(
        // Doctrine Query, not results
            $allgrades,
            // Define the page parameter
            $request->query->getInt('page', 1),
            // Items per page
            5
        );
        return $this->render('grade/index.html.twig', [
            'grades' => $grades,
            'grade' => $grade,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/new", name="grade_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $grade = new Grade();
        $form = $this->createForm(GradeType::class, $grade);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($grade);
            $entityManager->flush();
            $this->addFlash("info", "Grade Ajouter Avec Succés");
            return new Response('ok');
        }

        return $this->render('grade/_form.html.twig', [
            'grade' => $grade,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="grade_show", methods={"GET"})
     */
    public function show(Grade $grade): Response
    {
        return $this->render('grade/show.html.twig', [
            'grade' => $grade,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="grade_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Grade $grade): Response
    {
        $form = $this->createForm(GradeType::class, $grade);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("info", "Grade Modifier Avec Succés");
            return new Response('ok');
        }

        return $this->render('grade/_form_edit.html.twig', [
            'grade' => $grade,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="grade_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Grade $grade): Response
    {
        if ($this->isCsrfTokenValid('delete' . $grade->getId(), $request->request->get('_token'))) {
            if($grade->getEnseignants()->count()>0){
                $this->addFlash("danger", " Ce Grade ne peut pas être supprimer ");
            }
            else{
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($grade);
            $entityManager->flush();
            $this->addFlash("info", "Grade supprimer Avec Succés");
        }
        }
        return $this->redirectToRoute('grade_index');
    }
}
