<?php

namespace App\Controller;

use App\Repository\CentreExamenRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\UserRepository;
use App\Repository\EnseignantRepository;
use App\Repository\DocumentRepository;
use App\Repository\MailBoxRepository;
use App\Repository\ExamenRepository;

class ScolariteController extends AbstractController
{
    /**
     * @Route("/scolarite", name="scolarite")
     */
    public function index(ExamenRepository $examenRepository,MailBoxRepository $mailBoxRepository,UserRepository $userRepository,CentreExamenRepository $centreExamenRepository,EnseignantRepository $enseignantRepository,DocumentRepository $documentRepository)
    {
        $ens=$enseignantRepository->findBy(['avec_contrat' => 1]);
        $cen=$centreExamenRepository->findAll();
        $mail=$mailBoxRepository->findAll();
        $exm=$examenRepository->findAll();
        $user=$this->getUser();
        $documents=$user->getDocuments();
        return $this->render('scolarite/index.html.twig', [
            'controller_name' => 'ScolariteController',
            'ens' => $ens,
            'doc'=>$documents,
            'cen'=>$cen,
            'mail'=>$mail,
            'exm'=>$exm,
        ]);
    }
}
