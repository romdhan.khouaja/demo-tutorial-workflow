<?php

namespace App\Controller;

use App\Entity\Qualiter;
use App\Form\QualiterType;
use App\Repository\QualiterRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/qualiter")
 * @Security("is_granted('ROLE_ADMIN')")
 */
class QualiterController extends AbstractController
{
    /**
     * @Route("/", name="qualiter_index", methods={"GET","POST"})
     */
    public function index(QualiterRepository $qualiterRepository, Request $request): Response
    {
        $qualiter = new Qualiter();
        $form = $this->createForm(QualiterType::class, $qualiter);
        $form->handleRequest($request);


        return $this->render('qualiter/index.html.twig', [
            'qualiters' => $qualiterRepository->findAll(), 'qualiter' => $qualiter,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/new", name="qualiter_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $qualiter = new Qualiter();
        $form = $this->createForm(QualiterType::class, $qualiter);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($qualiter);
            $entityManager->flush();
            $this->addFlash("info", "Qualiter Ajouter Avec Succés");
            return new Response('ok');
        }

        return $this->render('qualiter/_form.html.twig', [
            'qualiter' => $qualiter,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="qualiter_show", methods={"GET"})
     */
    public function show(Qualiter $qualiter): Response
    {
        return $this->render('qualiter/show.html.twig', [
            'qualiter' => $qualiter,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="qualiter_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Qualiter $qualiter): Response
    {
        $form = $this->createForm(QualiterType::class, $qualiter);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("info", "Qualiter Modifier Avec Succés");
            return new Response('ok');
        }

        return $this->render('qualiter/_form_edit.html.twig', [
            'qualiter' => $qualiter,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="qualiter_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Qualiter $qualiter): Response
    {
        if ($this->isCsrfTokenValid('delete' . $qualiter->getId(), $request->request->get('_token'))) {
            if ($qualiter->getEnseignants()->count() > 0) {
                $this->addFlash("danger", "Cet Qualiter ne peut pas etre supprimer");
            } else {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($qualiter);
                $entityManager->flush();
                $this->addFlash("info", "Qualiter supprimer Avec Succés");
            }
        }
        return $this->redirectToRoute('qualiter_index');
    }
}
