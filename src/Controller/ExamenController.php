<?php

namespace App\Controller;

use App\Entity\Correction;
use App\Entity\Examen;
use App\Entity\Session;
use App\Entity\GroupeExamen;
use App\Form\ExamenType;
use App\Form\SessionSearchType;
use App\Repository\ConfigurationRepository;
use App\Repository\CorrectionRepository;
use App\Repository\EnseignantRepository;
use App\Repository\ExamenRepository;
use App\Repository\MatiereRepository;
use App\Repository\NiveauRepository;
use App\Repository\SessionRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\GroupeExamenRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Dompdf\Dompdf;
use Dompdf\Options;

/**
 * @Route("/examen")
 *
 */
class ExamenController extends AbstractController
{
    private $em;
    protected $session;

    public function __construct(EntityManagerInterface $em, SessionInterface $session)
    {
        $this->em = $em;
        $this->session = $session;
    }

    /**
     * @Route("/", name="examen_index", methods={"GET","POST"})
     * @Security(" is_granted('ROLE_CHEF_DEPARTEMENT') or is_granted('ROLE_SCOLARITE') or is_granted('ROLE_DOYEN') or is_granted('ROLE_SECRETAIRE_GENERAL')or is_granted('ROLE_AGENT')")
     */
    public function index(ExamenRepository $examenRepository, MatiereRepository $matiereRepository, Request $request, SessionRepository $sessionRepository, PaginatorInterface $paginator): Response
    {
        $form_search = $this->createForm(SessionSearchType::class);
        $form_search->handleRequest($request);
        if ($form_search->isSubmitted()) {
            $url = $this->getUrlSearch($request->request->get('session_search'));

            return $this->redirectToRoute('examen_index', $url);
        }
        $allsessions = [];
        if (isset($request->query->all()['session']) and !empty($request->query->all()['session'])) {
            $allsessions = $examenRepository->searchSession($request->query->all());
        }

        $examens = $paginator->paginate(
        // Doctrine Query, not results
            $allsessions,
            // Define the page parameter
            $request->query->getInt('page', 1),
            // Items per page
            6
        );

        $validEtp1_session = 0;
        $validEtp2_session = 0;
        $tab_depart_valid = [];
        if (isset($request->query->all()['session']) and !empty($request->query->all()['session'])) {
            $session = $sessionRepository->find($request->query->all()['session']);
            if ($session->getValideEtp1() == 1) {
                $validEtp1_session = 1;
            }
            if ($session->getValideEtp2() == 1) {
                $validEtp2_session = 1;
            }
            $tab_depart_valid = explode(';', $session->getDepartValid());
        }
        return $this->render('examen/index.html.twig', [
            'examens' => $examens,
            'validEtp1_session' => $validEtp1_session,
            'validEtp2_session' => $validEtp2_session,
            'tab_depart_valid' => $tab_depart_valid,
            'form_search' => $form_search->createView(),
        ]);
    }

    public function getUrlSearch($request)
    {
        $url = [];
        if (isset($request['session']) && !empty($request['session'])) {
            $url['session'] = $request['session'];
        }
        if (isset($request['niveau']) && !empty($request['niveau'])) {
            $url['niveau'] = $request['niveau'];
        }
        return $url;
    }

    /**
     * @Route("/{id}/calendrierparniveau", name="calendrier_niveau", methods={"GET"})
     * @Security(" is_granted('ROLE_SCOLARITE') ")
     */
    public function calendrierniveau(Session $session, ConfigurationRepository $configurationRepository, NiveauRepository $niveauRepository, ExamenRepository $examenRepository): Response
    {
        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'courier');
        //$pdfOptions->set('isHtml5ParserEnabled', true);

        $pdfOptions->set('DOMPDF_ENABLE_FONTSUBSETTING', true);
        $pdfOptions->set('DOMPDF_UNICODE_ENABLED', true);
        $pdfOptions->setIsRemoteEnabled(true);
        $dompdf = new Dompdf($pdfOptions);

        $dompdf->output();
        $niveaux = $niveauRepository->findAll();
        $examens = $examenRepository->findBy(['session' => $session->getId()]);

        $tab_niveau = [];
        foreach ($niveaux as $niveau) {
            $tab_niveau[$niveau->getId()] = $niveau->getLibelleNiveauAr();
        }

        $tab_examen = [];
        foreach ($examens as $examen) {
            $tab_examen[$examen->getNiveau()->getId()][] = $examen;
        }
        $d = date('Y-m-d H:i:s');
        // Retrieve the HTML generated in our twig file
        $html = $this->renderView('examen/calendrierparniveau.html.twig', [
            'tab_examen' => $tab_examen,
            'tab_niveau' => $tab_niveau,
            'session' => $session,
          'configuration' => $configurationRepository->find(3),
            'dt' => $d
        ]);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html, 'UTF-8');

        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'landscape');
        // $dompdf->set_option('isRemoteEnabled', true);

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser (force download)
        $dompdf->stream("mypdf.pdf", [
            "Attachment" => false
        ]);

    }

    /**
     * @Route("{id}/convocation", name="convocation_ens", methods={"GET"})
     * @Security(" is_granted('ROLE_SCOLARITE') ")
     */
    public function convocation(Session $session, EnseignantRepository $enseignantRepository, ConfigurationRepository $configurationRepository): Response
    {
        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'courier');
        //$pdfOptions->set('isHtml5ParserEnabled', true);

        $pdfOptions->set('DOMPDF_ENABLE_FONTSUBSETTING', true);
        $pdfOptions->set('DOMPDF_UNICODE_ENABLED', true);
        $pdfOptions->setIsRemoteEnabled(true);
        $dompdf = new Dompdf($pdfOptions);

        $dompdf->output();
        $enseignants = $enseignantRepository->printConvocation($session->getId());

        /*foreach ($enseignants as $enseignant) {
            echo $enseignant->getId() . "-";
        }*/
        $d = date('Y-m-d H:i:s');
        // Retrieve the HTML generated in our twig file
        $tab_jour = ['Monday' => 'الإثنين', 'Tuesday' => 'الثلاثاء', 'Wednesday' => 'الأربعاء', 'Thursday' => 'الخميس', 'Friday' => 'الجمعة', 'saturday' => 'السبت'];
        $tab_mois = ['January' => 'جانفي', 'February' => 'فيفري', 'March' => 'مارس', 'April' => 'أفريل', 'May' => 'ماي', 'Jun' => 'جوان', 'July' => 'جويلية', 'August' => 'أوت', 'Septembre' => 'سبتمبر', 'October' => 'أكتوبر', 'November' => 'نوفمبر', 'December' => 'ديسمبر',];
        $html = $this->renderView('examen/convocation.html.twig', [
            'enseignants' => $enseignants,
            'session' => $session,
            'tab_jour' => $tab_jour,
            'tab_mois' => $tab_mois,
            'configuration' => $configurationRepository->find(3),
            'dt' => $d
        ]);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html, 'UTF-8');

        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');
        // $dompdf->set_option('isRemoteEnabled', true);

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser (force download)
        $dompdf->stream("mypdf.pdf", [
            "Attachment" => false
        ]);

    }

    /**
     * @Route("{id}/correction_part", name="correction_part", methods={"GET"})
     * @Security(" is_granted('ROLE_SCOLARITE') ")
     */
    public function correctionPart(Session $session, ExamenRepository $examenRepository, ConfigurationRepository $configurationRepository): Response
    {
        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'courier');
        //$pdfOptions->set('isHtml5ParserEnabled', true);

        $pdfOptions->set('DOMPDF_ENABLE_FONTSUBSETTING', true);
        $pdfOptions->set('DOMPDF_UNICODE_ENABLED', true);
        $pdfOptions->setIsRemoteEnabled(true);
        $dompdf = new Dompdf($pdfOptions);

        $dompdf->output();
        $examens = $examenRepository->examenPart($session->getId());

        /* foreach ($examens as $examen) {
             echo $examen->getDateexamen()->format('Y-m-d '); "--";
         }
         exit();*/
        $d = date('Y-m-d H:i:s');
        // Retrieve the HTML generated in our twig file

        $html = $this->renderView('examen/correction_part.html.twig', [
            'examens' => $examens,
            'session' => $session,
            'configuration' => $configurationRepository->find(1),
            'dt' => $d
        ]);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html, 'UTF-8');

        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');
        // $dompdf->set_option('isRemoteEnabled', true);

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser (force download)
        $dompdf->stream("mypdf.pdf", [
            "Attachment" => false
        ]);

    }

    /**
     * @Route("/{id}/centrejourexamen", name="salle_surveille", methods={"GET"})
     * @Security(" is_granted('ROLE_SCOLARITE') ")
     */
    public function sallesurveille(Session $session, ExamenRepository $examenRepository, GroupeExamenRepository $groupeExamenRepository): Response
    {
        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'courier');
        //$pdfOptions->set('isHtml5ParserEnabled', true);

        $pdfOptions->set('DOMPDF_ENABLE_FONTSUBSETTING', true);
        $pdfOptions->set('DOMPDF_UNICODE_ENABLED', true);
        $pdfOptions->setIsRemoteEnabled(true);
        $dompdf = new Dompdf($pdfOptions);

        $dompdf->output();
        // $examens = $examenRepository->findBy(['session' => $session->getId()]);
        $examens = $examenRepository->examenCentre($session->getId());
        $groupes = $groupeExamenRepository->findAll();
        $d = date('Y-m-d H:i:s');
        // Retrieve the HTML generated in our twig file
        $html = $this->renderView('examen/centreparjour.html.twig', [
            'session' => $session,
            'examens' => $examens,
            'groupes' => $groupes,
            'dt' => $d
        ]);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html, 'UTF-8');

        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');
        // $dompdf->set_option('isRemoteEnabled', true);

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser (force download)
        $dompdf->stream("mypdf.pdf", [
            "Attachment" => false
        ]);

    }


    /**
     * @Route("/correction/{id}", name="examen_correction", methods={"GET"})
     * @Security(" is_granted('ROLE_CHEF_DEPARTEMENT') ")
     */
    public function correction(Examen $examen, CorrectionRepository $correctionRepository): Response
    {
        $correction = $this->getDoctrine()
            ->getRepository(Correction::class)
            ->findBy(['examen' => $examen]);
        //  $nbre_part_rest=$correction->;

        $part_restant = 1;

        foreach ($examen->getCorrections() as $correct) {
            $part_restant -= $correct->getPart();
        }
        return $this->render('examen/correction.html.twig', [
            'examan' => $examen,
            'correction' => $correction,
            'part_restant' => $part_restant,
        ]);
    }

    /**
     * @Route("/groupe/{id}", name="examen_show", methods={"GET"})
     * @Security(" is_granted('ROLE_SCOLARITE') or is_granted('ROLE_AGENT')")
     */
    public function show(Examen $examen, ExamenRepository $examenRepository): Response
    {
        $grp = $this->getDoctrine()
            ->getRepository(GroupeExamen::class)
            ->findBy(['examen' => $examen]);


        $nbre_elev_rest = $examen->getNombreetudiant();

        foreach ($examen->getGroupeExamens() as $groupeExamen) {
            $nbre_elev_rest -= $groupeExamen->getNbretudiantcentre();
        }

        return $this->render('examen/show.html.twig', [
            'examan' => $examen,
            'groupe_examan' => $grp,
            'nbre_elev_rest' => $nbre_elev_rest,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="examen_edit", methods={"GET","POST"})
     * @Security("is_granted('ROLE_DOYEN')or is_granted('ROLE_CHEF_DEPARTEMENT') or is_granted('ROLE_SCOLARITE') or is_granted('ROLE_SECRETAIRE_GENERAL')")
     */
    public function edit(Request $request, Examen $examan): Response
    {
        $form = $this->createForm(ExamenType::class, $examan);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // recupere la date sans heure minute
            $heuredebut = $form->get('heureexamen')->getData();
            $matiere = $examan->getMatiere();
            $heureDeFin = $heuredebut + $matiere->getDure();
            $examan->setHeureexamenfin($heureDeFin);
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("info", "Examen modifier Avec Succés");
            return new Response('ok');
        }

        return $this->render('examen/_form_edit.html.twig', [
            'examan' => $examan,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="examen_delete", methods={"DELETE"})
     * @Security(" is_granted('ROLE_SCOLARITE') ")
     */
    public function delete(Request $request, Examen $examan): Response
    {
        if ($this->isCsrfTokenValid('delete' . $examan->getId(), $request->request->get('_token'))) {
            if ($examan->getGroupeExamens()->count() > 0) {
                $this->addFlash("danger", "Cet Examen ne peut pas etre supprimer ");
            } else {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($examan);
                $entityManager->flush();
                $this->addFlash("info", "Examen supprimer Avec Succés");
            }
        }
        return $this->redirectToRoute('examen_index');
    }
}
