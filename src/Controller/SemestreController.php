<?php

namespace App\Controller;

use App\Entity\Semestre;
use App\Form\SemestreType;
use App\Repository\SemestreRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * @Route("/semestre")
 * @Security("is_granted('ROLE_ADMIN')")
 */
class SemestreController extends AbstractController
{
    protected $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @Route("/", name="semestre_index", methods={"GET","POST"})
     */
    public function index(SemestreRepository $semestreRepository, Request $request): Response
    {
        $semestre = new Semestre();
        $form = $this->createForm(SemestreType::class, $semestre);
        $form->handleRequest($request);


        return $this->render('semestre/index.html.twig', [
            'semestres' => $semestreRepository->findAll(), 'semestre' => $semestre,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/new", name="semestre_ajout", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $semestre = new Semestre();
        $form = $this->createForm(SemestreType::class, $semestre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($semestre);
            $entityManager->flush();
            $this->addFlash("info", "Semestre ajouter Avec Succés");
            return new Response('ok');
        }

        return $this->render('semestre/_form.html.twig', [
            'semestre' => $semestre,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="semestre_show", methods={"GET"})
     */
    public function show(Semestre $semestre): Response
    {
        return $this->render('semestre/show.html.twig', [
            'semestre' => $semestre,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="semestre_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Semestre $semestre): Response
    {
        $form = $this->createForm(SemestreType::class, $semestre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
             $this->addFlash("info","Semestre Modifier Avec Succés");
            return new Response('ok');
        }

        return $this->render('semestre/_form_edit.html.twig', [
            'semestre' => $semestre,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="semestre_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Semestre $semestre): Response
    {
        if ($this->isCsrfTokenValid('delete' . $semestre->getId(), $request->request->get('_token'))) {
            if ($semestre->getSessions()->count() > 0) {
                $this->addFlash('danger', 'Cette semestre ne peut pas etre supprimer');
            } else {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($semestre);
                $entityManager->flush();
                $this->addFlash("info", "Semestre supprimé Avec Succés");
            }
        }
        return $this->redirectToRoute('semestre_index');
    }
}
