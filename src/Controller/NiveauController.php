<?php

namespace App\Controller;

use App\Entity\Niveau;
use App\Form\NiveauType;
use App\Form\NiveauSearchType;
use App\Repository\NiveauRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Dompdf\Dompdf;
use Dompdf\Options;

/**
 * @Route("/niveau")
 * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_CHEF_DEPARTEMENT')")
 */
class NiveauController extends AbstractController
{
    protected $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @Route("/", name="niveau_index", methods={"GET","POST"})
     */
    public function index(NiveauRepository $niveauRepository, Request $request, PaginatorInterface $paginator): Response
    {
        $niveau = new Niveau();
        $form = $this->createForm(NiveauType::class, $niveau);
        $form->handleRequest($request);
        $form_search = $this->createForm(NiveauSearchType::class, null, ['responsable' => $this->getUser()->getDepartement()]);
        $form_search->handleRequest($request);
        if ($form_search->isSubmitted()) {
            $url = $this->getUrlSearch($request->request->get('niveau_search'));

            return $this->redirectToRoute('niveau_index', $url);

        }
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($niveau);
            $entityManager->flush();
            $this->addFlash("info", "Niveau Ajouter Avec Succés");
            return $this->redirectToRoute('niveau_index');
        }
        $allniveaux = $niveauRepository->searchNiveau($request->query->all());

        if ($this->getUser()->isChefDepartement()) {
            $allniveaux = $niveauRepository->searchNiveau($request->query->all(), $this->getUser()->getDepartement());
        }

        // Paginate the results of the query
        $niveaux = $paginator->paginate(
        // Doctrine Query, not results
            $allniveaux,
            // Define the page parameter
            $request->query->getInt('page', 1),
            // Items per page
            5
        );
        return $this->render('niveau/index.html.twig', [
            'niveaux' => $niveaux,
            'niveau' => $niveau,
            'form' => $form->createView(),
            'form_search' => $form_search->createView(),
        ]);
    }

    public function getUrlSearch($request)
    {
        $url = [];
        if (isset($request['code_niveau']) && !empty($request['code_niveau'])) {
            $url['code_niveau'] = $request['code_niveau'];
        }
        if (isset($request['libelle_niveau_fr']) && !empty($request['libelle_niveau_fr'])) {
            $url['libelle_niveau_fr'] = $request['libelle_niveau_fr'];
        }
        if (isset($request['libelle_niveau_ar']) && !empty($request['libelle_niveau_ar'])) {
            $url['libelle_niveau_ar'] = $request['libelle_niveau_ar'];
        }
        if (isset($request['filiere']) && !empty($request['filiere'])) {
            $url['filiere'] = $request['filiere'];
        }
        return $url;
    }

    /**
     * @Route("/listniveau", name="niveau_list", methods={"GET"})
     */
    public function listn(NiveauRepository $niveauRepository): Response
    {
        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'courier');
        //$pdfOptions->set('isHtml5ParserEnabled', true);

        $pdfOptions->set('DOMPDF_ENABLE_FONTSUBSETTING', true);
        $pdfOptions->set('DOMPDF_UNICODE_ENABLED', true);
        $pdfOptions->setIsRemoteEnabled(true);
        $dompdf = new Dompdf($pdfOptions);

        $niveaux = $niveauRepository->findAll();
        if ($this->getUser()->isChefDepartement()) {
            $niveaux = $niveauRepository->findAllNiveau($this->getUser()->getDepartement());
        }
        $comp = 0;
        $page=1;
        foreach ($niveaux as $niveau) {
            $tab_niveau[$page][]=$niveau;
            $comp++;
            if ($comp == 10) {
                $comp = 0;
                $page++;
            }
        }
        $dompdf->output();
        $d = date('Y-m-d H:i:s');
        // Retrieve the HTML generated in our twig file
        $html = $this->renderView('niveau/listeniveau.html.twig', [
            'tab_niveau' => $tab_niveau, 'dt' => $d
        ]);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html, 'UTF-8');

        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser (force download)
        $dompdf->stream("mypdf.pdf", [
            "Attachment" => false
        ]);
    }


    /**
     * @Route("/new-ajax", name="niveau_new_ajax", methods={"GET","POST"})
     */
    public function newAjax(Request $request): Response
    {
        $niveau = new Niveau();
        $form = $this->createForm(NiveauType::class, $niveau, ['responsable' => $this->getUser()->getDepartement()]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($niveau);
            $entityManager->flush();
            $this->addFlash("info", "Niveau Ajouter Avec Succés");
            return new Response('ok');
        }

        return $this->render('niveau/_form.html.twig', [
            'niveau' => $niveau,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="niveau_show", methods={"GET"})
     */
    public function show(Niveau $niveau): Response
    {
        return $this->render('niveau/show.html.twig', [
            'niveau' => $niveau,
        ]);
    }

    /**
     * @Route("/{id}/edit-ajax", name="niveau_edit_ajax", methods={"GET","POST"})
     */
    public function editAjax(Niveau $niveau, Request $request): Response
    {

        $form = $this->createForm(NiveauType::class, $niveau, ['responsable' => $this->getUser()->getDepartement()]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("info", "Niveau modifier Avec Succés");
            return new Response('ok');
        }

        return $this->render('niveau/_form_edit.html.twig', [
            'niveau' => $niveau,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="niveau_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Niveau $niveau): Response
    {
        if ($this->isCsrfTokenValid('delete' . $niveau->getId(), $request->request->get('_token'))) {
            if ($niveau->getMatieres()->count() > 0) {
                $this->session->getFlashBag()->add('danger', 'Cette niveau ne peut pas étre supprimer');
            } else {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($niveau);
                $entityManager->flush();
            }
        }
        return $this->redirectToRoute('niveau_index');
    }
}
