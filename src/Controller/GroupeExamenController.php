<?php

namespace App\Controller;

use App\Entity\Session;
use App\Entity\GroupeExamen;
use App\Form\GroupeExamenMatriculeType;
use App\Form\GroupeExamenType;
use App\Repository\CentreExamenRepository;
use App\Repository\ConfigurationRepository;
use App\Repository\EnseignantRepository;
use App\Repository\ExamenRepository;
use App\Repository\GroupeExamenRepository;
use App\Repository\MatiereRepository;
use App\Repository\NiveauRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Dompdf\Dompdf;
use Dompdf\Options;

/**
 * @Route("/groupe")
 */
class GroupeExamenController extends AbstractController
{
    protected $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @Route("/", name="groupe_examen_index", methods={"GET"})
     * @Security(" is_granted('ROLE_SCOLARITE') ")
     */
    public function index(GroupeExamenRepository $groupeExamenRepository): Response
    {
        return $this->render('groupe_examen/index.html.twig', [
            'groupe_examens' => $groupeExamenRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}/matricule", name="calendrier_matricule", methods={"GET"})
     * @Security(" is_granted('ROLE_SCOLARITE') ")
     */
    public function calendriermatricule(Session $session, ConfigurationRepository $configurationRepository, NiveauRepository $niveauRepository, MatiereRepository $matiereRepository, ExamenRepository $examenRepository, GroupeExamenRepository $groupeExamenRepository, CentreExamenRepository $centreExamenRepository): Response
    {
        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'courier');
        //$pdfOptions->set('isHtml5ParserEnabled', true);

        $pdfOptions->set('DOMPDF_ENABLE_FONTSUBSETTING', true);
        $pdfOptions->set('DOMPDF_UNICODE_ENABLED', true);
        $pdfOptions->setIsRemoteEnabled(true);
        $dompdf = new Dompdf($pdfOptions);

        $dompdf->output();
        $examens = $examenRepository->examenCalendrier($session->getId());
        $niveaux = $niveauRepository->selectNiveau($session->getId());


        $tab_niveau = [];
        foreach ($niveaux as $niveau) {
            $tab_niveau[$niveau->getId()] = $niveau->getLibelleNiveauAr();
        }

        $tab_examen = [];
        foreach ($niveaux as $niveau) {
            $tab_examen[$niveau->getId()][0][0] = "التوقيت";
        }
        foreach ($examens as $examen) {
            $tab_exam_niveau[$examen->getNiveau()->getId()][] = $examen;
        }
        /* foreach ($tab_exam_niveau as $t_exam_niveau) {
             foreach ($t_exam_niveau as $examen) {
                 echo $examen . "-";
             }
         }
         echo "<pre>" . print_r($tab_exam_niveau, 1) . "</pre>";
         exit();*/
        foreach ($tab_exam_niveau as $id_niveau => $t_exam_niveau) {
            $max_nb_groupe = 1;
            foreach ($t_exam_niveau as $examen) {
                if ($max_nb_groupe < count($examen->getGroupeExamens())) {
                    $max_nb_groupe = count($examen->getGroupeExamens());
                }
            }
            for ($c_g = 1; $c_g <= $max_nb_groupe; $c_g++) {
                foreach ($t_exam_niveau as $examen) {
                    $tab_examen[$id_niveau][$c_g][0] = '';
                    $tab_examen[$id_niveau][$c_g][$examen->getId()] = '';
                }

            }
            foreach ($t_exam_niveau as $examen) {
                $tab_examen[$id_niveau][0][$examen->getId()] = $examen->getDateexamen()->format('d/m/Y') . " " . $examen->getMatiere()->getLibelleMatiereAr();
                $c_group = 1;
                foreach ($examen->getGroupeExamens() as $groupeExamen) {

                    $tab_examen[$id_niveau][$c_group][0] = $examen->getHeureexamen();

                    $tab_examen[$id_niveau][$c_group][$examen->getId()] = $groupeExamen->getCentreexamen()->getLibelleCentre() . "-------" . $groupeExamen->getMatretuddebut() . "---->" . $groupeExamen->getMatretudfin();
                    $c_group++;
                }

            }
        }
// Retrieve the HTML generated in our twig file
        $html = $this->renderView('groupe_examen/calendrierparmatricule.html.twig', [
            'tab_niveau' => $tab_niveau,
            'tab_examen' => $tab_examen,
            'session' => $session,
            'configuration' => $configurationRepository->find(3),
        ]);

// Load HTML to Dompdf
        $dompdf->loadHtml($html, 'UTF-8');

// (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'landscape');
// $dompdf->set_option('isRemoteEnabled', true);

// Render the HTML as PDF
        $dompdf->render();

// Output the generated PDF to Browser (force download)
        $dompdf->stream("mypdf.pdf", [
            "Attachment" => false
        ]);

    }

    /**
     * @Route("/{id}/new", name="groupe_examen_new", methods={"GET","POST"})
     * @Security(" is_granted('ROLE_SCOLARITE') ")
     */
    public function new(Request $request, ExamenRepository $examenRepository, CentreExamenRepository $centreExamenRepository, EnseignantRepository $enseignantRepository, $id = null): Response
    {
        $examen = $examenRepository->find($id);
        $groupeExamen = new GroupeExamen();
        $form = $this->createForm(GroupeExamenType::class, $groupeExamen);
        $form->handleRequest($request);
        $examens = $examenRepository->findAll();

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $groupeExamen->setExamen($examen);

            $entityManager->persist($groupeExamen);
            $entityManager->flush();
            $this->addFlash("info", "Groupe Ajouter Avec Succés");
            return new Response('ok');

        }
        $enseignants = $enseignantRepository->enseignantOccupe($examen->getDateexamen(), $examen->getHeureexamen(), $examen->getHeureexamenfin());
        $tab_ens_occ = [];
        foreach ($enseignants as $enseignant) {
            $tab_ens_occ[] = $enseignant->getId();
        }

        $centres = $centreExamenRepository->centreOccupe($examen->getDateexamen(), $examen->getHeureexamen(), $examen->getHeureexamenfin());
        $tab_centre_occ = [];
        foreach ($centres as $centre) {
            $tab_centre_occ[] = $centre->getId();
        }

        return $this->render('groupe_examen/_form.html.twig', [
            'groupe_examan' => $groupeExamen,
            'examens' => $examens,
            'examan' => $examen,
            'tab_ens_occ' => $tab_ens_occ,
            'tab_centre_occ' => $tab_centre_occ,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("matricule/{id}", name="new_matricule", methods={"GET","POST"})
     * @Security(" is_granted('ROLE_AGENT') ")
     */
    public function matretud(Request $request, ExamenRepository $examenRepository, $id = null, GroupeExamen $groupeExamen): Response
    {
        $examen = $examenRepository->find($id);
        $form = $this->createForm(GroupeExamenMatriculeType::class, $groupeExamen);
        $form->handleRequest($request);
        $examens = $examenRepository->findAll();

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("info", "Matricule Ajouter Avec Succés");
            return new Response('ok');

        }
        return $this->render('groupe_examen/_matricule_form.html.twig', [
            'ge' => $groupeExamen,
            'examens' => $examens,
            'examan' => $examen,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}", name="groupe_examen_show", methods={"GET"})
     */
    public function show(GroupeExamen $groupeExaman): Response
    {
        return $this->render('groupe_examen/show.html.twig', [
            'groupe_examan' => $groupeExaman,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="groupe_examen_modif", methods={"GET","POST"})
     * @Security(" is_granted('ROLE_SCOLARITE') ")
     */
    public function edit_groupe(Request $request, GroupeExamen $groupeExamen): Response
    {
        $form = $this->createForm(GroupeExamenType::class, $groupeExamen);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash("info", "Groupe Modifier Avec Succés");
            return new Response('ok');
        }

        return $this->render('groupe_examen/_form_edit.html.twig', [
            'groupe_examan' => $groupeExamen,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="groupe_examen_delete", methods={"DELETE"})
     * @Security(" is_granted('ROLE_SCOLARITE') ")
     */
    public function delete(Request $request, GroupeExamen $groupeExaman): Response
    {
        if ($this->isCsrfTokenValid('delete' . $groupeExaman->getId(), $request->request->get('_token'))) {
            $exam_id = $groupeExaman->getExamen()->getId();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($groupeExaman);
            $entityManager->flush();
            $this->addFlash("info", "Groupe supprimer Avec Succés");
            return $this->redirectToRoute('examen_show', ['id' => $exam_id]);
        }

    }
}
