<?php

namespace App\Controller;

use App\Entity\CentreExamen;
use App\Form\CentreExamenType;
use App\Form\CentreSearchType;
use App\Repository\CentreExamenRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Dompdf\Dompdf;
use Dompdf\Options;

/**
 * @Route("/centreexamen")
 */
class CentreExamenController extends AbstractController
{
    /**
     * @Route("/", name="centre_examen_index", methods={"GET","POST"})
     *@Security(" is_granted('ROLE_ADMIN')")
     */
    public function index(CentreExamenRepository $centreExamenRepository, Request $request, PaginatorInterface $paginator): Response
    {


        $centreExaman = new CentreExamen();
        $form = $this->createForm(CentreExamenType::class, $centreExaman);
        $form->handleRequest($request);
        $form_search = $this->createForm(CentreSearchType::class);
        $form_search->handleRequest($request);

        if ($form_search->isSubmitted()) {
            $url = $this->getUrlSearch($request->request->get('centre_search'));

            return $this->redirectToRoute('centre_examen_index', $url);

        }
        $allexamen = $centreExamenRepository->searchCentre($request->query->all());
        // $allexamen = $centreExamenRepository->findAll();
// Paginate the results of the query
        $examens = $paginator->paginate(
        // Doctrine Query, not results
            $allexamen,
            // Define the page parameter
            $request->query->getInt('page', 1),
            // Items per page
            6
        );

        return $this->render('centre_examen/index.html.twig', [
            'centre_examens' => $examens,
            'centre_examan' => $centreExaman,
            'form' => $form->createView(),
            'form_search' => $form_search->createView(),
        ]);
    }

    public function getUrlSearch($request)
    {
        $url = [];
        if (isset($request['code_centre']) && !empty($request['code_centre'])) {
            $url['code_centre'] = $request['code_centre'];
        }
        if (isset($request['capacite']) && !empty($request['capacite'])) {
            $url['capacite'] = $request['capacite'];
        }

        return $url;
    }

    /**
     * @Route("/listcentre", name="centre_list", methods={"GET"})
     * @Security("is_granted('ROLE_SCOLARITE') or is_granted('ROLE_ADMIN') or is_granted('ROLE_DOYEN') or is_granted('ROLE_SECRETAIRE_GENERAL')")
     */
    public function listd(CentreExamenRepository $centreExamenRepository): Response
    {
        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'courier');
        //$pdfOptions->set('isHtml5ParserEnabled', true);

        $pdfOptions->set('DOMPDF_ENABLE_FONTSUBSETTING', true);
        $pdfOptions->set('DOMPDF_UNICODE_ENABLED', true);
        $pdfOptions->setIsRemoteEnabled(true);
        $dompdf = new Dompdf($pdfOptions);

        $centres = $centreExamenRepository->findAll();
        $comp = 0;
        $page=1;
        foreach ($centres as $centre) {
            $tab_centre[$page][]=$centre;
            $comp++;
            if ($comp == 15) {
                $comp = 0;
                $page++;
            }
        }
        $dompdf->output();
        $d = date('Y-m-d H:i:s');
        // Retrieve the HTML generated in our twig file
        $html = $this->renderView('centre_examen/listecentre.html.twig', [
            'centres' => $centres, 'tab_centre' => $tab_centre, 'dt' => $d
        ]);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html, 'UTF-8');

        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser (force download)
        $dompdf->stream("mypdf.pdf", [
            "Attachment" => false
        ]);


    }

    /**
     * @Route("/new", name="centre_examen_new", methods={"GET","POST"})
     * @Security(" is_granted('ROLE_ADMIN')")
     */
    public function new(Request $request): Response
    {
        $centreExaman = new CentreExamen();
        $form = $this->createForm(CentreExamenType::class, $centreExaman);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($centreExaman);
            $entityManager->flush();
            $this->addFlash("info", "Centre Examen ajouter Avec Succés");
            return new Response('ok');
        }

        return $this->render('centre_examen/_form.html.twig', [
            'centre_examan' => $centreExaman,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="centre_examen_show", methods={"GET"})
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function show(CentreExamen $centreExaman): Response
    {
        return $this->render('centre_examen/show.html.twig', [
            'centre_examan' => $centreExaman,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="centre_examen_edit", methods={"GET","POST"})
     *  @Security(" is_granted('ROLE_ADMIN')")
     */
    public function edit(Request $request, CentreExamen $centreExaman): Response
    {
        $form = $this->createForm(CentreExamenType::class, $centreExaman);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("info", "Centre Examen modifier Avec Succés");
            return new Response('ok');
        }

        return $this->render('centre_examen/_form_edit.html.twig', [
            'centre_examan' => $centreExaman,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="centre_examen_delete", methods={"DELETE"})
     *  @Security(" is_granted('ROLE_ADMIN')")
     */
    public function delete(Request $request, CentreExamen $centreExaman): Response
    {
        if ($this->isCsrfTokenValid('delete' . $centreExaman->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($centreExaman);
            $entityManager->flush();
        }

        return $this->redirectToRoute('centre_examen_index');
    }
}
