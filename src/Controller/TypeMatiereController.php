<?php

namespace App\Controller;

use  App\Entity\TypeMatiere;
use App\Form\TypeMatiereType;
use App\Repository\TypeMatiereRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
/**
 * @Route("/typematiere")
 * @Security("is_granted('ROLE_ADMIN')")
 */
class TypeMatiereController extends AbstractController
{
    protected $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }
    /**
     * @Route("/", name="type_matiere_index", methods={"GET","POST"})
     */
    public function index(TypeMatiereRepository $typeMatiereRepository,Request $request): Response
    {
        $typeMatiere = new TypeMatiere();
        $form = $this->createForm(TypeMatiereType::class, $typeMatiere);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($typeMatiere);
            $entityManager->flush();
            $this->addFlash("info","Type Ajouter Avec Succés");
            return $this->redirectToRoute('type_matiere_index');
        }

        return $this->render('type_matiere/index.html.twig', [
            'type_matieres' => $typeMatiereRepository->findAll(),'type_matiere' => $typeMatiere,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/newAjax", name="type_matiere_new", methods={"GET","POST"})
     */
    public function newAjax(Request $request): Response
    {
        $typeMatiere = new TypeMatiere();
        $form = $this->createForm(TypeMatiereType::class, $typeMatiere);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($typeMatiere);
            $entityManager->flush();
            $this->addFlash("info", "Type matiére Ajouter Avec Succés");
            return new Response('ok');
        }

        return $this->render('type_matiere/_form.html.twig', [
            'type_matiere' => $typeMatiere,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="type_matiere_show", methods={"GET"})
     */
    public function show(TypeMatiere $typeMatiere): Response
    {
        return $this->render('type_matiere/show.html.twig', [
            'type_matiere' => $typeMatiere,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="type_matiere_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, TypeMatiere $typeMatiere): Response
    {
        $form = $this->createForm(TypeMatiereType::class, $typeMatiere);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("info","Type matiére Modifier Avec Succés");
            return new Response('ok');
        }

        return $this->render('type_matiere/_form_edit.html.twig', [
            'type_matiere' => $typeMatiere,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="type_matiere_delete", methods={"DELETE"})
     */
    public function delete(Request $request, TypeMatiere $typeMatiere): Response
    {
        if ($this->isCsrfTokenValid('delete'.$typeMatiere->getId(), $request->request->get('_token'))) {
            if($typeMatiere->getMatieres()->count()>0){
                $this->session->getFlashBag()->add('danger', 'Ce type de matière ne peut pas être supprimer');
            }
            else{
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($typeMatiere);
            $entityManager->flush();
            $this->addFlash("info", "Type matière supprimer Avec Succés");
        }
        }
        return $this->redirectToRoute('type_matiere_index');
    }
}
