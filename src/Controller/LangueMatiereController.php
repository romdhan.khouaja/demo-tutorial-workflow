<?php

namespace App\Controller;

use App\Entity\LangueMatiere;
use App\Form\LangueMatiereType;
use App\Repository\LangueMatiereRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/languematiere")
 * @Security("is_granted('ROLE_ADMIN')")
 */
class LangueMatiereController extends AbstractController
{
    /**
     * @Route("/", name="langue_matiere_index", methods={"GET","POST"})
     */
    public function index(LangueMatiereRepository $langueMatiereRepository, Request $request): Response
    {
        $langueMatiere = new LangueMatiere();
        $form = $this->createForm(LangueMatiereType::class, $langueMatiere);
        $form->handleRequest($request);


        return $this->render('langue_matiere/index.html.twig', [
            'langue_matieres' => $langueMatiereRepository->findAll(), 'langue_matiere' => $langueMatiere,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/new", name="langue_matiere_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $langueMatiere = new LangueMatiere();
        $form = $this->createForm(LangueMatiereType::class, $langueMatiere);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($langueMatiere);
            $entityManager->flush();
            $this->addFlash("info", "Langue Ajouter Avec Succés");
            return new Response('ok');
        }

        return $this->render('langue_matiere/_form.html.twig', [
            'langue_matiere' => $langueMatiere,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="langue_matiere_show", methods={"GET"})
     */
    public function show(LangueMatiere $langueMatiere): Response
    {
        return $this->render('langue_matiere/show.html.twig', [
            'langue_matiere' => $langueMatiere,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="langue_matiere_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, LangueMatiere $langueMatiere): Response
    {
        $form = $this->createForm(LangueMatiereType::class, $langueMatiere);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("info", "Langue Modifier Avec Succés");
            return new Response('ok');
        }

        return $this->render('langue_matiere/_form_edit.html.twig', [
            'langue_matiere' => $langueMatiere,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="langue_matiere_delete", methods={"DELETE"})
     */
    public function delete(Request $request, LangueMatiere $langueMatiere): Response
    {
        if ($this->isCsrfTokenValid('delete' . $langueMatiere->getId(), $request->request->get('_token'))) {
            if($langueMatiere->getMatieres()->count()>0){
                $this->addFlash("danger", " Ce Langue matière ne peut pas être supprimer ");
            }
            else{
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($langueMatiere);
            $entityManager->flush();
                $this->addFlash("info", "Langue matière supprimer Avec Succés");
        }
        }
        return $this->redirectToRoute('langue_matiere_index');
    }
}
