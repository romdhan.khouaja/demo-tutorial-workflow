<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\MailBox;
use App\Entity\DocumentMail;
use App\Form\MailBoxType;
use App\Repository\MailBoxRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * @Route("/mailbox")
 * @Security("is_granted('ROLE_SCOLARITE')")
 */
class MailBoxController extends AbstractController
{
    /**
     * @Route("/", name="mail_box_index", methods={"GET","POST"})
     */
    public function index(MailBoxRepository $mailBoxRepository,Request $request, \Swift_Mailer $mailer): Response
    {
        // initialisation du nouveau fichier
        $newFilename="";
        $mailBox = new MailBox();
        $form = $this->createForm(MailBoxType::class, $mailBox);
        $form->handleRequest($request);

        return $this->render('mail_box/index.html.twig', [
            'mail_boxes' => $mailBoxRepository->findAll(),'mail_box' => $mailBox,
            'form' => $form->createView()
        ]);
    }

    private static function findLastCreatedMailBox(MailBoxRepository $mailBoxRepository)
    {
        return $mailBoxRepository->findOneBy([], [
            'id' => 'DESC'
        ]);
    }
    /**
     * @Route("/new", name="mail_box_new", methods={"GET","POST"})
     */
    public function new(Request $request, \Swift_Mailer $mailer, MailBoxRepository $mailBoxRepository): Response
    {
// initialisation du nouveau fichier
        $newFilename="";
        $mailBox = new MailBox();
        $form = $this->createForm(MailBoxType::class, $mailBox);
        $form->handleRequest($request);
        // Champ de validation
        if ($form->isSubmitted() && $form->isValid())
        {
            if($this->getUser()){
                $user=$this->getUser();
            }
            // le sujet
            $objet=$form['subject']->getData();
            // le message
            $messagetxt = $form['message']->getData();
            // listes des prof pour retrouver leur email
            $parametre = $this->getParameter('pdf_directory');
            // dd($user);
            $files = $form['documents']->getData();
            // dd($files);
            if($files!==null)
            {
                foreach($files as $file)
                {
                    if ($file)
                    {
                        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                        // this is needed to safely include the file name as part of the URL
                        // dans php.ini decommenter extension=intl pour activer la fonction transliterator_transliterate
                        // redemarrer le serveur web
                        $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                        $newFilename = $safeFilename.'-'.uniqid().'.'.$file->guessExtension();

                        // $fileName = 'document_'.$this->generateUniqueFileName().'.'.$file->guessExtension();

                        // Move the file to the directory where pdf are stored
                        try
                        {
                            $file->move(
                                $parametre,
                                $newFilename
                            );
                        }
                        catch (FileException $e)
                        {
                            // ... handle exception if something happens during file upload
                            $this->addFlash('error','fichier impossible à charger');
                        }
                        // Met à jour la propriété 'fichier' pour stocker le nom du fichier PDF
                        // Au lieu de son contenu
                        $document = new DocumentMail();
                        if($newFilename!==null){
                            $document->setName($newFilename);
                            $document->setUser($user);
                            $mailBox->addDocument($document);
                        }
                        $mailBox->setUser($user);
                    }
                }
            }

            $entityManager = $this->getDoctrine()->getManager();
            // test si il existe un fichier a charger ou non
            if($newFilename!==""){
                $entityManager->persist($document);
            }
            $entityManager->persist($mailBox);
            $entityManager->flush();
            // fonction pour retrouver la dernier entité enregistré
            $dernier_mailbox = self::findLastCreatedMailBox($mailBoxRepository);
            // dd($dernier_mailbox);
            $professeurs = $dernier_mailbox->getEnseignant();
            foreach($professeurs as $prof){

                // dump($prof->getEmail());
                // traitement email
                // Créer le message
                $message = (new \Swift_Message());
                // Donnez un sujet au message
                $message->setSubject($objet);
                // Définir l'adresse de départ avec un tableau associatif
                $message->setFrom('faculter.droit.sfax@gmail.com');
                // Définir les adresses destinations avec un tableau associatif (setTo / setCc / setBcc)
                $message->setTo($prof->getEmail());
                // Donner un Body
                $message->setBody( $messagetxt, 'text/plain');
                foreach($dernier_mailbox->getDocuments() as $document)
                {
                    $message->attach( \Swift_Attachment::fromPath($this->getParameter('pdf_directory').'/'.$document->getName() ));
                }
                $mailer->send($message);
            }
            // die();


            $this->addFlash('info','Mail Envoyer avec succes');
            return new Response('ok');
            // fin traitement de email


        }
        return $this->render('mail_box/_form.html.twig', [
            'mail_boxes' => $mailBoxRepository->findAll(),
            'mail_box' => $mailBox,
            'form' => $form->createView()
        ]);


    }

    /**
     * @Route("/{id}", name="mail_box_show", methods={"GET"})
     */
    public function show(MailBox $mailBox): Response
    {
        return $this->render('mail_box/show.html.twig', [
            'mail_box' => $mailBox,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="mail_box_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, MailBox $mailBox): Response
    {
        $form = $this->createForm(MailBoxType::class, $mailBox);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('mail_box_index');
        }

        return $this->render('mail_box/edit.html.twig', [
            'mail_box' => $mailBox,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="mail_box_delete", methods={"DELETE"})
     */
    public function delete(Request $request, MailBox $mailBox): Response
    {
        if ($this->isCsrfTokenValid('delete'.$mailBox->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($mailBox);
            $entityManager->flush();
        }

        return $this->redirectToRoute('mail_box_index');
    }
}
