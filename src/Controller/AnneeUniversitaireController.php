<?php

namespace App\Controller;

use App\Entity\AnneeUniversitaire;
use App\Form\AnneeUniversitaireType;
use App\Repository\AnneeUniversitaireRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @Route("/anneeuniversitaire")
 * @Security("is_granted('ROLE_ADMIN')")
 *
 */
class AnneeUniversitaireController extends AbstractController
{
    protected $sesssion;

    public function _construct(SessionInterface $session)
    {
        $this->sesssion = $session;
    }

    /**
     * @Route("/", name="annee_universitaire_index", methods={"GET"})
     */
    public function index(AnneeUniversitaireRepository $anneeUniversitaireRepository, Request $request, PaginatorInterface $paginator): Response
    {
        $allannnees = $anneeUniversitaireRepository->findAll();
        // Paginate the results of the query
        $annees = $paginator->paginate(
        // Doctrine Query, not results
            $allannnees,
            // Define the page parameter
            $request->query->getInt('page', 1),
            // Items per page
            5
        );

        return $this->render('annee_universitaire/index.html.twig', [
            'annees' => $annees,
        ]);
    }

    /**
     * @Route("/new", name="annee_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $anneeUniversitaire = new AnneeUniversitaire();
        $form = $this->createForm(AnneeUniversitaireType::class, $anneeUniversitaire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($anneeUniversitaire);
            $entityManager->flush();
            $this->addFlash("info", "Année Ajouter Avec Succés");
            return new Response('ok');
        }

        return $this->render('annee_universitaire/_form.html.twig', [
            'annee_universitaire' => $anneeUniversitaire,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="annee_show", methods={"GET"})
     */
    public function show(AnneeUniversitaire $anneeUniversitaire): Response
    {
        return $this->render('annee_universitaire/show.html.twig', [
            'annee_universitaire' => $anneeUniversitaire,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="annee_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, AnneeUniversitaire $anneeUniversitaire): Response
    {
        $form = $this->createForm(AnneeUniversitaireType::class, $anneeUniversitaire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("info", "Année Modifier Avec Succés");
            return new Response('ok');
        }

        return $this->render('annee_universitaire/_form_edit.html.twig', [
            'annee_universitaire' => $anneeUniversitaire,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="annee_universitaire_delete", methods={"DELETE"})
     */
    public function delete(Request $request, AnneeUniversitaire $anneeUniversitaire): Response
    {
        if ($this->isCsrfTokenValid('delete' . $anneeUniversitaire->getId(), $request->request->get('_token'))) {
           /* if ($anneeUniversitaire->getSemestres()->count() > 0) {
                $this->session->getFlashBag()->add('danger', "Cette annee universitaire ne peut pas etre supprimer");
            } else */{
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($anneeUniversitaire);
                $entityManager->flush();
               $this->session->getFlashBag()->add('success', "Annee universitaire  supprimer avec succé");
            }
        }

        return $this->redirectToRoute('annee_universitaire_index');
    }
}
