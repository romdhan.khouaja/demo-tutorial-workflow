<?php

namespace App\Controller;

use App\Repository\CentreExamenRepository;
use App\Repository\DepartementRepository;
use App\Repository\MatiereRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\DocumentRepository;
use App\Repository\EnseignantRepository;
use App\Repository\FiliereRepository;
use App\Repository\NiveauRepository;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(DepartementRepository $departementRepository,EnseignantRepository $enseignantRepository,FiliereRepository $filiereRepository,NiveauRepository $niveauRepository,DocumentRepository $documentRepository,MatiereRepository $matiereRepository,UserRepository $userRepository,CentreExamenRepository $centreExamenRepository)
    {
        $dep=$departementRepository->findAll();
        $mat=$matiereRepository->findAll();
        $ens=$enseignantRepository->findBy(['avec_contrat' => 1]);
        $fil=$filiereRepository->findAll();
        $niv=$niveauRepository->findAll();
        $usr=$userRepository->findAll();
        $cen=$centreExamenRepository->findAll();
        $user=$this->getUser();

        $documents=$user->getDocuments();
        return $this->render('admin/index.html.twig', [
            'dep' => $dep,
            'mat' => $mat,
            'ens' => $ens,
            'fil'=>$fil,
            'niv'=>$niv,
            'usr'=>$usr,
            'doc'=>$documents,
            'cen'=>$cen,
            'controller_name' => 'AdminController',
        ]);
    }
}
