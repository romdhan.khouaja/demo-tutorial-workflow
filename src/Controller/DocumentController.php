<?php

namespace App\Controller;

use App\Entity\Document;
use App\Form\DocumentType;
use App\Repository\DocumentOpenedRepository;
use App\Repository\DocumentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use App\Entity\DocumentOpened;

/**
 * @Route("/document")
 * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_CHEF_DEPARTEMENT') or is_granted('ROLE_SCOLARITE') or is_granted('ROLE_AGENT') or is_granted('ROLE_DOYEN') or is_granted('ROLE_SECRETAIRE_GENERAL')")
 *
 */
class DocumentController extends AbstractController
{

    /**
     * @Route("/", name="document_index", methods={"GET","POST"})
     */
    public function index(DocumentRepository $documentRepository, Request $request, PaginatorInterface $paginator): Response
    {
        $document = new Document();
        $form = $this->createForm(DocumentType::class, $document);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $form->getData()->setCreatedBy($this->getUser());
            $entityManager->persist($document);
            $entityManager->flush();
            $this->addFlash("info", "Document Enregistrer Avec Succés");
            return $this->redirectToRoute('document_index');
        }

        $documents = $documentRepository->findBy(['createdBy' => $this->getUser()],['createdBy'=>'DESC']);
       //
        // $documents = $documentRepository->findBy(['createdBy' => $this->security->getUser()]);
        // Paginate the results of the query
        $pdocuments = $paginator->paginate(
        // Doctrine Query, not results
            $documents,
            // Define the page parameter
            $request->query->getInt('page', 1),
            // Items per page
            6
        );
        return $this->render('document/index.html.twig', [
            'documents' => $documents,
            'document' => $document,
            'pdocuments' => $pdocuments,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/received", name="document_received", methods={"GET"})
     */
    public function received(DocumentRepository $documentRepository, Request $request, PaginatorInterface $paginator): Response
    {
        // test de l'utilisateur authentifié
        $user = $this->getUser();

        $documents = $user->getDocuments();
        $documents=$documentRepository->rechercheDocument($user->getId());

        $pdfdocuments = $paginator->paginate(
        // Doctrine Query, not results
            $documents,
            // Define the page parameter
            $request->query->getInt('page', 1),
            // Items per page
            6
        );
        return $this->render('document/received.html.twig', [
            'documents' => $documents,
            'pdfdocuments' => $pdfdocuments,
        ]);
    }

    /**
     * @Route("/error", name="document__error", methods={"GET"})
     */
    public function error(Request $request)
    {
        return $this->render('document/error404.html.twig');
    }

    /**
     * @Route("/{id}/send", name="document_send", methods={"GET","POST"})
     */
    public function send(Request $request, Document $document): Response
    {
        $form = $this->createForm(DocumentuserType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $form->getData()->setIsSend(true);
            $this->getDoctrine()->getManager()->persist($document);
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("info", "Document  chargé avec  Succés");
            return $this->redirectToRoute('document_index');
        }

        return $this->render('document/send.html.twig', [
            'document' => $document,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/new", name="document_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $document = new Document();
        $form = $this->createForm(DocumentType::class, $document);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $form->getData()->setCreatedBy($this->getUser());
            $form->getData()->setIsSend(true);
            $entityManager->persist($document);
            $entityManager->flush();
            $this->addFlash("info", "Document envoyé Avec Succés");
            return new Response('ok');

        }

        return $this->render('document/_form.html.twig', [
            'document' => $document,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="documentexcel_show_by_user", methods={"GET"})
     */
    public function showByUser(Document $document = null): Response
    {
        if ($document === null) {
            $this->addFlash('danger', 'Element inexistant');
            return $this->redirectToRoute('document_index');
        }
        $user = $this->getUser();
        if ($document->getCreatedBy() !== $user) {
            $document = null;
        }
        return $this->render('document/show.html.twig', [
            'document' => $document,
        ]);
    }


    /**
     * @Route("/{id}/edit", name="document_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Document $document): Response
    {
        $form = $this->createForm(DocumentType::class, $document);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('document_index');
        }

        return $this->render('document/edit.html.twig', [
            'document' => $document,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="document_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Document $document): Response
    {
        if ($this->isCsrfTokenValid('delete' . $document->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($document);
            $entityManager->flush();
        }

        return $this->redirectToRoute('document_index');
    }

    /**
     * Permet de dire que le document a été vue ou non
     *
     * @Route("/{id}/opened", name="document_opened")
     *
     * @param Document $doc
     * @param ObjectManager $manager
     * @param DocumentOpenedRepository $openedRepository
     * @return Response
     */
    public function opened(Document $doc = null, ObjectManager $manager, DocumentOpenedRepository $openedRepository): Response
    {
        if ($doc === null) {
            $this->addFlash('danger', 'Element inexistant');
            return $this->redirectToRoute('document_received');
        }

        $user = $this->getUser();
        if (!$user) {
            // return $this->json(['code'=>403, 'message'=>"unauthorized"],403);
            $this->addFlash('danger', 'Utilisateur inexistant');
            return $this->redirectToRoute('document_received');
        }
        if (!$doc->isOpenedByUser($user)) {
            $opened = new DocumentOpened();
            $opened->setDocument($doc)
                ->setUser($user);
            $manager->persist($opened);
            $manager->flush();
        }

        return $this->redirectToRoute('document_show_received', [
            'id' => $doc->getId(),
        ]);
    }

    /**
     * @Route("/showreceived/{id}", name="document_show_received", methods={"GET"})
     */
    public function showreceived(Document $document): Response
    {
        if ($document->getUsers()->contains($this->getUser())) {
            return $this->render('document/show.html.twig', [
                'document' => $document,
            ]);
        } else {
            $this->addFlash('danger', ' Vous n\'êtes pas autorisé à visualiser ce message');
            return $this->redirectToRoute('document_received');
        }
    }
}
