<?php

namespace App\Controller;

use App\Repository\DocumentRepository;
use App\Repository\EnseignantRepository;
use App\Repository\FiliereRepository;
use App\Repository\NiveauRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ChefdepartementController extends AbstractController
{
    /**
     * @Route("/chefdepartement", name="chefdepartement")
     */
    public function index(EnseignantRepository $enseignantRepository,FiliereRepository $filiereRepository,NiveauRepository $niveauRepository,DocumentRepository $documentRepository)
    {
        $ens=$enseignantRepository->findBy(['avec_contrat' => 1]);
        $fil=$filiereRepository->findBy(['departement'=>$this->getUser()->getDepartement()]);
        $niv=$niveauRepository->findAllNiveau($this->getUser()->getDepartement());
        $user=$this->getUser();

        $documents=$user->getDocuments();
        return $this->render('chefdepartement/index.html.twig', [
            'ens' => $ens,
            'fil'=>$fil,
            'niv'=>$niv,
            'doc'=>$documents,
            'controller_name' => 'ChefdepartementController',
        ]);
    }
}
