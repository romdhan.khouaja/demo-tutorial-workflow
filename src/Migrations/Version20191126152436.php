<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191126152436 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE annee_universitaire (id INT AUTO_INCREMENT NOT NULL, libelleannee VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE centre_examen (id INT AUTO_INCREMENT NOT NULL, code_centre VARCHAR(255) NOT NULL, capacite INT NOT NULL, libelle_centre VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE configuration (id INT AUTO_INCREMENT NOT NULL, nomprenomdoyen VARCHAR(255) NOT NULL, adresse VARCHAR(255) NOT NULL, telfaculter INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE correction (id INT AUTO_INCREMENT NOT NULL, enseignant_id INT DEFAULT NULL, examen_id INT DEFAULT NULL, part DOUBLE PRECISION NOT NULL, INDEX IDX_A29DA1B8E455FCC0 (enseignant_id), INDEX IDX_A29DA1B85C8659A (examen_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE departement (id INT AUTO_INCREMENT NOT NULL, libelle_departement_fr VARCHAR(255) NOT NULL, libelle_departement_ar VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE document (id INT AUTO_INCREMENT NOT NULL, created_by_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, excel_name VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, is_send TINYINT(1) NOT NULL, INDEX IDX_D8698A76B03A8386 (created_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE document_user (document_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_2A275ADAC33F7837 (document_id), INDEX IDX_2A275ADAA76ED395 (user_id), PRIMARY KEY(document_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE document_csv (id INT AUTO_INCREMENT NOT NULL, session_id INT DEFAULT NULL, csv_name VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, is_sent TINYINT(1) NOT NULL, INDEX IDX_DB0EA0B3613FECDF (session_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE document_mail (id INT AUTO_INCREMENT NOT NULL, mail_box_id INT DEFAULT NULL, user_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_F69220DB52EC454 (mail_box_id), INDEX IDX_F69220DBA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE document_opened (id INT AUTO_INCREMENT NOT NULL, document_id INT DEFAULT NULL, user_id INT DEFAULT NULL, INDEX IDX_1334963DC33F7837 (document_id), INDEX IDX_1334963DA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE enseignant (id INT AUTO_INCREMENT NOT NULL, grade_id INT DEFAULT NULL, qualiter_id INT DEFAULT NULL, nomprenomfr VARCHAR(255) NOT NULL, nomprenomar VARCHAR(255) NOT NULL, code_enseignant VARCHAR(255) NOT NULL, casier INT NOT NULL, cin INT NOT NULL, matricule INT NOT NULL, email VARCHAR(255) NOT NULL, telephone1 INT NOT NULL, telephone2 INT NOT NULL, avec_contrat TINYINT(1) NOT NULL, etat_surveille TINYINT(1) NOT NULL, INDEX IDX_81A72FA1FE19A1A8 (grade_id), INDEX IDX_81A72FA1F6E98F9 (qualiter_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE examen (id INT AUTO_INCREMENT NOT NULL, niveau_id INT DEFAULT NULL, matiere_id INT DEFAULT NULL, session_id INT DEFAULT NULL, nombreetudiant INT NOT NULL, heureexamen DOUBLE PRECISION NOT NULL, heureexamenfin DOUBLE PRECISION NOT NULL, dateexamen DATETIME NOT NULL, INDEX IDX_514C8FECB3E9C81 (niveau_id), INDEX IDX_514C8FECF46CD258 (matiere_id), INDEX IDX_514C8FEC613FECDF (session_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE filiere (id INT AUTO_INCREMENT NOT NULL, departement_id INT DEFAULT NULL, libelle_filiere_fr VARCHAR(255) NOT NULL, libelle_filiere_ar VARCHAR(255) NOT NULL, INDEX IDX_2ED05D9ECCF9E01E (departement_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE grade (id INT AUTO_INCREMENT NOT NULL, libelle_grade_fr VARCHAR(255) NOT NULL, libelle_grade_ar VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE groupe_examen (id INT AUTO_INCREMENT NOT NULL, examen_id INT DEFAULT NULL, centreexamen_id INT DEFAULT NULL, nbretudiantcentre INT NOT NULL, libellegroupe VARCHAR(255) NOT NULL, matretuddebut INT DEFAULT NULL, matretudfin INT DEFAULT NULL, INDEX IDX_3A7273D65C8659A (examen_id), INDEX IDX_3A7273D62DF6F46E (centreexamen_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE groupe_examen_enseignant (groupe_examen_id INT NOT NULL, enseignant_id INT NOT NULL, INDEX IDX_D220675C555CB6AD (groupe_examen_id), INDEX IDX_D220675CE455FCC0 (enseignant_id), PRIMARY KEY(groupe_examen_id, enseignant_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE langue_matiere (id INT AUTO_INCREMENT NOT NULL, libelle_langue_fr VARCHAR(255) NOT NULL, libelle_langue_ar VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mail_box (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, subject VARCHAR(255) NOT NULL, message LONGTEXT DEFAULT NULL, INDEX IDX_14B26750A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mail_box_enseignant (mail_box_id INT NOT NULL, enseignant_id INT NOT NULL, INDEX IDX_370ACA3E52EC454 (mail_box_id), INDEX IDX_370ACA3EE455FCC0 (enseignant_id), PRIMARY KEY(mail_box_id, enseignant_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE matiere (id INT AUTO_INCREMENT NOT NULL, module_id INT DEFAULT NULL, languematiere_id INT DEFAULT NULL, typematiere_id INT DEFAULT NULL, typepassage_id INT DEFAULT NULL, semestre_id INT DEFAULT NULL, niveau_id INT DEFAULT NULL, enseignant_id INT DEFAULT NULL, code_matiere INT NOT NULL, libelle_matiere_fr VARCHAR(255) NOT NULL, libelle_matiere_ar VARCHAR(255) NOT NULL, coefficient DOUBLE PRECISION NOT NULL, dure DOUBLE PRECISION NOT NULL, INDEX IDX_9014574AAFC2B591 (module_id), INDEX IDX_9014574A3757FB0E (languematiere_id), INDEX IDX_9014574AD363255 (typematiere_id), INDEX IDX_9014574A259CA870 (typepassage_id), INDEX IDX_9014574A5577AFDB (semestre_id), INDEX IDX_9014574AB3E9C81 (niveau_id), INDEX IDX_9014574AE455FCC0 (enseignant_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE module (id INT AUTO_INCREMENT NOT NULL, libelle_module_fr VARCHAR(255) NOT NULL, libelle_module_ar VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE niveau (id INT AUTO_INCREMENT NOT NULL, filiere_id INT DEFAULT NULL, code_niveau VARCHAR(255) NOT NULL, libelle_niveau_fr VARCHAR(255) NOT NULL, libelle_niveau_ar VARCHAR(255) NOT NULL, INDEX IDX_4BDFF36B180AA129 (filiere_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE qualiter (id INT AUTO_INCREMENT NOT NULL, libelle_qualite_fr VARCHAR(255) NOT NULL, libelle_qualite_ar VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE semestre (id INT AUTO_INCREMENT NOT NULL, libelle_semestre_ar VARCHAR(255) NOT NULL, libelle_semestre_fr VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE session (id INT AUTO_INCREMENT NOT NULL, annee_id INT DEFAULT NULL, semestre_id INT DEFAULT NULL, codesession VARCHAR(255) NOT NULL, libellesession VARCHAR(255) NOT NULL, valide_etp1 TINYINT(1) DEFAULT NULL, valide_etp2 TINYINT(1) DEFAULT NULL, depart_valid VARCHAR(255) DEFAULT NULL, INDEX IDX_D044D5D4543EC5F0 (annee_id), INDEX IDX_D044D5D45577AFDB (semestre_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_matiere (id INT AUTO_INCREMENT NOT NULL, libelle_type_fr VARCHAR(255) NOT NULL, libelle_type_ar VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_passage (id INT AUTO_INCREMENT NOT NULL, libelle_passage_fr VARCHAR(255) NOT NULL, libelle_passage_ar VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, departement_id INT DEFAULT NULL, username VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, is_active TINYINT(1) NOT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, modified_at DATETIME DEFAULT NULL, email VARCHAR(255) NOT NULL, token VARCHAR(255) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, last_login DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), UNIQUE INDEX UNIQ_8D93D649CCF9E01E (departement_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ext_translations (id INT AUTO_INCREMENT NOT NULL, locale VARCHAR(8) NOT NULL, object_class VARCHAR(255) NOT NULL, field VARCHAR(32) NOT NULL, foreign_key VARCHAR(64) NOT NULL, content LONGTEXT DEFAULT NULL, INDEX translations_lookup_idx (locale, object_class, foreign_key), UNIQUE INDEX lookup_unique_idx (locale, object_class, field, foreign_key), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB ROW_FORMAT = DYNAMIC');
        $this->addSql('ALTER TABLE correction ADD CONSTRAINT FK_A29DA1B8E455FCC0 FOREIGN KEY (enseignant_id) REFERENCES enseignant (id)');
        $this->addSql('ALTER TABLE correction ADD CONSTRAINT FK_A29DA1B85C8659A FOREIGN KEY (examen_id) REFERENCES examen (id)');
        $this->addSql('ALTER TABLE document ADD CONSTRAINT FK_D8698A76B03A8386 FOREIGN KEY (created_by_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE document_user ADD CONSTRAINT FK_2A275ADAC33F7837 FOREIGN KEY (document_id) REFERENCES document (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE document_user ADD CONSTRAINT FK_2A275ADAA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE document_csv ADD CONSTRAINT FK_DB0EA0B3613FECDF FOREIGN KEY (session_id) REFERENCES session (id)');
        $this->addSql('ALTER TABLE document_mail ADD CONSTRAINT FK_F69220DB52EC454 FOREIGN KEY (mail_box_id) REFERENCES mail_box (id)');
        $this->addSql('ALTER TABLE document_mail ADD CONSTRAINT FK_F69220DBA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE document_opened ADD CONSTRAINT FK_1334963DC33F7837 FOREIGN KEY (document_id) REFERENCES document (id)');
        $this->addSql('ALTER TABLE document_opened ADD CONSTRAINT FK_1334963DA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE enseignant ADD CONSTRAINT FK_81A72FA1FE19A1A8 FOREIGN KEY (grade_id) REFERENCES grade (id)');
        $this->addSql('ALTER TABLE enseignant ADD CONSTRAINT FK_81A72FA1F6E98F9 FOREIGN KEY (qualiter_id) REFERENCES qualiter (id)');
        $this->addSql('ALTER TABLE examen ADD CONSTRAINT FK_514C8FECB3E9C81 FOREIGN KEY (niveau_id) REFERENCES niveau (id)');
        $this->addSql('ALTER TABLE examen ADD CONSTRAINT FK_514C8FECF46CD258 FOREIGN KEY (matiere_id) REFERENCES matiere (id)');
        $this->addSql('ALTER TABLE examen ADD CONSTRAINT FK_514C8FEC613FECDF FOREIGN KEY (session_id) REFERENCES session (id)');
        $this->addSql('ALTER TABLE filiere ADD CONSTRAINT FK_2ED05D9ECCF9E01E FOREIGN KEY (departement_id) REFERENCES departement (id)');
        $this->addSql('ALTER TABLE groupe_examen ADD CONSTRAINT FK_3A7273D65C8659A FOREIGN KEY (examen_id) REFERENCES examen (id)');
        $this->addSql('ALTER TABLE groupe_examen ADD CONSTRAINT FK_3A7273D62DF6F46E FOREIGN KEY (centreexamen_id) REFERENCES centre_examen (id)');
        $this->addSql('ALTER TABLE groupe_examen_enseignant ADD CONSTRAINT FK_D220675C555CB6AD FOREIGN KEY (groupe_examen_id) REFERENCES groupe_examen (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE groupe_examen_enseignant ADD CONSTRAINT FK_D220675CE455FCC0 FOREIGN KEY (enseignant_id) REFERENCES enseignant (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE mail_box ADD CONSTRAINT FK_14B26750A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE mail_box_enseignant ADD CONSTRAINT FK_370ACA3E52EC454 FOREIGN KEY (mail_box_id) REFERENCES mail_box (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE mail_box_enseignant ADD CONSTRAINT FK_370ACA3EE455FCC0 FOREIGN KEY (enseignant_id) REFERENCES enseignant (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE matiere ADD CONSTRAINT FK_9014574AAFC2B591 FOREIGN KEY (module_id) REFERENCES module (id)');
        $this->addSql('ALTER TABLE matiere ADD CONSTRAINT FK_9014574A3757FB0E FOREIGN KEY (languematiere_id) REFERENCES langue_matiere (id)');
        $this->addSql('ALTER TABLE matiere ADD CONSTRAINT FK_9014574AD363255 FOREIGN KEY (typematiere_id) REFERENCES type_matiere (id)');
        $this->addSql('ALTER TABLE matiere ADD CONSTRAINT FK_9014574A259CA870 FOREIGN KEY (typepassage_id) REFERENCES type_passage (id)');
        $this->addSql('ALTER TABLE matiere ADD CONSTRAINT FK_9014574A5577AFDB FOREIGN KEY (semestre_id) REFERENCES semestre (id)');
        $this->addSql('ALTER TABLE matiere ADD CONSTRAINT FK_9014574AB3E9C81 FOREIGN KEY (niveau_id) REFERENCES niveau (id)');
        $this->addSql('ALTER TABLE matiere ADD CONSTRAINT FK_9014574AE455FCC0 FOREIGN KEY (enseignant_id) REFERENCES enseignant (id)');
        $this->addSql('ALTER TABLE niveau ADD CONSTRAINT FK_4BDFF36B180AA129 FOREIGN KEY (filiere_id) REFERENCES filiere (id)');
        $this->addSql('ALTER TABLE session ADD CONSTRAINT FK_D044D5D4543EC5F0 FOREIGN KEY (annee_id) REFERENCES annee_universitaire (id)');
        $this->addSql('ALTER TABLE session ADD CONSTRAINT FK_D044D5D45577AFDB FOREIGN KEY (semestre_id) REFERENCES semestre (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649CCF9E01E FOREIGN KEY (departement_id) REFERENCES departement (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE session DROP FOREIGN KEY FK_D044D5D4543EC5F0');
        $this->addSql('ALTER TABLE groupe_examen DROP FOREIGN KEY FK_3A7273D62DF6F46E');
        $this->addSql('ALTER TABLE filiere DROP FOREIGN KEY FK_2ED05D9ECCF9E01E');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649CCF9E01E');
        $this->addSql('ALTER TABLE document_user DROP FOREIGN KEY FK_2A275ADAC33F7837');
        $this->addSql('ALTER TABLE document_opened DROP FOREIGN KEY FK_1334963DC33F7837');
        $this->addSql('ALTER TABLE correction DROP FOREIGN KEY FK_A29DA1B8E455FCC0');
        $this->addSql('ALTER TABLE groupe_examen_enseignant DROP FOREIGN KEY FK_D220675CE455FCC0');
        $this->addSql('ALTER TABLE mail_box_enseignant DROP FOREIGN KEY FK_370ACA3EE455FCC0');
        $this->addSql('ALTER TABLE matiere DROP FOREIGN KEY FK_9014574AE455FCC0');
        $this->addSql('ALTER TABLE correction DROP FOREIGN KEY FK_A29DA1B85C8659A');
        $this->addSql('ALTER TABLE groupe_examen DROP FOREIGN KEY FK_3A7273D65C8659A');
        $this->addSql('ALTER TABLE niveau DROP FOREIGN KEY FK_4BDFF36B180AA129');
        $this->addSql('ALTER TABLE enseignant DROP FOREIGN KEY FK_81A72FA1FE19A1A8');
        $this->addSql('ALTER TABLE groupe_examen_enseignant DROP FOREIGN KEY FK_D220675C555CB6AD');
        $this->addSql('ALTER TABLE matiere DROP FOREIGN KEY FK_9014574A3757FB0E');
        $this->addSql('ALTER TABLE document_mail DROP FOREIGN KEY FK_F69220DB52EC454');
        $this->addSql('ALTER TABLE mail_box_enseignant DROP FOREIGN KEY FK_370ACA3E52EC454');
        $this->addSql('ALTER TABLE examen DROP FOREIGN KEY FK_514C8FECF46CD258');
        $this->addSql('ALTER TABLE matiere DROP FOREIGN KEY FK_9014574AAFC2B591');
        $this->addSql('ALTER TABLE examen DROP FOREIGN KEY FK_514C8FECB3E9C81');
        $this->addSql('ALTER TABLE matiere DROP FOREIGN KEY FK_9014574AB3E9C81');
        $this->addSql('ALTER TABLE enseignant DROP FOREIGN KEY FK_81A72FA1F6E98F9');
        $this->addSql('ALTER TABLE matiere DROP FOREIGN KEY FK_9014574A5577AFDB');
        $this->addSql('ALTER TABLE session DROP FOREIGN KEY FK_D044D5D45577AFDB');
        $this->addSql('ALTER TABLE document_csv DROP FOREIGN KEY FK_DB0EA0B3613FECDF');
        $this->addSql('ALTER TABLE examen DROP FOREIGN KEY FK_514C8FEC613FECDF');
        $this->addSql('ALTER TABLE matiere DROP FOREIGN KEY FK_9014574AD363255');
        $this->addSql('ALTER TABLE matiere DROP FOREIGN KEY FK_9014574A259CA870');
        $this->addSql('ALTER TABLE document DROP FOREIGN KEY FK_D8698A76B03A8386');
        $this->addSql('ALTER TABLE document_user DROP FOREIGN KEY FK_2A275ADAA76ED395');
        $this->addSql('ALTER TABLE document_mail DROP FOREIGN KEY FK_F69220DBA76ED395');
        $this->addSql('ALTER TABLE document_opened DROP FOREIGN KEY FK_1334963DA76ED395');
        $this->addSql('ALTER TABLE mail_box DROP FOREIGN KEY FK_14B26750A76ED395');
        $this->addSql('DROP TABLE annee_universitaire');
        $this->addSql('DROP TABLE centre_examen');
        $this->addSql('DROP TABLE configuration');
        $this->addSql('DROP TABLE correction');
        $this->addSql('DROP TABLE departement');
        $this->addSql('DROP TABLE document');
        $this->addSql('DROP TABLE document_user');
        $this->addSql('DROP TABLE document_csv');
        $this->addSql('DROP TABLE document_mail');
        $this->addSql('DROP TABLE document_opened');
        $this->addSql('DROP TABLE enseignant');
        $this->addSql('DROP TABLE examen');
        $this->addSql('DROP TABLE filiere');
        $this->addSql('DROP TABLE grade');
        $this->addSql('DROP TABLE groupe_examen');
        $this->addSql('DROP TABLE groupe_examen_enseignant');
        $this->addSql('DROP TABLE langue_matiere');
        $this->addSql('DROP TABLE mail_box');
        $this->addSql('DROP TABLE mail_box_enseignant');
        $this->addSql('DROP TABLE matiere');
        $this->addSql('DROP TABLE module');
        $this->addSql('DROP TABLE niveau');
        $this->addSql('DROP TABLE qualiter');
        $this->addSql('DROP TABLE semestre');
        $this->addSql('DROP TABLE session');
        $this->addSql('DROP TABLE type_matiere');
        $this->addSql('DROP TABLE type_passage');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE ext_translations');
    }
}
