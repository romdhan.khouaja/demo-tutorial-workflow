<?php

namespace App\Security;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Http\Util\TargetPathTrait;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class LoginFormAuthenticator extends AbstractFormLoginAuthenticator
{
    use TargetPathTrait;
    private $security;
    private $entityManager;
    private $urlGenerator;
    private $csrfTokenManager;
    private $passwordEncoder;
    private $session;

    public function __construct(EntityManagerInterface $entityManager, UrlGeneratorInterface $urlGenerator, CsrfTokenManagerInterface $csrfTokenManager, UserPasswordEncoderInterface $passwordEncoder,SessionInterface $session, Security $security)
    {
        $this->entityManager = $entityManager;
        $this->urlGenerator = $urlGenerator;
        $this->csrfTokenManager = $csrfTokenManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->session = $session;
        $this->security = $security;
    }

    public function supports(Request $request)
    {
        return 'app_login' === $request->attributes->get('_route')
            && $request->isMethod('POST');
    }

    public function getCredentials(Request $request)
    {
        $credentials = [
            'username' => $request->request->get('username'),
            'password' => $request->request->get('password'),
            'csrf_token' => $request->request->get('_csrf_token'),
        ];
        $request->getSession()->set(
            Security::LAST_USERNAME,
            $credentials['username']
        );

        return $credentials;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $token = new CsrfToken('authenticate', $credentials['csrf_token']);
        if (!$this->csrfTokenManager->isTokenValid($token)) {
            throw new InvalidCsrfTokenException();
        }

        $user = $this->entityManager->getRepository(User::class)->findOneBy(['username' => $credentials['username']]);

        if (!$user || $user->getIsActive()===false) {
            // fail authentication with a custom error
            throw new CustomUserMessageAuthenticationException('Le nom d\'utilisateur n\'a pas été trouvé');
        }

        return $user;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return $this->passwordEncoder->isPasswordValid($user, $credentials['password']);
    }

   /* public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        if ($targetPath = $this->getTargetPath($request->getSession(), $providerKey)) {
            return new RedirectResponse($targetPath);
        }

        // For example : return new RedirectResponse($this->urlGenerator->generate('some_route'));
        throw new \Exception('TODO: provide a valid redirect inside '.__FILE__);
    }*/
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        /*if ($targetPath = $this->getTargetPath($request->getSession(), $providerKey)) {
            return new RedirectResponse($targetPath);
        }*/
        // retrouver le role
     /*   $roles = $token->getRoles();
        $username = $token->getUsername();
        $rolesTab = array_map(function ($role) {
            return $role->getRole();
        }, $roles);
        if (in_array('ROLE_ADMIN', $rolesTab, true)) {
            // c'est un aministrateur : on le rediriger vers l'espace admin
            $redirection = new RedirectResponse($this->urlGenerator->generate('admin'));
        } else if(in_array('ROLE_CHEF_DEPARTEMENT', $rolesTab, true)) {
            // c'est un chef departement : on le rediriger vers l'espace chef departement
            // retrouver le user
            $user = $token->getUser();
            $userId = $user->getId();
            $deptId = $user->getDepartement()->getId();
            // set and get session attributes
            // $session = getSession();
            $this->session->set('userId', $userId);
            $this->session->set('deptId', $deptId);
            $redirection = new RedirectResponse($this->urlGenerator->generate('chefdepartement'));
        } else if(in_array('ROLE_AGENT', $rolesTab, true))
        {
            // c'est un agent : on le rediriger vers l'espace agent
            $redirection = new RedirectResponse($this->urlGenerator->generate('agent'));
        }
        else if(in_array('ROLE_DOYEN', $rolesTab, true))
        {
            // c'est un doyen : on le rediriger vers l'espace doyen
            $redirection = new RedirectResponse($this->urlGenerator->generate('doyen'));
        }
        else if(in_array('ROLE_SECRETAIRE_GENERAL', $rolesTab, true))
        {
            // c'est un secretaire : on le rediriger vers l'espace secretaire
            $redirection = new RedirectResponse($this->urlGenerator->generate('secretairegeneral'));
        }
        else if(in_array('ROLE_SCOLARITE', $rolesTab, true))
        {
            // c'est un scolarite : on le rediriger vers l'espace scolarite
            $redirection = new RedirectResponse($this->urlGenerator->generate('scolarite'));
        }
        else
        {
            if($this->session)
            {
                $this->session->clear();
            }
            // c'est un utilisateur inconnu : on le redirige vers l'accueil
            $redirection = new RedirectResponse($this->urlGenerator->generate('home'));
        }
        // For example : return new RedirectResponse($this->urlGenerator->generate('some_route'));
        // throw new \Exception('TODO: provide a valid redirect inside '.__FILE__);
        return $redirection ;*/
        if($this->security->isGranted('ROLE_ADMIN')){
            $redirection = new RedirectResponse($this->urlGenerator->generate('admin'));
        }
        if($this->security->isGranted('ROLE_AGENT')){
            $redirection = new RedirectResponse($this->urlGenerator->generate('agent'));
        }
        if($this->security->isGranted('ROLE_SCOLARITE')){
            $redirection = new RedirectResponse($this->urlGenerator->generate('scolarite'));
        }
        if($this->security->isGranted('ROLE_CHEF_DEPARTEMENT')){
            $redirection = new RedirectResponse($this->urlGenerator->generate('chefdepartement'));
        }
        if($this->security->isGranted('ROLE_DOYEN')){
            $redirection = new RedirectResponse($this->urlGenerator->generate('doyen'));
        }
        if($this->security->isGranted('ROLE_SECRETAIRE_GENERAL')){
            $redirection = new RedirectResponse($this->urlGenerator->generate('secretairegeneral'));
        }
        return $redirection ;

    }

    protected function getLoginUrl()
    {
        return $this->urlGenerator->generate('app_login');
    }
}
